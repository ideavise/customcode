Ext.define('Paperless.view.main.Actions', {
    extend: 'Ext.app.Controller',
    
    alias: 'custom',
    
    
    
    rebuildlabxmlHandler: function () {
        var cmp = this.main.view.down("*[name='rebuildsample']");
        var samplealias = cmp.getValue();
        debugger;
        if (samplealias) {
            this.main.actions.perform({
                cmd: 'request',
                a: samplealias,
                relationship: 'descendants',
                scope: this,
                callback: this.rebuildlabxmlCallback
            });
        }
    },
    
    rebuildlabxmlCallback: function (rsp) {
        var me = (rsp.scope||this);
        var objTpl = me.main.data.getObjectByName('labxmltemplate');
        var tpl = new Ext.XTemplate(objTpl.ct,me.main.util.getTemplateFunctions());
        if (rsp.leaf) {
            var h = tpl.apply(rsp.leaf);
            me.main.actions.perform({
                cmd: 'put',
                component: me,
                objects: [rsp.leaf],
                ct: h
            });
            me.main.actions.perform({
                cmd: 'say',
                component: me,
                title: 'Lab XML Saved',
                message: h
            });
        } else {
            me.main.actions.perform({
                cmd: 'say',
                component: me,
                title: 'Error',
                message: 'Cant find this sample.'
            });
        }
    },
    
    
    
//removeaction property on grid - set to name of custom action
//addaction property on grid - set to name of custom function    
    /* -----------------------------------------------------------------------
    Go Previous Dashboard:
    Finds the last dashboard visited by the user and opens it
    __________________________________________________________________________ */
    
    gopreviousdashboardHandler: function (sp) {
        var arr = this.main.render.getHistory();
    
	this.main.cache.invalidate(this.main.entity.getUser());
        if (arr.length>0){
            for (var i=arr.length-1,l=0;i>l;i--){
		var item = arr[i];
                var src = item.source||item.redirectsource;
                if (/dashboard\b/.test(String(src))) {
                    this.main.actions.perform(item);
                    break;
                }
            }
        }
    },
    
    gobackHandler: function (sp) {
        var arr = this.main.render.getHistory();
        var last = arr.pop();
        var item = arr.pop();
        this.main.actions.perform(item);
    },
    
    triggerrequiredHandler: function (sp) {
        this.main.rules.broadcastTopic({topic:'{activetestsession}'});
        this.main.rules.broadcastTopic({topic:'requiredchange'});
    },
    
    onlinestatusHandler: function (sp) {
    
        debugger;

        var btn = this.main.view.down('*[n="onlineindicator"]');

        var fl = this.main.environment.getOnline();
        if (fl) {
            btn.addCls('online');
            btn.removeCls('offline');
        } else {
            btn.addCls('offline');
            btn.removeCls('online');
        }
    },

    /* -----------------------------------------------------------------------
    Start DCF:
    Opens the DCF form for the selected test session
    __________________________________________________________________________ */
    
    startdcfHandler: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
     
//	this.main.actions.perform({
//            cmd: 'setbusy',
//            component: sp.par.par.par,
//            message: 'Opening DCF...'
//        });
   
	if (!Ext.isDefined(ats.DCOPNFinal)) {
		this.main.actions.perform({
			cmd: "put",
			component: this,
			objects:[ats],
			DCOPNFinal: this.main.entity.getUser().PersonId,
				DCOPNFinal_caption: this.main.entity.getUser().FirstName + ' ' + this.main.entity.getUser().LastName
		});
	}
    
        // Enable/disable blood form
        var bforms = this.main.data.getObjectByName('dcorbl');
        var disablebl = (ats.Blood===true) ? false : true;
        bforms.disabled = disablebl;
        
        // Enable/disable urine form
        var uforms = this.main.data.getObjectByName('dcorur');
        var disableur = (ats.Urine===true) ? false : true;
        uforms.disabled = disableur;
           
        // Sets time zone to the current context
        // every time test session is opened
        this.main.actions.perform({
             cmd: "put",
             component: this,
             objects: [ats],
             TimeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
        });
        
        // If necessary, set SiteId to OOC for out of competition tests
        if (!Ext.isDefined(ats.SiteID)){
            this.main.actions.perform({
                cmd: "put",
                component: this,
                objects: [ats],
                SiteID: 'OOC'
            });
        }
    
    	this.main.actions.perform({
		cmd: 'safemode',
		value: true
	});	
           
        this.main.actions.perform({
            cmd: 'goto',
            redirectsource: 'dcfForm',
            target: 'userinterface'
        });
    
//	this.main.actions.perform({
//                cmd: 'removebusy',
//                component: sp.par.par.par
//            });
    },
    
    /* -----------------------------------------------------------------------
    Toggle Blood forms:
    Enable/Disable blood forms when the Blood checkbox is toggled
    __________________________________________________________________________ */
    
    togglebloodformsHandler: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
        var btab = this.main.util.get('sessionmanager').tabBar.items.items[4];
        var bforms = this.main.util.get('dcorbl');
        if (ats.Blood===true) {
            bforms.enable(true);
            btab.enable(true);
        } else {
            bforms.disable(true);
            btab.disable(true);
        }
        
        this.main.rules.broadcastTopic({topic:'{activetestsession}'});
//        this.main.rules.broadcastTopic({topic:'refresh',component:sm});
        this.main.rules.broadcastTopic({topic:'requiredchange'});

    },
    
    /* -----------------------------------------------------------------------
    Toggle Blood forms:
    Enable/Disable blood forms when the Blood checkbox is toggled
    __________________________________________________________________________ */
    
    toggleurineformsHandler: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
        var utab = this.main.util.get('sessionmanager').tabBar.items.items[5];
        var uforms = this.main.util.get('dcorur');
        if (ats.Urine===true) {
            uforms.enable(true);
            utab.enable(true);
        } else {
            uforms.disable(true);
            utab.disable(true);
        }
        
        this.main.rules.broadcastTopic({topic:'{activetestsession}'});
//        this.main.rules.broadcastTopic({topic:'refresh',component:sm});
        this.main.rules.broadcastTopic({topic:'requiredchange'});

    },
    
    refreshdeclarationslistHandler: function (sp) {
        var deccmp = this.main.util.get('declarations');
        if (deccmp) deccmp.refresh();
    },
    
    /* -----------------------------------------------------------------------
    Incomplete Test:
    Allows user to sumbit test without completing required fields
    __________________________________________________________________________ */
    
    incompletetestHandler: function (sp) {
        this.main.rules.broadcastTopic({topic:'{activetestsession}'});
        this.main.rules.broadcastTopic({topic:'requiredchange'});
    },
    
    /* -----------------------------------------------------------------------
    Copy Notification Date:
    Copies the notification date from the notification form to the athlete info
    form.
    __________________________________________________________________________ */
    
    copynotificationdateHandler: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
        
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [ats],
            ArrivalDate: ats.NotificationDate,
            ArrivalTime: ats.NotificationTime
        });
        
        var adate = this.main.util.get('ArrivalDate');
        var atime = this.main.util.get('ArrivalTime');
        
        adate.setValue(ats.NotificationDate);
        adate.commitValue();
        
//        atime.setValue(ats.NotificationTime);
//        atime.commitValue(atime,ats.NotificationTime,null);

        this.main.rules.broadcastTopic({topic:'{activetestsession}'});
        this.main.rules.broadcastTopic({topic:'requiredchange'});

    },
    
    /* -----------------------------------------------------------------------
    Update test session status:
    Update the status of the test to OPENED after the athlete signs the 
    notification form.  TODO: how/when to call this?
    __________________________________________________________________________ */
    
    updatetestsessionstatusHandler: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
                // Change status of test session once the athlete has signed notification
        if (ats.notificationPart4_completed && this.main.data.getDerivedValue(ats, 'Status')=='ASSIGNED'){
            this.main.actions.perform({
                cmd: "put",
                component: this,
                objects: [ats],
                _Status: 'OPENED'
            });
        }
    },
    
    /* -----------------------------------------------------------------------
    Copy Address:
    Copies the values in the Residence Address form to the matching fields
    in the Mailing Address form.
    __________________________________________________________________________ */
    
    copyaddressHandler: function (sp) {
//        debugger;
        
        if (sp.component.value==true) {
            var ats = this.main.data.getProvider("activetestsession");
           
            this.main.actions.perform({
                cmd: "put",
                component: this,
                objects: [ats],
                MailingAddr1: ats.ResidenceAddr1,
                MailingCity: ats.ResidenceCity,
                MailingState: ats.ResidenceState,
                MailingZip: ats.ResidenceZip,
                MailingCountry: ats.ResidenceCountry
            });
           
           // If first field in the mailing address set is triggered
           // the rest of the fields are populated automatically from
           // the values in the active test session
            this.main.util.get('MailingAddr1').setValue(ats.ResidenceAddr1);
            this.main.util.get('MailingAddr1').commitValue();

            this.main.rules.broadcastTopic({topic:'{activetestsession}'});
            this.main.rules.broadcastTopic({topic:'requiredchange'});
           
        }
        
    },
    
    doLoadActiveDocumentHandler: function (sp) {
        var ad = this.main.data.getProvider("activedocument");
    //TODO Sample Manifest document isn't loading after app is reloaded

        var doccmp = this.main.data.getObjectByName('DocumentReviewer');
        doccmp.ct = ad.ct;
        
        
        this.main.actions.perform({
            cmd: 'goto',
            redirectsource: 'docreviewpanel',
            target: 'userinterface'
        });
    },

     /* -------------------------------------------------------------------
     * Blood sample barcode scanner
     * _________________________________________________________________*/

    scanbsampleHandler: function (sp) {
	var samplefld = this.main.util.get('BloodSampleCode');
	samplefld.setValue(sp.value);
        samplefld.commitValue();
    },

     /* -------------------------------------------------------------------
     * Urine sample barcode scanner
     * _________________________________________________________________*/

    scanusampleHandler: function (sp) {
	    var samplefld = this.main.util.get('UrineSampleCode');
	    samplefld.setValue(sp.value);
	    samplefld.commitValue();
    },

     /* -------------------------------------------------------------------
     * Tracking number barcode scanner
     * _________________________________________________________________*/

    scantrackingnumberHandler: function (sp) {
	    var samplefld = this.main.util.get('TrackingNumber');
	    samplefld.setValue(sp.value);
	    samplefld.commitValue();
    },

     /* -------------------------------------------------------------------
     * Monitor number barcode scanner
     * _________________________________________________________________*/

    scanmonitornumberHandler: function (sp) {
	    var samplefld = this.main.util.get('MonitorNumber');
	    samplefld.setValue(sp.value);
	    samplefld.commitValue();
    },

    /* -------------------------------------------------------------------
     * Functions to handle the Urine Sample form multiples
     * _________________________________________________________________*/

    editurinesampleHandler: function (sp) {
	// Run the buisness logic for the selected Urine sample
	this.main.rules.broadcastTopic({topic:'{activeurinesample}'});
	this.main.rules.broadcastTopic({topic:'requiredchange'});

        var samplelist = this.main.util.get('urinetabs');
        //samplelist.collapse();
	samplelist.setCollapsed(false);
	samplelist.setActiveTab(0);
    },
    
    addurinesampleHandler: function (sp) {
	// Run the business logic to include new urine sample
	Ext.defer(function () {
	    this.main.rules.broadcastTopic({topic:'{activeurinesample}'});
	    this.main.rules.broadcastTopic({topic:'requiredchange'});
	    this.checksubsequentrequiredHandler(sp);
	},
	1200,
	this)
    },

    removeurinesampleHandler: function (sp) {
	// Run the business logic for the updated list
	Ext.defer(function () {
	    this.main.rules.broadcastTopic({topic:'{activeurinesample}'});
	    this.main.rules.broadcastTopic({topic:'requiredchange'});
	    this.checksubsequentrequiredHandler(sp);
	},
	1200,
	this)
    },

    checksubsequentrequiredHandler: function (sp) {
	// Sets subsequentofficerrequired flag to true in the test session
	// if more than one urine sample is found.  Will enable/disable the
	// Subsequent Officer tab in the signatures section
	var adp = this.main.data.getProvider("{activetestsession}");
	var desc = this.main.data.getDescendants(adp.a);
        var arr = this.main.data.filter(desc,'cid EQ urinesample');

	var v = (arr.length>1) ? true : false;
	this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [adp],
            subsequentofficerrequired: v
        });

	this.main.rules.broadcastTopic({topic:'subsequentofficerrequired'});
    },

    /* -------------------------------------------------------------------
     * Functions to handle the Partial Sample  form multiples
     * _________________________________________________________________*/
   
    editpartialsampleHandler: function (sp) {
	// Run the business logic for the selected Partial Sample
	this.main.rules.broadcastTopic({topic:'{activepartialsample}'});
	this.main.rules.broadcastTopic({topic:'requiredchange'});

	// Hide the navigation grid
        var samplelist = this.main.util.get('ptdetailstabs');
        samplelist.setCollapsed(false);
	samplelist.setActiveTab(0);
    },
    
    addpartialsampleHandler: function (sp) {
	// Run the business logic to include the new Partial Sample
	Ext.defer(function () {
	    this.main.rules.broadcastTopic({topic:'{activepartialsample}'});
	    this.main.rules.broadcastTopic({topic:'requiredchange'});
    	},
    	1200,
    	this)
    },

    removepartialsampleHandler: function (sp) {
	// Run the business logic for the updated list
	Ext.defer(function () {
	    this.main.rules.broadcastTopic({topic:'{activepartialsample}'});
	    this.main.rules.broadcastTopic({topic:'requiredchange'});
	},
	1200,
	this)
    },

    /* -------------------------------------------------------------------
     * Functions to handle the Custody transfer form multiples
     * _________________________________________________________________*/
    
    editcustodytransferHandler: function (sp) {
	// Run the business logic for the selected Custody Transfer
	this.main.rules.broadcastTopic({topic:'{activecustodytransfer}'});
    	this.main.rules.broadcastTopic({topic:'requiredchange'});

	// Hide the navigation grid
        var samplelist = this.main.util.get('custodytabs');
        samplelist.setCollapsed(false);
	samplelist.setActiveTab(0);
    },

    addcustodytransferHandler: function (sp) {
	// Run the business logic to include the new Custody Transfer
	Ext.defer(function () {
	   this.main.rules.broadcastTopic({topic:'{activecustodytransfer}'});
	   this.main.rules.broadcastTopic({topic:'requiredchange'});
	},
	1200,
	this)
    },

    removecustodytransferHandler: function (sp) {
	// Run the business logic for the updated list
	Ext.defer(function () {
	    this.main.rules.broadcastTopic({topic:'{activecustodytransfer}'});
	    this.main.rules.broadcastTopic({topic:'requiredchange'});
	},
	1200,
	this)
    },
    
    /* -------------------------------------------------------------------
     * Functions to handle the Supplementary Report  form multiples
     * _________________________________________________________________*/
    
    addsuppreportHandler: function (sp) {
	    Ext.defer(function () {
        	this.main.data.setProvider("{supplementarysupportperson}",{});
		// force business logic to update
		this.main.rules.broadcastTopic({topic:'{activesupplementary}'});
        	this.main.rules.broadcastTopic({topic:'requiredchange'});
                this.main.util.get('addsuppsignaturesgrid').loadData();
	    },
	    1200,
	    this)
    },

    removesuppreportHandler: function (sp) {
       Ext.defer(function () {
           this.main.rules.broadcastTopic({topic:'{activesupplementary}'});
           this.main.rules.broadcastTopic({topic:'requiredchange'});
        },
        1200,
        this)
    },

    editpresuppHandler: function (sp) {
	// Configure the provider for the Signature form
	// this is a special case because we have a multiple (signatures) within
	// a multiple (supplementary forms)
	var sup = this.main.data.getProvider("activesupplementary");
	var supsigs = this.main.data.filter(this.main.data.getChildren(sup),"cid EQ supportperson");
	this.main.data.setProvider("{supplementarysupportperson}",supsigs);

        var suppgrd = this.main.util.get('addsuppsignaturesgrid');
	suppgrd.loadData();
	// Select the first item in the grid
	//var grd = suppgrd.down("grid");
	//var rec = grd.getStore().config.data[0];
        //var store = grid.getStore();
        //var data = store.proxy.data;
	//var rec = grd.store.first();
	//grd.getSelectionModel().select([rec]);
	//grd.setSelection(0);
	
	this.main.rules.broadcastTopic({topic:'{activesupplementary}'});
	this.main.rules.broadcastTopic({topic:'requiredchange'});
	this.main.rules.broadcastTopic({topic:'{supplementarysupportperson}'});


        var samplelist = this.main.util.get('suptabs');
        samplelist.setCollapsed(false);
	samplelist.setActiveTab(0);
    },
   
    /* -----------------------------------------------------------------------
    startuar:
    Starts an Unsuccessful Attempt Report (UAR) for the test session:  If a UAR
    already exists for the test session it is used again, otherwise a new UAR
    document is created; Starts providers for the two locations in the UAR.
    __________________________________________________________________________ */
    
    startuarHandler: function (sp) {
        
        var par = this.main.data.getProvider("activetestsession");
        
        // Reset all providers
//        this.main.data.setProvider("uarphonecalls",{});
//        this.main.data.setProvider("uarsecondphonecalls",{});

        // Mark the test session as opened
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [par],
            _Status: 'OPENED'
        });

        // Find if the test session already contains a UAR
        var desc = this.main.data.getDescendants(par.a);
        var arr = this.main.data.filter(desc,'cid EQ uar');
        
        if (arr.length > 0){
            this.startuarCallback(arr);
        } else {
            var aarr = this.main.data.filter(desc,'cid EQ athlete');
            var apn = '';
            var spt = ''
            if (aarr.length>0) {
                if (Ext.isDefined(aarr[0].FirstName) && Ext.isDefined(aarr[0].LastName)) apn = apn.concat(aarr[0].FirstName,' ',aarr[0].LastName);
                if (Ext.isDefined(aarr[0].Sport)) spt = aarr[0].Sport;
           }
           
        
            this.main.data.create({
                callback: this.startuarCallback,
                component: sp.component,
                scope: this,
                data: [{
                    cid: 'uar',
                    l:  'Unsuccessful Attempt',
                    pa: par.ca,
                    t: 0,
                    AthletePrintedName: apn,
                    Sport: spt,
                    TestingAuthority: par.TestingAuthority,
                    AttemptType: par.AttemptType
                }]
            });
        }
        
    },
    
    startuarCallback: function (rsp) {
    
        // set the uar to the activeuar provider and open the uar form
        var me = (rsp.scope||this);
        var ts = me.main.data.getProvider("activetestsession");
        var uar, loc1, loc2;
        if (rsp.objects) {
            // A new UAR object was created
            uar = rsp.objects[0];
           
        } else {
            // UAR already exists
            uar = rsp[0];
        }
        
        me.main.data.setProvider("activeuar",uar.a);
        
        // enable/disable first location phone calls
        var phonelist = me.main.data.getObjectByName('uarfirstnumberscalledgrid');

        if (uar.FirstCallsPlaced==true) {
           phonelist.disabled=false;
        } else {
           phonelist.disabled=true;
        }
        
        // enable/disable second location fields
        
        var loc = me.main.data.getObjectByName('secondlocfs');
        var time = me.main.data.getObjectByName('secondloctimefs');
        var contact = me.main.data.getObjectByName('secondlocpersoncontactedfs');
        var access = me.main.data.getObjectByName('secondlocrestrictedaccessfs');
        var phone = me.main.data.getObjectByName('secondloccallsplacedfs');
        var secondphonelist = me.main.data.getObjectByName('uarsecondnumberscalledgrid');
        var comments = me.main.data.getObjectByName('secondloccommentsfs');
        
        if (uar.secondlocationvisited=="Yes") {
            loc.disabled=false;
            time.disabled=false;
            contact.disabled=false;
            access.disabled=false;
            phone.disabled=false;
            secondphonelist.disabled = (uar.SecondCallsPlaced==true) ? false : true;
            comments.disabled=false;
        } else {
            loc.disabled=true;
            time.disabled=true;
            contact.disabled=true;
            access.disabled=true;
            phone.disabled=true;
            secondphonelist.disabled=true;
            comments.disabled=true;
        }
        
        me.main.actions.perform({
            cmd: 'request',
            source: 'uarpanel',
            target: 'userinterface',
            provider: 'activeuar'
        });
    },
    
    
    /* -----------------------------------------------------------------------
    addfirstloccallsHandler:
    Run business logic when a phone number is added or removed
    __________________________________________________________________________ */

    addfirstloccallsHandler: function (sp) {
	// disable Phone Calls Placed checkbox
        var auar = this.main.data.getProvider("activeuar");
	this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [auar],
            FirstLocPhoneExists: 'Yes'
        });
	// Run the business logic 
	Ext.defer(function () {
	    this.main.rules.broadcastTopic({topic:'FirstLocPhoneExists'});
	    this.main.rules.broadcastTopic({topic:'{uarphonecalls}'});
	    this.main.rules.broadcastTopic({topic:'requiredchange'});
    	},
    	1200,
    	this)
    },

    /*------------------------------------------------------------------------
     * deletefirstloccallsHandler:
     * Run business logic when phone number is removed
     * _____________________________________________________________________*/

    deletefirstloccallsHandler: function (sp) {
	    // see if we need to enable the Phone Calls Placed checkbox again
	    var uar = this.main.data.getProvider("activeuar");
            var pnumbers = this.main.data.filter(this.main.data.getChildren(uar),"cid EQ phonecall");
	    var pfound = false;
            for (var i=0,l=pnumbers.length;i<l;i++) {
                if (pnumbers[i].UARLocation=='FirstLocation') pfound=true;
	    }

	    // If no numbers found for first location enable checkbox
	    if (pfound){
	    } else {
		    this.main.actions.perform({
			    cmd: "put",
			    component: this,
			    objects: [uar],
			    FirstLocPhoneExists: 'No'
		    });

        	    this.main.data.setProvider("{uarphonecalls}",{});
    	    }
            
            // Run the business logic
	    Ext.defer(function () {
		this.main.rules.broadcastTopic({topic:'FirstLocPhoneExists'});
	    	this.main.rules.broadcastTopic({topic:'{uarphonecalls}'});
	    	this.main.rules.broadcastTopic({topic:'requiredchange'});
    	    },
    	    1200,
    	    this)    
    },


   
    /* -----------------------------------------------------------------------
    addsecondloccallsHandler:
    Run business logic when a phone number is added 
    __________________________________________________________________________ */

    addsecondloccallsHandler: function (sp) {
	// disable Phone Calls Placed checkbox
        var auar = this.main.data.getProvider("activeuar");
	this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [auar],
            SecondLocPhoneExists: 'Yes'
        });
	// Run the business logic 
	Ext.defer(function () {
	    this.main.rules.broadcastTopic({topic:'SecondLocPhoneExists'});
	    this.main.rules.broadcastTopic({topic:'{uarsecondphonecalls}'});
	    this.main.rules.broadcastTopic({topic:'requiredchange'});
    	},
    	1200,
    	this)
    },

    /*------------------------------------------------------------------------
     * deletesecondloccallsHandler:
     * Run business logic when phone number is removed
     * _____________________________________________________________________*/

    deletesecondloccallsHandler: function (sp) {
	    // see if we need to enable the Phone Calls Placed checkbox again
	    var uar = this.main.data.getProvider("activeuar");
            var pnumbers = this.main.data.filter(this.main.data.getChildren(uar),"cid EQ phonecall");
	    var pfound = false;
            for (var i=0,l=pnumbers.length;i<l;i++) {
                if (pnumbers[i].UARLocation=='SecondLocation') pfound=true;
	    }

	    // If no numbers found for second location enable checkbox
	    if (pfound){
	    } else {
		    this.main.actions.perform({
			    cmd: "put",
			    component: this,
			    objects: [uar],
			    SecondLocPhoneExists: 'No'
		    });

        	    this.main.data.setProvider("{uarsecondphonecalls}",{});
    	    }
            
            // Run the business logic
	    Ext.defer(function () {
		this.main.rules.broadcastTopic({topic:'SecondLocPhoneExists'});
	    	this.main.rules.broadcastTopic({topic:'{uarsecondphonecalls}'});
	    	this.main.rules.broadcastTopic({topic:'requiredchange'});
    	    },
    	    1200,
    	    this)    
    },


    /* -----------------------------------------------------------------------
    secondlocvisited:
    Toggles disabled/enabled state of second location fields in uar
    __________________________________________________________________________ */
    
    secondlocvisitedHandler: function (sp) {
        var uar = this.main.data.getProvider("activeuar");
        var loc = this.main.util.get('secondlocfs');
        var time = this.main.util.get('secondloctimefs');
        var contact = this.main.util.get('secondlocpersoncontactedfs');
        var access = this.main.util.get('secondlocrestrictedaccessfs');
        var phone = this.main.util.get('secondloccallsplacedfs');
        var phonelist = this.main.util.get('uarsecondnumberscalledgrid');
        var phonedetails = this.main.util.get('uarsecondphonedetailsfs');
        var comments = this.main.util.get('secondloccommentsfs');
        
        if (uar.secondlocationvisited=="Yes") {
            loc.enable(true);
            time.enable(true);
            contact.enable(true);
            access.enable(true);
            phone.enable(true);
            comments.enable(true);
            if (uar.SecondCallsPlaced==true) {
                phonelist.enable(true);
                phonedetails.enable(true);
            }
        } else {
            loc.disable(true);
            time.disable(true);
            contact.disable(true);
            access.disable(true);
            phone.disable(true);
            phonelist.disable(true);
            phonedetails.disable(true);
            comments.disable(true);
        }
        
        
        this.main.rules.broadcastTopic({topic:'requiredchange'});
        
        
    },
    
    /* -----------------------------------------------------------------------
    submituar:
    Submits an Unsuccessful Attempt Report (UAR): creates UAR document; check
    that times are valid; display confirmation message; update status of 
    associated test session
    __________________________________________________________________________ */
    
    submituarHandler: function (sp) {
        var auar = this.main.data.getProvider("activeuar");
        
        // check that the times entered by user are valid
        var msgarr = [];
        var timemsg = '<div>You must correct the following time values:</div>';
        var timepass = false;
        if (auar.FirstArrivalTime>auar.FirstAttemptStart) msgarr.push('First Location: Arrival Time is later than Attempt Start');
        if (auar.FirstArrivalTime>auar.FirstAttemptEnd) msgarr.push('First Location: Arrival Time is later than Attempt End');
        if (auar.FirstAttemptStart>auar.FirstAttemptEnd) msgarr.push('First Location: Attempt Start is later than Attempt End');
        if (auar.SecondArrivalTime>auar.SecondAttemptStart) msgarr.push('Second Location: Arrival Time is later than Attempt Start');
        if (auar.SecondArrivalTime>auar.SecondAttemptEnd) msgarr.push('Second Location: Arrival Time is later than Attempt End');
        if (auar.SecondAttemptStart>auar.SecondAttemptEnd) msgarr.push('Second Location: Attempt Start is later than Attempt End');
        
        for (var i=0,l=msgarr.length;i<l;i++) {
           timemsg = timemsg + '<div>' + msgarr[i] + '</div>';
           timepass = true;
        }
        
        if (timepass) {
           this.main.feedback.say({
                title: 'Invalid Time Found',
                message: timemsg
            });
            return;
        }
        
        var cb = Ext.bind(this.submituarCallback,this);
        
        Ext.Msg.show({
            title: 'Submit Confirmation',
            message: 'All information, particularly the Comments Sections has been thoroughly reviewed and phrases such as “missed test” or “whereabouts failure” have been avoided, as well as ensuring all spelling and grammar is accurate?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: cb
        });
    },
    
    submituarCallback: function (rsp) {
        var me = (rsp.scope||this);
        
        if (rsp==='no') {
            return;
        }
        
        var ts = new Date();
        var auar = me.main.data.getProvider("activeuar");
        var ats = me.main.data.getProvider("activetestsession");
        // Get the active uar and it's descendants
        var report = this.main.data.map[auar.a];

        // Get the template from the dcf Review form and apply test session data to it
        var tpl = new Ext.XTemplate(me.main.data.getObjectByName('UAROutput').ct,me.main.util.getTemplateFunctions());
        var output = tpl.apply(report);
        
        
        me.main.data.create({
            callback: me.submituarFeedback,
            component: me,
            scope: me,
            data: [{
                cid: 'document',
                pa: ats.a,
                l: 'Unsuccessful Attempt Report ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                t: 0,
                timestamp: ts,
                doctype: 'uar',
                ct: output,
                a: ats.a + '.uar'
            }]
        });
        
        // Update test session status
        me.main.actions.perform({
            cmd: "put",
            component: me,
            objects: [ats],
            _Status: 'COMPLETED',
            UAR_completed: true,
            DocumentGenerated: ts
        });
    },
    
    submituarFeedback: function (rsp) {
        var me = (rsp.scope||this);
        
        me.gopreviousdashboardHandler(rsp);
        
        Ext.defer(function () {
            this.main.feedback.say({
                title: 'Success',
                message: 'Unsuccessful Attempt Report saved.'
            })
        },
        1200,
        this)
    },

    
    /* -----------------------------------------------------------------------
    startaar:
    Starts an After Action Report (AAR) for the test session:  If a AAR
    already exists for the test session it is used again, otherwise a new AAR
    document is created; Starts providers for the sub-objects in the AAR.
    __________________________________________________________________________ */
    
    startaarHandler: function (sp) {
        
        var par = this.main.data.getProvider("activetestsession");
        
        // Reset all providers
        //this.main.data.setProvider("activeaarlocation",{});
        
        // Find if the test session already contains an AAR
        var desc = this.main.data.getDescendants(par.a);
        var arr = this.main.data.filter(desc,'cid EQ aar');
        
        // Get the labs that the shipped samples were sent to and date sent
        var ucla, smrtl = false;
        var sdate;
        var uarr = this.main.data.filter(desc,'cid EQ urinesample');
        for (var i=0,l=uarr.length;i<l;i++) {
            if (uarr[i].SampleStatus=='SHIPPED') {
                if (uarr[i].EPO=='1' || uarr[i].CIRIRMS=='1' || uarr[i].GHRP=='1') ucla=true;
                if (uarr[i].EPO=='2' || uarr[i].CIRIRMS=='2' || uarr[i].GHRP=='2') smrtl=true;
                sdate = uarr[i].DateShipped;
            }
        }
        
        var barr = this.main.data.filter(desc,'cid EQ bloodsample');
        for (var i=0,l=barr.length;i<l;i++) {
            if (barr[i].SampleStatus=='SHIPPED') {
                if (barr[i].Parameters=='1' || barr[i].HGH=='1' || barr[i].HGH2=='1' || barr[i].HBOC=='1' || barr[i].CERA=='1' || barr[i].HBT=='1' || barr[i].EPOBlood=='1') ucla=true;
                if (barr[i].Parameters=='2' || barr[i].HGH=='2' || barr[i].HGH2=='2' || barr[i].HBOC=='2' || barr[i].CERA=='2' || barr[i].HBT=='2' || barr[i].EPOBlood=='2') smrtl=true;
                sdate = barr[i].DateShipped;
            }
        }
        
        if (arr.length > 0){
//            var aardesc = this.main.data.getDescendants(arr[0].ca);
//            var locs = this.main.data.filter(aardesc,'cid EQ location');
//            var comb = arr.concat(locs);
            //this.main.data.setProvider("activeaarlocation",{});
            this.startaarCallback(arr);
        } else {
            var atharr = this.main.data.filter(desc,'cid EQ athlete');
            var apn = '';
            apn = apn.concat(atharr[0].FirstName,' ',atharr[0].LastName);

            // Create AAR and pre-populate values from the completed test session
            this.main.data.create({
                callback: this.startaarCallback,
                component: sp.component,
                scope: this,
                data: [{
                    cid: 'aar',
                    l:  'After Action Report - ' + apn,
                    pa: par.a,
                    t: 0,
                    LOCTArrivalTime: par.ArrivalTime,
                    LOCTNotificationTime: par.NotificationTime,
                    LOCTAttemptEnd: par.ProcessingTime,
                    LOCTCity: par.EventCity || '',
                    LOCTState: par.EventState || '',
                    BCOPrintedName: par.BCOPrintedName || '',
                    AfterActionUCLA: ucla,
                    AfterActionSMRTL: smrtl,
                    AfterActionDateSent: sdate
                }]
            });
        }
        
    },
    
    startaarCallback: function (rsp) {
    
        // set the aar to the activeaar provider and open the aar form
        var me = (rsp.scope||this);
        var ts = me.main.data.getProvider("activetestsession");
        var aar, loc1;
        if (rsp.objects) {
            // A new AAR object was created
            aar = rsp.objects[0];
        } else {
            // AAR already exists
            aar = rsp[0];
        }
    
        me.main.data.setProvider("activeaar",aar.a);
        
        me.main.actions.perform({
            cmd: 'request',
            source: 'aarpanel',
            target: 'userinterface',
            provider: 'activeaar'
        });
    },
    
    completedtestlocationCallback: function (rsp) {
        var me = (rsp.scope||this);
        me.main.data.setProvider("completedtestlocation",rsp.objects[0].a);
        // Create object for the completed test location phone call
        me.main.data.create({
            callback: me.completedtestphoneCallback,
            component: rsp.cmp,
            scope: me,
            data: [{
                cid: 'phonecall',
                l: 'Completed Test Phone Call',
                pa: rsp.objects[0].a,
                t: 0
            }]
        });
    },

    completedtestphoneCallback: function (rsp) {
        var me = (rsp.scope||this);
        me.main.data.setProvider("completedtestphonecall",rsp.objects[0].a);
    },
    
    /* -----------------------------------------------------------------------
    aarchaperone:
    Toggles disabled/enabled state of chaperone roles fieldset in aar
    __________________________________________________________________________ */
    
    aarchaperoneHandler: function (sp) {
        var aar = this.main.data.getProvider("activeaar");
        var roles = this.main.util.get('aarrolesfs');
        
        if (aar.AfterActionChapPresent=="Yes") {
            roles.enable(true);
        } else {
            roles.disable(true);
        }
        
        this.main.rules.broadcastTopic({topic:'requiredchange'});
    },
    
     /* -----------------------------------------------------------------------
    athleteuncooperative:
    Sets response field to required if athlete is uncooperative
    __________________________________________________________________________ */
       
    athleteuncooperativeHandler: function (sp) {
        var aar = this.main.data.getProvider("activeaar");
        var comments = this.main.util.get('AAProcessingComments');
        
        if (aar.AfterActionCooperative=="No") {
           comments.required=true;
        } else {
            comments.required=false;
        }

        this.main.rules.broadcastTopic({topic:'refresh',component:comments});
        this.main.rules.broadcastTopic({topic:'requiredchange'});
        
    },
    
    /* -----------------------------------------------------------------------
    submitaar:
    Submits an After Action Report (AAR): creates AAR document
    __________________________________________________________________________ */
    
    submitaarHandler: function (sp) {
        var aar = this.main.data.getProvider("activeaar");
        var ats = this.main.data.getProvider("activetestsession");
        // timestamp
        var ts = new Date();

        // Get the active arr and it's descendants
        var report = this.main.data.map[aar.a];

        // Get the template from the dcf Review form and apply test session data to it
        var tpl = new Ext.XTemplate(this.main.data.getObjectByName('AAROutput').ct,this.main.util.getTemplateFunctions());
        var output = tpl.apply(report);
        
        // Create the After Action Report inside of the active test session
        this.main.data.create({
            callback: this.submitaarFeedback,
            component: this,
            scope: this,
            data: [{
                cid: 'document',
                pa: ats.a,
                l: 'After Action Report ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                t: 0,
                timestamp: ts,
                doctype: 'aar',
                ct: output,
                a: ats.a + '.aar'
            }]
        });
        
        // Update test session status
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [ats],
            AfterAction_completed: 1
        });
    },
    
    submitaarFeedback: function (rsp) {
        var me = (rsp.scope||this);
        
        me.gopreviousdashboardHandler(rsp);
        
        Ext.defer(function () {
            this.main.feedback.say({
                title: 'Success',
                message: 'After Action Report saved.'
            })
        },
        1200,
        this)
    },
    
    /* -----------------------------------------------------------------------
    submitsession:
    Saves the test session: Updates the test session status to COMPLETED; creates
    dcor document; Sends email to athlete; builds samples
    __________________________________________________________________________ */
    
    submitsessionHandler: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
        var ts = new Date();
	var ds = Ext.Date.format(ts,'m-d-Y h:i A');
        
        // Update test session status
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [ats],
            _Status: 'COMPLETED',
            DocumentGenerated: ts,
            CheckedOut: 'Completed'
        });
        
        // Get the active test session and it's descendants
        var report = this.main.data.map[ats.a];
        
        // Get the template from the dcf Review form and apply test session data to it
        var tpl = new Ext.XTemplate(this.main.data.getObjectByName('dcfReview').ct,this.main.util.getTemplateFunctions());
        var output = tpl.apply(report);
	output = output.concat(this.main.localization.translate('languagedisclaimer'), ds, "</div>");

	var combinedoutput = ''; 
	// If athlete is UFC or Professional Boxing create the dcor receipt
	if (ats.Sport=='155'||ats.Sport=='139') {
		var receipttpl = new Ext.XTemplate(this.main.data.getObjectByName('dcorReceipt').ct,this.main.util.getTemplateFunctions());
		var receipt = receipttpl.apply(report);
		combinedoutput = output.concat(receipt);
	}


        var desc = this.main.data.getDescendants(ats.ca);
        
        // Get the lab screens for the test session
        var sarr = this.main.data.filter(desc, 'cid EQ labscreen');
        
        // Get the urine samples for the test session
        var uarr = this.main.data.filter(desc, 'cid EQ urinesample');
        
        var up = {
            cmd: 'put',
            component: this,
            objects: uarr
        };
        
        // Add urine screens to the urine samples
        for (var i=0,l=sarr.length;i<l;i++) {
            var sname = this.main.localization.lookupValue("screen",sarr[i].ScreenTypeId,"ShortName");
            var tmethod = this.main.localization.lookupValue("screen",sarr[i].ScreenTypeId,"DefaultTestMethod");
            if (/Urine/.test(String(tmethod))) {
                switch (String(sarr[i].ScreenTypeId)) {
                    case '1':
                        up[sname] = 'full';
                        break;
                    case '2':
                        up[sname] = 'partial';
                        break;
                    default:
                        up[sname] = sarr[i].LabId;
                        up[sname+'_caption'] = this.main.localization.lookupValue("testinglab",sarr[i].LabId,"ShortName");
                }
            }
        }

        this.main.actions.perform(up);

        
        // Calculate the UTC date/time and set object label to sample code
        for (var i=0,l=uarr.length;i<l;i++) {
            var tempdate = uarr[i].UrineSealedDate;
            var temptime = uarr[i].UrineSealedTime;
            // Calculate UTC date/time
            if (Ext.isDate(tempdate)&&Ext.isDate(temptime)) {
                var utcdate = new Date(tempdate.getUTCFullYear(), tempdate.getUTCMonth(), tempdate.getUTCDate(), temptime.getUTCHours(), temptime.getUTCMinutes(), temptime.getUTCSeconds());
                var utctime = new Date(tempdate.getUTCFullYear(), tempdate.getUTCMonth(), tempdate.getUTCDate(), temptime.getUTCHours(), temptime.getUTCMinutes(), temptime.getUTCSeconds());
            }

            this.main.actions.perform({
                cmd: "put",
                component: this,
                objects: [uarr[i]],
		l: uarr[i].UrineSampleCode,
                SampleStatus: 'AVAILABLE',
                ProcessDate_gmt: this.main.util.dateToISO8601(utcdate),
                ProcessTime_gmt: this.main.util.dateToISO8601(utctime)
            });
           
        }


        // Calculate athletes age to include with blood samples
        // Get the athlete attached to the test session
        var aarr = this.main.data.filter(desc, 'cid EQ athlete');
        var now = new Date();
        var age = '';
	var apn = ''; 
        var apnfl = apn.concat(this.main.data.getDerivedValue(aarr[0],'FirstName'),', ',this.main.data.getDerivedValue(aarr[0],'LastName'));
	var apnlf = apn.concat(this.main.data.getDerivedValue(aarr[0],'LastName'),', ',this.main.data.getDerivedValue(aarr[0],'FirstName'));
	var apncmb = apn.concat(this.main.data.getDerivedValue(aarr[0],'FirstName'),' ',this.main.data.getDerivedValue(aarr[0],'LastName'));
	var pemail = this.main.data.getDerivedValue(aarr[0],'PrimaryEmail');
        if (!Ext.isEmpty(aarr[0].DOB)){
            age = this.main.util.getAge(aarr[0].DOB,now);
        }

        // Update status of blood samples
        var barr = this.main.data.filter(desc, 'cid EQ bloodsample');
        
        var bp = {
            cmd: 'put',
            component: this,
            objects: barr,
            Age: age
        };
        
       
        this.main.actions.perform(bp);
        
	var garr = [];
	var parr = [];

        for (var i=0,l=barr.length;i<l;i++) {
	    // Sort blood samples by kit color so we can apply screens
	    if (barr[i].SampleKitColor=="Purple") parr.push(barr[i]);
	    if (barr[i].SampleKitColor=="Gold") garr.push(barr[i]);

            // Calculate the UTC date/time and set the object label to the sample code
            var tempdate = barr[i].BloodSealedDate;
            var temptime = barr[i].BloodSealedTime;
            // Calculate UTC date/time
            if (Ext.isDate(tempdate)&&Ext.isDate(temptime)) {
                var utcdate = new Date(tempdate.getUTCFullYear(), tempdate.getUTCMonth(), tempdate.getUTCDate(), temptime.getUTCHours(), temptime.getUTCMinutes(), temptime.getUTCSeconds());
                var utctime = new Date(tempdate.getUTCFullYear(), tempdate.getUTCMonth(), tempdate.getUTCDate(), temptime.getUTCHours(), temptime.getUTCMinutes(), temptime.getUTCSeconds());
            }

            this.main.actions.perform({
                cmd: "put",
                component: this,
                objects: [barr[i]],
                SampleStatus: 'AVAILABLE',
		l: barr[i].BloodSampleCode,
                ProcessDate_gmt: this.main.util.dateToISO8601(utcdate),
                ProcessTime_gmt: this.main.util.dateToISO8601(utctime)
            });
           
        }

	var gp = {
		cmd: 'put',
		component: this,
		objects: garr
	};

	var pp = {
		cmd: 'put',
		component: this,
		objects: parr
	};

  	// Add blood screens to the blood samples
        for (var i=0,l=sarr.length;i<l;i++) {
            var sname = this.main.localization.lookupValue("screen",sarr[i].ScreenTypeId,"ShortName");
            var tmethod = this.main.localization.lookupValue("screen",sarr[i].ScreenTypeId,"DefaultTestMethod");

	    switch (sarr[i].ScreenTypeId){
		    case 4:
			    gp[sname] = sarr[i].LabId;
			    gp[sname+'_caption'] = this.main.localization.lookupValue("testinglab",sarr[i].LabId,"ShortName");
			    break;
		    case 7:
			    pp[sname] = sarr[i].LabId;
			    pp[sname+'_caption'] = this.main.localization.lookupValue("testinglab",sarr[i].LabId,"ShortName");
			    break;
		    case 8:
			    pp[sname] = sarr[i].LabId;
			    pp[sname+'_caption'] = this.main.localization.lookupValue("testinglab",sarr[i].LabId,"ShortName");
			    break;
		    case 11:
			    gp[sname] = sarr[i].LabId;
			    gp[sname+'_caption'] = this.main.localization.lookupValue("testinglab",sarr[i].LabId,"ShortName");
			    if (garr.length==0){
			    	pp[sname] = sarr[i].LabId;
			    	pp[sname+'_caption'] = this.main.localization.lookupValue("testinglab",sarr[i].LabId,"ShortName");
			    }
			    break;
		    case 12:
			    gp[sname] = sarr[i].LabId;
			    gp[sname+'_caption'] = this.main.localization.lookupValue("testinglab",sarr[i].LabId,"ShortName");
			    if (garr.length==0){
			    	pp[sname] = sarr[i].LabId;
			    	pp[sname+'_caption'] = this.main.localization.lookupValue("testinglab",sarr[i].LabId,"ShortName");
			    }
			    break;
		    case 15:
			    gp[sname] = sarr[i].LabId;
			    gp[sname+'_caption'] = this.main.localization.lookupValue("testinglab",sarr[i].LabId,"ShortName");
			    break;
		    case 17:
			    gp[sname] = sarr[i].LabId;
			    gp[sname+'_caption'] = this.main.localization.lookupValue("testinglab",sarr[i].LabId,"ShortName");
			    if (garr.length==0){
			    	pp[sname] = sarr[i].LabId;
			    	pp[sname+'_caption'] = this.main.localization.lookupValue("testinglab",sarr[i].LabId,"ShortName");
			    }
			    break;
		    default:
            		if (/Blood-Serum/.test(String(tmethod))) {
				gp[sname] = sarr[i].LabId;
				gp[sname+'_caption'] = this.main.localization.lookupValue("testinglab",sarr[i].LabId,"Shortname");
			} else if (/Blood-EDTA/.test(String(tmethod))) {
				pp[sname] = sarr[i].LabId;
				pp[sname+'_caption'] = this.main.localization.lookupValue("testinglab",sarr[i].LabId,"ShortName");
			}
			break;
	    }
        }
   
        this.main.actions.perform(gp);
        this.main.actions.perform(pp);

        var dcorlbl = 'DCOR ';
       	dcorlbl = (ats.designation=='nonevent') ? dcorlbl.concat(ds) : dcorlbl.concat(apnfl, ' - ', ds);

	// If not a paper based test, did athlete elect to receive forms by email?
	var senddcor = (ats.papertest) ? false : ats.emailforms;

	// Set up the message for the email
	var emailmsg = this.main.localization.translate('athleteemailmessage');
	var surveylink = (ats.Sport=='155') ? '<a href="https://www.surveymonkey.com/r/2016UFCAthlete?DCOID='+ ats.DCOPNFinal + '"><b>ATHLETE SURVEY</b></a>'
 : '<a href="https://www.surveymonkey.com/r/Athlete2016?DCOID='+ ats.DCOPNFinal + '"><b>ATHLETE SURVEY</b></a>';

	// Need to create the DCOR and Receipt combo to email to UFC and Proboxing athletes
	if (combinedoutput!='') {
		var receiptlbl = apnlf.concat(' - ', ds); 

		// This will be emailed to athlete instead of dcor
		senddcor = false;

		// Create the combined DCOR/Receipt document to send to the athlete
		this.main.data.create({
			callback: Ext.emptyFn,
			component: this,
			scope: this,
			data: [{
				cid: 'document',
				pa: ats.a,
				l: receiptlbl.concat(' - DCOR Combined'),
				t: 0,
				notification: pemail,
				AthletePrintedName: apncmb,
				message: emailmsg.concat(surveylink),
				subject: this.main.localization.translate('athleteemailsubject'),
				emailforms: ats.emailforms,
				timestamp: ts,
				doctype: 'dcorcombined',
				ct: combinedoutput,
				a: ats.a + '.dcorcombined'
			}]
		});

		// Create the DCOR Receipt to send to USADA
		this.main.data.create({
			callback: Ext.emptyFn,
			component: this,
			scope: this,
			data: [{
				cid: 'document',
				pa: ats.a,
				l: receiptlbl.concat(' - DCOR Receipt'),
				t: 0,
				notification: 'DCORReceipts@usada.org',
				AthletePrintedName: apncmb,
				message: receiptlbl.concat(' - DCOR Receipt'),
				subject: receiptlbl.concat(' - DCOR Receipt'),
				emailforms: true,
				timestamp: ts,
				doctype: 'dcorreceipt',
				ct: receipt,
				a: ats.a + '.dcorreceipt'
			}]
		});
	}


        var cb = (ats.designation=='event') ? Ext.bind(this.setrostercompleteCallback,this,[ats],1) : Ext.bind(this.submitsessionCallback,this);
       // Create the DCOR object. Only email this if the athlete has opted to receive it and they are not a UFC or Professional Boxing athlete 
        this.main.data.create({
            //callback: this.submitsessionCallback,
	    callback: cb,
            component: this,
            scope: this,
            data: [{
                cid: 'document',
                pa: ats.a,
                l: dcorlbl,
                t: 0,
                notification: pemail,
		AthletePrintedName: apncmb,
		message:emailmsg.concat(surveylink), 
		subject: this.main.localization.translate('athleteemailsubject'),
		emailforms: senddcor,
                timestamp: ts,
                doctype: 'dcor',
                ct: output,
                a: ats.a + '.dcor'
            }]
        });
        
        
    },
    
    submitsessionCallback: function (rsp) {
        var me = (rsp.scope||this);
        
        me.gopreviousdashboardHandler(rsp);
        
        Ext.defer(function () {
            me.main.feedback.say({
                title: 'Success',
                message: 'Test Session saved.'
            })
        },
        1200,
        me)
    },
    
    setrostercompleteCallback: function (rsp,ats) {
        var me = (rsp.scope||this);
        
        var ts = new Date();
        // Get an array of all the samples attached to the manifest
	var desc = me.main.data.getChildren(me.main.data.getProvider("activeevent").ca);
        //var desc = rsp.leaf.par.children;
        var srarr = me.main.data.filter(desc, 'cid EQ siteroster');
        var ar = '';

	for (var i=0,l=srarr.length;i<l;i++) {
		if (srarr[i].testalias==ats.ca){
		       	ar=srarr[i];
			break;
		}
	}

	if (ar) {

            me.main.actions.perform({
                cmd: "put",
                component: rsp.component,
                objects: [ar],
                RosterHistory: 'Completed',
                timestamp: ts
            });
               
           // Create a ledger entry to check athlete into site roster
            me.main.data.create({
                component: rsp.component,
                scope: me,
                data: [{
                    cid: 'ledgerentry',
                    l:  'Ledger Entry ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                    pa: ar.a,
                    t: 0,
                    Timestamp: ts,
                    RosterHistory: 'Completed'
                }]
            });
	}
	me.gopreviousdashboardHandler(rsp);
        
        Ext.defer(function () {
            me.main.feedback.say({
                title: 'Success',
                message: 'Test Session saved.'
            })
        },
        1200,
        me)

    },
    
    /* -----------------------------------------------------------------------
    selectallcompletedtests:
    Selects all completed tests in the OOC sync queue
    __________________________________________________________________________ */
   
    selectallcompletedtestsHandler: function (sp) {
   	var tgrid = this.main.util.get('ooccompletedtestsgrid');
        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        
        // Get the selected urine samples
        for (var i=0,l=data.length;i<l;i++) {
     		data[i].selectedtestsession=true;
		store.getAt(i).set('selectedtestsession', true);
	}

	grid.reconfigure(store);

    }, 

    /* -----------------------------------------------------------------------
    selectalltestsessions:
    Selects all test sessions in the IC sync queue
    __________________________________________________________________________ */
   
    selectalltestsessionsHandler: function (sp) {
   	var tgrid = this.main.util.get('iccompletedtestsgrid');
        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        
        // Get the selected urine samples
        for (var i=0,l=data.length;i<l;i++) {
     		data[i].selectedtestsession=true;
		store.getAt(i).set('selectedtestsession', true);
	}

	grid.reconfigure(store);

    }, 


    /* -----------------------------------------------------------------------
    submit completed tests :
    Check that all required docs are completed, create json packet, update 
    test session status
    __________________________________________________________________________ */
    
    submitcompletedtestsHandler: function (sp) {
    
        sp.component.setDisabled(true);
	    
	var gname = (sp.component.n=='submitOOCbutton') ? 'ooccompletedtestsgrid' : 'iccompletedtestsgrid';
        var tgrid = this.main.util.get(gname);
        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        var tarray = [];
        var earray = [];
        var tlist='';
        
        // Check that required docs are complete
        for (var i=0,l=data.length;i<l;i++) {
            var upass = false, spass = false, apass = false;
            if (data[i].selectedtestsession) {
                if (data[i].UAR_completed) {
                    upass = true;
                } else {
                    if (data[i].SampleShipment_completed) {
                        spass = true;
                    } else {
                        earray.push("All samples for " + data[i].FirstName + ' ' + data[i].LastName + " test session have not been shipped.");
                    }
                    if (data[i].designation=='event' || data[i].AfterAction_completed) {
                        apass = true;
                    } else {
                        earray.push("After action report for " + data[i].FirstName + ' ' + data[i].LastName + " test session has not been completed.");
                    }
                }
           
                if (upass || (spass && apass)) tarray.push(data[i]);
            }
        }
        
        if (earray.length > 0) {
            var elist = earray.join('<br>');
            this.main.feedback.say({
                title: 'Submission Cancelled',
                message: 'Some of the selected tests could not be submitted:<br>' + elist
            });
	    sp.component.setDisabled(false);
            return;
        }
        
	if (tarray.length==0) {
		this.main.feedback.say({
			title: 'Error',
			message: 'You must select at least one test session to submit.'
		});
		sp.component.setDisabled(false);
		return;
	}

        for (var i=0,l=tarray.length;i<l;i++) {
            // Get the raw objects for test session and docs
            // otherwise json encode will fail
            var ts = [];
            var desc = this.main.data.getDescendants(tarray[i].a);
            for (index in desc) {
                var o = this.main.data.getObject(desc[index].a);
                ts.push(o);
            }
            var s = Ext.JSON.encode(ts);
           
            // write json data to completed tests folder
            this.main.data.create({
                component: sp.component,
                scope: this,
                data: [{
                    cid: 'testpacket',
                    l:  tarray[i].TSID,
                    pa: this.main.environment.get("COMPLETEDTESTS_FOLDER"),
		    n: 'testpacket.' + tarray[i].TSID,
                    t: 0,
                    ct: s
                }]
            });
           
           
            // Update the status of the test session
            this.main.actions.perform({
                cmd: "put",
                component: this,
                objects: [tarray[i]],
                _Status: 'CLOSED'
            });
           
        }
        
	sp.component.setDisabled(false);
        this.main.feedback.say({
            title: 'Success',
            message: 'Selected tests have been submitted.'
        });
        
        this.main.util.get(gname).loadData();

    },
    
    
    
    /* -----------------------------------------------------------------------
    submitpostsupplementary:
    Saves the post supplementary report: Updates the test session status to COMPLETED; creates
    dcor document; Sends email to athlete; builds samples
    __________________________________________________________________________ */

    submitpostsupplementaryHandler: function (sp) {
        
        var aps = this.main.data.getProvider("activepostsupplementary");
        var ats = this.main.data.getProvider("activetestsession");
        // timestamp
        var ts = new Date();

        // Get the active arr and it's descendants
        var report = this.main.data.map[aps.a];

        // Get the template from the dcf Review form and apply test session data to it
        var tpl = new Ext.XTemplate(this.main.data.getObjectByName('PostSuppOutput').ct,this.main.util.getTemplateFunctions());
        var output = tpl.apply(report);
        
        // Create the After Action Report inside of the active test session
        this.main.data.create({
            callback: this.submitpostsupplementaryFeedback,
            component: this,
            scope: this,
            data: [{
                cid: 'document',
                pa: ats.a,
                l: 'Post Supplementary Report ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                t: 0,
                timestamp: ts,
                doctype: 'postsupp',
                ct: output
            }]
        });
        
        // Update test session status
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [ats],
            PostSupplementary_completed: 1
        });
    },
    
    submitpostsupplementaryFeedback: function (rsp) {
        var me = (rsp.scope||this);
        
        me.gopreviousdashboardHandler(rsp);
        
        Ext.defer(function () {
            this.main.feedback.say({
                title: 'Success',
                message: 'Post Supplementary Report saved.'
            })
        },
        1200,
        this)
    },

    /* -----------------------------------------------------------------------
    setenglish:
    Changes the internal language to english
    __________________________________________________________________________ */

    setenglishHandler: function (sp) {
//        this.main.feedback.say({
//            title: 'Change Language',
//            message: 'This will set the language to English'
//        });
        this.main.environment.set("language","en-us","local");
        this.main.data.reset();
        this.main.render.reload();
    },
    
    /* -----------------------------------------------------------------------
    setspanish:
    changes the internal language to spanish
    __________________________________________________________________________ */

    setspanishHandler: function (sp) {
//        this.main.feedback.say({
//            title: 'Change Language',
//            message: 'This will set the languge to Spanish'
//        });
        this.main.environment.set("language","es-mx","local");
        this.main.data.reset();
        this.main.render.reload();
    },

    /* -----------------------------------------------------------------------
    setportuguese:
    Changes the internal language to portuguese
    __________________________________________________________________________ */

    setportugueseHandler: function (sp) {
//        this.main.feedback.say({
//            title: 'Change Language',
//            message: 'This will set the langugae to Portuguese'
//        });
        this.main.environment.set("language","pt-br","local");
        this.main.data.reset();
        this.main.render.reload();
    },
    
    /* -----------------------------------------------------------------------
    toggleonline:
    Toggles the app between offline and online mode
    __________________________________________________________________________ */
    
    //toggleonlineHandler: function (sp) {
    //    this.main.feedback.say({
    //        title: 'Online Indicator',
    //        message: 'This will toggle the app between online and offline mode'
    //    });
    //},
    
/* -----------------------------------------------------------------------
    showalerts:
    Display Paperless alerts
    __________________________________________________________________________ */
    
    showalertsHandler: function (sp) {
        /*this.main.feedback.say({
            title: 'Alerts',
            message: 'This will display any unviewed alerts'
        });*/
	if (this.main.environment.isNative()) {
            var v = AppVersion.version;
            var b = AppVersion.build;
	} else {
		v = 'only available in native build.';
		b = 'only available in native build.';
	}
        this.main.feedback.say({
            title: 'Paperless',
            message: 'Version ' + v + '<br>Build ' + b
        });

        //this.main.feedback.ask({
        //    title: 'Name of item you want to debug',
        //    mesaage: 'what item do you want?',
        //    callback: this.debugCallback,
        //    scope: this
        //});
        
    },

    debugHandler: function (sp) {
        
        this.main.feedback.ask({
            title: 'Name of item you want to debug',
            mesaage: 'What item do you want?',
            callback: this.debugCallback,
            scope: this
        });
    },
    
    debugCallback: function (sp,s) {
        debugger;
        var mainobject = this.main.data.getObject(s)||this.main.data.getObjectByName(s);
        var mappedobject = this.main.data.map[mainobject.a];
        var cachedobject;
        var me = this.main.cache;
        
        me.db.transaction(function (tx) {
            tx.executeSql("SELECT value FROM data WHERE a=?", [mainobject.a], function(tx, results) {
                for (var i=0,l=results.rows.length; i<l; i++) {
                    var row = results.rows.item(i);
                    cachedobject = me.decrypt(Ext.decode(row['value']));
                }
                debugger;
            });
        });
    },

    
    /* -----------------------------------------------------------------------
    logout:
    Logs user out of Paperless
    __________________________________________________________________________ */
    
    logoutuserHandler: function (sp) {
	    var v = AppVersion.version;
	    var b = AppVersion.build;
        this.main.feedback.say({
            title: 'Paperless',
            message: 'Version ' + v
        });
	cmd: logout
    },
    
    /* -----------------------------------------------------------------------
    startmanifest:
    Opens the mainfest form and displays waybill photo
    __________________________________________________________________________ */
    startmanifestHandler: function (sp) {
        var am = this.main.data.getProvider("activemanifest");
        
        //debugger;
        (sp.designation=='nonevent') ? this.main.environment.set("smsource",this.main.entity.getUser().a) : this.main.environment.set("smsource",this.main.data.getProvider("activeevent").a);
        
        var desc = this.main.data.getDescendants(am.ca);
        var parr = this.main.data.filter(desc, 'cid EQ document');
        
        var photo = this.main.data.getObjectByName('waybillphoto');
        var cbtn = this.main.data.getObjectByName('clearphotobtn');
           
        if (parr.length>0){
            photo.ct = parr[0].ct;
            cbtn.disabled = false;
           
           this.main.actions.perform({
                cmd: 'setbusy',
                component: this.main.view
            });
        } else {
            photo.ct = "";
            cbtn.disabled = false;
        }
        
        this.main.actions.perform({
            cmd: 'goto',
            redirectsource: 'samplemanifestpanel',
            target: 'userinterface'
        });
        

    },
    
    /* -----------------------------------------------------------------------
    removemanifestsamples:
    Removes the link between the samples linked to the manifest and resets the
    status of the samples
    __________________________________________________________________________ */

    removemanifestsamplesHandler: function (sp) {
debugger;
        //var am = this.main.data.getProvider("activemanifest");
	var am = sp.value;
	var cb = Ext.bind(this.removemanifestsamplesCallback,this);

	// find the samples that were linked to the manifest
	if (this.main.environment.getOnline()) {
        	this.main.actions.perform({
        	    cmd: 'request',
        	    a: am.ca,
        	    relationship: 'stepparents',
        	    callback: cb,
        	    scope: this,
        	    component: sp.component
        	});
	} else {
		Ext.callback(cb,this,[{
			leaf: {stepparents: this.main.data.getStepParents(am)},
			scope: this,
			component: sp.component
		}]);
	}
    },

    //removeactiveictestCallback: function (rsp) {
    //    var me = (rsp.scope||this);
    //    me.main.data.move({
    //   	    component: rsp.component,
    //   	    scope: me,
    //   	    source: rsp.leaf,
    //   	    destination: me.main.environment.get("RECYCLEBIN")
    //   	});

    //    me.main.util.get('activesessionsgrid').loadData();
    
    removemanifestsamplesCallback: function (rsp) {
 	var me = (rsp.scope||this);
        var am = rsp.leaf;
       debugger; 
        // Get an array of all the samples attached to the manifest
        var desc = rsp.leaf.stepparents;
        var uarr = me.main.data.filter(desc, 'cid EQ urinesample');
        var barr = me.main.data.filter(desc, 'cid EQ bloodsample');
        var sarr = uarr.concat(barr);
        var cb = Ext.bind(me.resetsamplesCallback,me);
        
        for (var i=0,l=sarr.length;i<l;i++) {
	    if (me.main.environment.getOnline()){
            	me.main.actions.perform({
            	    cmd: 'request',
            	    a: sarr[i].ca,
            	    relationship: 'item',
            	    callback: cb,
            	    scope: me,
            	    component: rsp.component
            	});
	    } else {
		    Ext.callback(cb,me,[{
			    leaf: me.main.data.getSource(sarr[i]),
			    scope: me,
			    component: rsp.component
		    }]);
	    }
        }
    },

    resetsamplesCallback: function (rsp) {
	var me = (rsp.scope||this);
        var as = rsp.leaf;
       debugger; 
	var desc = me.main.data.getDescendants(as.ca);
        var asm = me.main.data.filter(desc, 'cid EQ samplemanifest');
   
        me.main.data.move({
            component: rsp.component,
            scope: me,
            source: asm[0],
            destination: me.main.environment.get("RECYCLEBIN")
        });
        
	me.main.actions.perform({
            cmd: 'put',
            component: rsp,
            objects: [rsp.leaf],
            SampleStatus: 'AVAILABLE'
        });

          
    },

    /* -----------------------------------------------------------------------
    submitmanifest:
    Saves the Sample Manifest: creates manifest document; marks samples as 
    shipped;
    __________________________________________________________________________ */
    
    submitmanifestHandler: function (sp) {
        var ts = new Date();
        var am = this.main.data.getProvider("activemanifest");
    //TODO check that at least one sample is selected before allowing them to submit 

        // Get the active manifest and it's descendants
        var report = this.main.data.map[am.a];
        
        // Get the template and apply the manifest form data to it
        var tpl = new Ext.XTemplate(this.main.data.getObjectByName('SampleManifestOutput').ct,this.main.util.getTemplateFunctions());
        var output = tpl.apply(report);

        // Create the sample manifest document
        this.main.data.create({
            callback: this.submitmanifestCallback,
            component: this,
            scope: this,
            data: [{
                cid: 'document',
                pa: am.a,
                l: 'Sample Manifest ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                t: 0,
                timestamp: ts,
                doctype: 'samplemanifest',
                ct: output,
                a: am.a + '.samplemanifest'
            }]
        });
        
    },
    

    
    submitmanifestCallback: function (rsp) {
        var me = (rsp.scope||this);
        var am = me.main.data.getProvider("activemanifest");
        
        // bind the sample manifest document to the callback
        var doc = rsp.objects[0];
        var cb = Ext.bind(me.pullsamplesCallback,me,[doc],1);
        
        // find the samples that were linked to the manifest
	if (me.main.environment.getOnline()) {
        	me.main.actions.perform({
        	    cmd: 'request',
        	    a: am.ca,
        	    relationship: 'stepparents',
        	    callback: cb,
        	    scope: me,
        	    component: rsp.component
        	});
	} else {
		Ext.callback(cb,me,[{
			leaf: {stepparents: me.main.data.getStepParents(am)},
			scope: me,
			component: rsp.component
		}]);
	}
    },
    
    pullsamplesCallback: function (rsp,doc) {
        debugger;
        var me = (rsp.scope||this);
        var am = me.main.data.getProvider("activemanifest");
        
        // Get an array of all the samples attached to the manifest
        var desc = rsp.leaf.stepparents;
        var uarr = me.main.data.filter(desc, 'cid EQ urinesample');
        var barr = me.main.data.filter(desc, 'cid EQ bloodsample');
        var sarr = uarr.concat(barr);
        
	// Check that user selected at least one sample
	if (sarr.length<1) {
		this.main.feedback.say({
                	title: 'Error',
                	message:'You must select at least one blood or urine sample.'
            	});
		return;
	}

        var cb = Ext.bind(me.processsampleCallback,me,[doc],1);
        
        for (var i=0,l=sarr.length;i<l;i++) {
	    if (me.main.environment.getOnline()){
            	me.main.actions.perform({
            	    cmd: 'request',
            	    a: sarr[i].ca,
            	    relationship: 'item',
            	    callback: cb,
            	    scope: me,
            	    component: rsp.component
            	});
	    } else {
		    Ext.callback(cb,me,[{
			    leaf: me.main.data.getSource(sarr[i]),
			    scope: me,
			    component: rsp.component
		    }]);
	    }
        }
           
        // Update the status of the manifest and move to the allmanifests folder
        me.main.actions.perform({
            cmd: "put",
            component: me,
            objects: [am],
            ManifestStatus: 'COMPLETED',
            pa: me.main.environment.get('ALLSAMPLEMANIFESTS_FOLDER')
        });
        
        me.gopreviousdashboardHandler(rsp);
    },
    
    processsampleCallback: function (rsp,doc) {
        // Marks sample as shipped, adds shipment date
        var me = (rsp.scope||this);
        var sample = rsp.leaf;
        var ts = me.main.data.getParent(sample);
        var am = me.main.data.getProvider("activemanifest");
 
        //Build a lab xml template.
        var obj = me.main.data.getObjectByName('labxmltemplate');
        var s = (obj) ? obj.ct : '';
        var tpl = new Ext.XTemplate(s,me.main.util.getTemplateFunctions());

 
        me.main.actions.perform({
            cmd: 'put',
            component: me,
            objects: [sample],
            SampleStatus: 'SHIPPED',
            DateShipped: am.ShipmentDate,
            ShippingLab: am.ShippingLab, 
	    ShippingLab_caption: me.main.localization.lookupValue('testinglab',am.ShippingLab,'ShortName'),
	    MonitorNumber: am.MonitorNumber,
            ct: tpl.apply(sample)
        });
        
        // link the sample manifest doc to the test session
        // that the sample is parented by
        me.main.data.createLink({
            parent: ts,
            child: doc
        });
        
        me.checktestsamplescomplete(rsp,ts);
    },
    
    checktestsamplescomplete: function (rsp,ts) {
        // Check if all samples for this test session
        // have been shipped
        var me = (rsp.scope||this);
        
        // get all of the samples for the test session
        var desc = me.main.data.getDescendants(ts.ca);
        var uarr = me.main.data.filter(desc, 'cid EQ urinesample');
        var barr = me.main.data.filter(desc, 'cid EQ bloodsample');
        var sarr = uarr.concat(barr);
        var done = true;
        
        for (var i=0,l=sarr.length;i<l;i++) {
           if (sarr[i].SampleStatus!='SHIPPED') done = false;
        }
        
        if (done) {
            me.main.actions.perform({
                cmd: 'put',
                component: me,
                objects: [ts],
                SampleShipment_completed: true
            });
        }
    },
    
    /* -----------------------------------------------------------------------
    selectallusamples:
    Selects all available samples in the completed Urine Samples grid
    __________________________________________________________________________ */
   
    selectallusamplesHandler: function (sp) {
   	var tgrid = this.main.util.get('completedurinesamples');
        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        
        // Get the selected urine samples
        for (var i=0,l=data.length;i<l;i++) {
     		data[i].selectedusamples=true;
		store.getAt(i).set('selectedusamples', true);
	}

	grid.reconfigure(store);
        //this.main.util.get('completedurinesamples').loadData();

    }, 

    /* -----------------------------------------------------------------------
    saveusampleselection:
    Saves the selected Urine Samples to the sample manifest
    __________________________________________________________________________ */
    
    saveusampleselectionHandler: function (sp) {
        var am = this.main.data.getProvider("activemanifest");
        //var auc = this.main.data.getProvider("activeurinecompleted");

        var tgrid = this.main.util.get('completedurinesamples');
        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        
        // Get the selected urine samples
        for (var i=0,l=data.length;i<l;i++) {
            if (data[i].selectedusamples) {
        
                // Link the selected sample to the sample manifest
                this.main.data.createLink({
                    parent: data[i],
                    child: am
                });
                
                // Update the status of the sample
                this.main.actions.perform({
                    cmd: "put",
                    component: this,
                    objects: [data[i]],
                    SampleStatus: 'SELECTED'
                });
	    }
	}
        
        // Refresh the grids
	this.main.cache.invalidate(tgrid.sourcealias);
        tgrid.loadData();
        this.main.util.get('urinesamplesgrid').loadData();
        this.main.util.get('usamplesanalysisgrid').loadData();
        
    },
    
    /* -----------------------------------------------------------------------
    removeselectedusample:
    Removes the selected Urine Samples from the sample manifest
    __________________________________________________________________________ */
    
    removeselectedusampleHandler: function (sp) {
    
        var uas = this.main.data.getProvider("activeurineselected");
        
	var desc = this.main.data.getDescendants(uas.ca);
        var asm = this.main.data.filter(desc, 'cid EQ samplemanifest');
        
        if (asm.length==0) {
            debugger;
        }
   
        this.main.data.move({
            component: sp.component,
            scope: this,
            source: asm[0],
            destination: this.main.environment.get("RECYCLEBIN")
        });
        
        this.main.actions.perform({
            cmd: 'request',
            a: uas.ca,
            relationship: 'item',
            callback: this.removeselectedusampleCallback,
            scope: this,
            component: sp.component
        });
           
    },

    removeselectedusampleCallback: function (rsp) {
        var me = (rsp.scope||this);
           
        me.main.actions.perform({
            cmd: 'put',
            component: rsp,
            objects: [rsp.leaf],
            SampleStatus: 'AVAILABLE'
        });
        
      // Refresh the grids
        me.main.util.get('completedurinesamples').loadData();
        me.main.util.get('urinesamplesgrid').loadData();
        me.main.util.get('usamplesanalysisgrid').loadData();
    },
    
    /* -----------------------------------------------------------------------
    selectallbsamples:
    Selects all available samples in the completed Blood Samples grid
    __________________________________________________________________________ */
   
    selectallbsamplesHandler: function (sp) {
   	var tgrid = this.main.util.get('completedbloodsamples');
        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        
        // Get the selected urine samples
        for (var i=0,l=data.length;i<l;i++) {
     		data[i].selectedbsamples=true;
		store.getAt(i).set('selectedbsamples', true);
	}

	grid.reconfigure(store);

    }, 


    /* -----------------------------------------------------------------------
    savebsampleselection:
    Saves the selected Blood Samples to the sample manifest
    __________________________________________________________________________ */
    
    savebsampleselectionHandler: function (sp) {
        var am = this.main.data.getProvider("activemanifest");
 
	var bsgrid = this.main.util.get('bloodsamplesgrid');
	var bagrid = this.main.util.get('banalysissamplesgrid');

        var tgrid = this.main.util.get('completedbloodsamples');
        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        
        // Get the selected urine samples
        for (var i=0,l=data.length;i<l;i++) {
            if (data[i].selectedbsamples) {
        
          
                // Link the selected sample to the sample manifest
                this.main.data.createLink({
                    parent: data[i],
                    child: am
                });
                
                // Update the status of the sample
                this.main.actions.perform({
                    cmd: "put",
                    component: this,
                    objects: [data[i]],
                    SampleStatus: 'SELECTED'
                });
	    }
	}
        
        // Refresh the grids
        this.main.cache.invalidate(tgrid.sourcealias);
        tgrid.loadData();
	this.main.cache.invalidate(bsgrid.sourcealias);
        bsgrid.loadData();
	this.main.cache.invalidate(bagrid.sourcealias);
        bagrid.loadData();
        
    },
    
    /* -----------------------------------------------------------------------
    removeselectedbsample:
    Removes the selected Blood Samples from the sample manifest
    __________________________________________________________________________ */
    
    removeselectedbsampleHandler: function (sp) {
    
//    debugger;
    
        var abs = this.main.data.getProvider("activebloodselected");
        
	var desc = this.main.data.getDescendants(abs.ca);
        var asm = this.main.data.filter(desc, 'cid EQ samplemanifest');
   
        this.main.data.move({
            component: sp.component,
            scope: this,
            source: asm[0],
            destination: this.main.environment.get("RECYCLEBIN")
        });
        
        this.main.actions.perform({
            cmd: 'request',
            a: abs.ca,
            relationship: 'item',
            callback: this.removeselectedbsampleCallback,
            scope: this,
            component: sp.component
        });
           
    },

    removeselectedbsampleCallback: function (rsp) {
        var me = (rsp.scope||this);
        
//        debugger;
        
        me.main.actions.perform({
            cmd: 'put',
            component: rsp,
            objects: [rsp.leaf],
            SampleStatus: 'AVAILABLE'
        });
        
      // Refresh the grids
        var cbgrid = me.main.util.get('completedbloodsamples');
	me.main.cache.invalidate(cbgrid.sourcealias);
	cbgrid.loadData();
        var bsgrid = me.main.util.get('bloodsamplesgrid');
	me.main.cache.invalidate(bsgrid.sourcealias);
	bsgrid.loadData();
        var bagrid = me.main.util.get('banalysissamplesgrid');
	me.main.cache.invalidate(bagrid.sourcealias);
	bagrid.loadData();
    },
    
    /* -----------------------------------------------------------------------
    Waybill:
    Allows user to upload a photo of the waybill document for the sample manifest.
    User can take a photo of the waybill using the device camera, or select a
    photo from the photo library on the device
    __________________________________________________________________________ */
    
    selectwaybillphotoHandler: function (sp) {
        var cb = Ext.bind(this.onPhotoURISuccess,this);
//        debugger;
        navigator.camera.getPicture(cb, this.onPhotoFail, {
            quality: 50,
            destinationType: 0,
            encodingType: 0,
            targetWidth: 850,
            targetHeight: 1100,
            correctOrientation: false,
            sourceType: 0
         });
    },
    
    takewaybillphotoHandler: function () {
        var cb = Ext.bind(this.onPhotoURISuccess,this);
        navigator.camera.getPicture(cb, this.onPhotoFail, {
            quality: 50,
            destinationType: 0,
            encodingType: 0,
            targetWidth: 850,
            targetHeight: 1100,
            correctOrientation: false,
            sourceType: 1
        });
    },
    
    onPhotoURISuccess: function (dataURL) {

        this.photofile = dataURL;
        var htmldata = '<img src="data:image/jpeg;base64,' + dataURL + '" style="max-width:100%" />';
        var waybillphoto = this.main.util.get('waybillphoto');
        waybillphoto.ct = htmldata;
        
        waybillphoto.loadData();
        
        this.main.util.get('clearphotobtn').enable(true);
        this.main.util.get('uploadwaybillbtn').enable(true);
    },
    
    onPhotoFail: function (message) {
//        alert(message);
        this.fireEvent('cmd',{
            cmd: 'say',
            component: this,
            title: 'Photo',
            message: message
        });
    },
    
    deletephotoHandler: function (sp) {
        var waybill = this.main.util.get('waybillphoto');
        waybill.ct = '';
        waybill.loadData();
        
        this.main.util.get('clearphotobtn').disable(true);
        this.main.util.get('uploadwaybillbtn').disable(true);
    },
    
    uploadphotoHandler: function (sp) {
        var ts = new Date();
        var am = this.main.data.getProvider('activemanifest');
        var waybill = this.main.util.get('waybillphoto');
        sp.component.disable(true);
        
        var desc = this.main.data.getDescendants(am.ca);
        var parr = this.main.data.filter(desc, 'cid EQ document');
        
        this.main.actions.perform({
            cmd: 'setbusy',
            component: this.main.view
        });

        if (parr.length>0) {
            var w = parr[0];
            parr[0].ct = waybill.ct;
            // Update the image in the waybill document
            this.main.actions.perform({
                cmd: "put",
                component: this,
                objects: [w],
                ct: waybill.ct
            });
           this.uploadphotoCallback(sp);
        } else {
            // Create waybill document
            this.main.data.create({
                callback: this.uploadphotoCallback,
                component: this,
                scope: this,
                data: [{
                    cid: 'document',
                    pa: am.a,
                    l: 'Waybill ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                    t: 0,
                    timestamp: ts,
                    doctype: 'waybill',
                    ct: waybill.ct,
                    a: am.a + '.waybill'
                }]
            });
        }
    },
    
    uploadphotoCallback: function (rsp) {
        // Create links to manifest
        var me = (rsp.scope||this);
        
        me.main.actions.perform({
            cmd: 'removebusy',
            component: me.main.view
        });
        
        Ext.defer(function () {
            me.main.feedback.say({
                title: 'Success',
                message: 'Waybill uploaded to server.'
            })
        },
        800,
        me)
        
    },
    
    /* -----------------------------------------------------------------------
    Checkout/Checkin:
    Checkout or Checkin an In-Competition test session.  When a test is checkout
    by a DCO it becomes locked so that no other DCO can start the test.  Checkin
    returns the test session to the pool of available test sessions.
    __________________________________________________________________________ */
    
    checkoutHandler: function (sp) {
        var ts = new Date();
        
        var ats = this.main.data.getProvider("activetestsession");
        var status = ats.CheckedOut;
        var dconame = this.main.entity.getFullName();
        if ((!status)||(status==dconame)) {
            this.main.actions.perform({
                cmd: "put",
                component: this,
                objects: [ats],
                CheckedOut: dconame,
//                CheckedOut: 'tech',
                CheckedOutTS: ts
            });
            this.startdcfHandler(sp);
        } else {
            this.main.feedback.say({
                title: 'Error',
                message: 'This test session is in use.'
            });
        }
        
    },
    
    checkInActiveSessionHandler: function (sp) {
        var ts = new Date();
        
        var ats = this.main.data.getProvider("activetestsession");
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [ats],
            CheckedOut: false,
            CheckedOutTS: ts
        });

        this.gopreviousdashboardHandler();

    },
    
    /* -----------------------------------------------------------------------
    View History
    For In Competition Events open the site roster history for the athlete 
    __________________________________________________________________________ */
    
    viewrosterhistoryHandler: function (sp) {

	var asr = this.main.data.getProvider("activesiteroster");
	var cb = Ext.bind(this.viewrosterhistoryCallback,this);
	this.main.actions.perform({
          	cmd: 'request',
            	a: asr.ca,
            	relationship: 'children',
            	callback: cb,
            	scope: this,
            	component: sp.component
       	});
    },

    viewrosterhistoryCallback: function (rsp) {
	var me = (rsp.scope||this);
	me.main.actions.perform({
            cmd: 'goto',
            redirectsource: 'siterosterhistoryform',
            target: 'userinterface'
        });
        

    },

    /* -----------------------------------------------------------------------
    Add Athlete:
    For In Competition Events opens a form so that the DCO can manually add an
    athletes information to the athlete queue.
    __________________________________________________________________________ */
    
    startAddAthleteHandler: function (sp) {
    
        var asr = this.main.data.getProvider("activesiteroster");
        
	if (asr.RosterHistory=='Completed') {
		this.main.feedback.say({
			title: 'Edit Athlete',
			message: 'This test session has been completed and the athlete information cannot be edited.'
		});
		return;
	}

        //var desc = this.main.data.getDescendants(asr.ca);
        //var arr = this.main.data.filter(desc, 'cid EQ athlete');
    
        //if (arr.length>0) {
	if (asr.testalias) {
            this.main.feedback.say({
                title: 'Edit Athlete',
                message: 'A test session has already been created for this athlete.  Use the active test session to edit athlete information.'
            });
            return;
        } else {
            this.main.actions.perform({
                cmd: 'request',
                source: 'addathletepanel',
                target: 'userinterface',
                provider: asr
             });
        }
         
         // TODO: check that an athlete object link does not exist u
    
//        var ae = this.main.data.getProvider("activeevent");
//        
//        // create the new site roster to add data to
//        this.main.data.create({
//            callback: this.startAthleteCallback,
//            component: this,
//            scope: this,
//            data: [{
//                cid: 'siteroster',
//                pa: ae.a,
//                l: 'New Site Roster',
//                t: 0,
//                RosterStatus: 'CREATED'
//            }]
//        });
    },
    
//    startAthleteCallback: function (rsp) {
//        var me = (rsp.scope||this);
//        
//        var ar = rsp.objects[0];
//        me.main.data.setProvider("activeroster",ar.a);
//        
//        me.main.actions.perform({
//            cmd: 'request',
//            source: 'addathletepanel',
//            target: 'userinterface',
//            provider: ar.a
//        });
//    },

    searchAthletesHandler: function () {
    
        var searchresults = this.main.view.down("*[name='selectathletegrid']");
    
        var lastnamefield = this.main.view.down("*[name='athletesearchlastname']");
        var lastname = (lastnamefield) ? lastnamefield.getValue() : '';
        
        var sportfield = this.main.view.down("*[name='athletesearchsport']");
        var sport = (sportfield) ? sportfield.getValue() : '';
        
        var sp = {
            source: 'allathletes',
            relationship: 'descendants',
            $cid: 'athlete'
        }
//        if (lastname) sp['$LastName'] = lastname;
//        if (sport) sp['$Sport'] = sport;
        if (lastname) sp['ix1'] = lastname;
        if (sport) sp['ix2'] = sport;

        //debugger;
        
        searchresults.loadData(sp);
    
    },
    /* -----------------------------------------------------------------------
    Add Selected Athlete to Queue:
    For In Competition Events allows the user to add a new athlete to the athlete
    pool by selecting the athlete from a list of all athletes.
    __________________________________________________________________________ */
    
    addSelectedAthleteToQueueHandler: function (sp) {
//        var ath = this.main.data.getProvider("pickedathlete");
        
        var ae = this.main.data.getProvider("activeevent");
        
        // Find the selected athletes in the grid
        var agrid = this.main.util.get('selectathletegrid');
        var grid = agrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        var atharray = [];
        
        // Add the selected athletes to an array
        for (var i=0,l=data.length;i<l;i++) {
           if (data[i].selectforpool) {
               atharray.push(data[i]);
           }
        }
        
        // Check that at least one athlete was selected
        if (atharray.length==0) {
            this.main.feedback.say({
                title: 'Add Selected to Pool',
                message: 'You must select at least one athlete to add to the athlete pool.'
            });
            return;
        }

        // Check that the selected athletes are not already in the site roster
        var desc = this.main.data.getDescendants(ae.ca);
        var aearr = this.main.data.filter(desc, 'cid EQ athlete');
        var foundlist = '';
        for (var item in atharray) {
            var x = this.main.data.filter(aearr, 'PersonId EQ ' + atharray[item].PersonId);
            if (x.length>0) {
                foundlist = (foundlist=='') ? atharray[item].l : foundlist + ', ' + atharray[item].l;
            }
        }
        
        if (foundlist!='') {
            this.main.feedback.say({
                title: 'Add Selected to Pool',
                message: 'The following athletes are already in the site roster:<br>' + foundlist + '<br>Remove these athletes from the selection and try again.'
            });
            return;
        }
        
        for (var i=0,l=atharray.length;i<l;i++){
            var ath = atharray[i];
           
           var cb = Ext.bind(this.addSelectedAthleteCallback,this,[ath],1);
           
            // create the new site roster to add data to
            this.main.data.create({
                callback: cb,
                component: this,
                scope: this,
                data: [{
                    cid: 'siteroster',
                    pa: ae.a,
                    l: 'Roster Entry - ' + ath.LastName,
                    t: 0,
                    RosterStatus: 'CREATED',
                    FirstName: ath.FirstName,
                    LastName: ath.LastName,
                    Sport: ath.Sport,
                    Discipline: ath.Discipline
                }]
            });
        }
        
        this.main.actions.perform({
            cmd: 'goto',
            redirectsource: 'athletelistform',
            target: 'userinterface'
        });
        
        //this.main.feedback.say({
        //    title: 'Add Selected to Pool',
        //    message: atharray.length + ' athletes added to Athlete Pool'
        //});

    },
    
    addSelectedAthleteCallback: function (rsp,ath) {
        var me = (rsp.scope||this);
        
        var ar = rsp.objects[0];
//        var ath = me.main.data.getProvider("pickedathlete");
        
        // Link the athlete to the site roster
        me.main.data.createLink({
            parent: ar,
            child: ath
        });
        
        
    },
    
    /* -----------------------------------------------------------------------
    Add Athletes to Queue:
    For In Competition Events adds the manually added athlete to the site roster and
    creates new test sessions for the athlete.
    __________________________________________________________________________ */
    
    addAthleteManuallyHandler: function (sp) {
        
        var ts = new Date();
        var ae = this.main.data.getProvider("activeevent");
        var asr = this.main.data.getProvider("activesiteroster");
        
        if (!Ext.isDefined(asr.FirstName) || asr.FirstName=="") {
            this.main.feedback.say({
               title: 'Add Test Session',
               message: 'You must enter athletes first name.'
            });
            return;
        }
        
        if (!Ext.isDefined(asr.LastName) || asr.LastName=="") {
            this.main.feedback.say({
                title: 'Add Test Session',
                message: 'You must enter athletes last name.'
            });
            return;
        }
        
        if (!Ext.isDefined(asr.Sport) || asr.Sport=="") {
            this.main.feedback.say({
                title: 'Add Test Session',
                message: 'You must select a Sport.'
            });
            return;
        }
        
        if (!Ext.isDefined(asr.Discipline) || asr.Discipline=="") {
            this.main.feedback.say({
                title: 'Add Test Session',
                message: 'You must select a Discipline.'
            });
            return;
        }

	// Get the Testing and Results Authorities from the sportauthority objects
	var ra,ta = 'USADA';
	var raln,taln = 'United States Anti-Doping Agency';
	var saarr = this.main.data.filter(this.main.data.getChildren(ae),"cid EQ sportauthority");
	for (var i=0,l=saarr.length;i<l;i++) {
		if (saarr[i].SportId == asr.Sport) {
			ra = saarr[i].ResultsAuthorityAbbreviation;
			ta = saarr[i].TestingAuthorityAbbreviation;
			raln = saarr[i].TestingAuthorityLongName;
			taln = saarr[i].ResultsAuthorityLongName;
		}
	}
        
        // Update the Site roster entry for the athlete
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [asr],
            RosterStatus: 'OPENED',
            RosterHistory: 'Checked In',
            timestamp: ts,
	    l: 'Roster Entry - ' + asr.LastName
        });
       
       // Create a ledger entry to check athlete into site roster
        this.main.data.create({
            component: this,
            scope: this,
            data: [{
                cid: 'ledgerentry',
                l:  'Ledger Entry ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                pa: asr.a,
                t: 0,
                Timestamp: ts,
                RosterHistory: 'Checked In'
            }]
        });
        
        var cb = Ext.bind(this.addAthleteManuallyCallback,this);
        
        // Create the test session
        this.main.data.create({
            callback: cb,
            component: this,
            scope: this,
            data: [{
                cid: 'testsession',
                l: 'Test Session ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                pa: ae.a,
                t: 0,
                ArrivalDate: ts,
                ArrivalTime: ts,
                EventID: ae.EventID,
                TestingAuthority: ta,
                ResultsManagementAuthority: ra,
		TestingAuthorityLongName: taln,
		ResultsAuthorityLongName: raln,
		SampleCollectionAuthorityLongName: 'United States Anti-Doping Agency',
		CollectionAuthority: 'USADA',
                SiteID: ae.SiteId,
                EventType: 'IC',
                designation: 'event',
                Urine: true,
                UrineType: 'full',
                Status: 'ASSIGNED',
                CheckedOut: false,
                Sport: asr.Sport,
                Sport_caption: asr.Sport_caption,
                Discipline: asr.Discipline,
                Discipline_caption: asr.Discipline_caption
            }]
        });
    },
    
    addAthleteManuallyCallback: function (rsp) {
        
        var me = (rsp.scope||this);
        var ts = rsp.objects[0];
        var asr = me.main.data.getProvider("activesiteroster");
        
	// Add the test session alias to the roster so that
	// the test can be removed if the athlete is removed
	// from the site roster
	me.main.actions.perform({
                cmd: "put",
                component: rsp.component,
                objects: [asr],
                testalias: ts.a
            });

        var cb = Ext.bind(me.linkAthleteManuallyCallback,me,[ts],1);

        // Create an athlete object for this test
        me.main.data.create({
            callback: cb,
            component: rsp.component,
            scope: me,
            data: [{
                cid: 'athlete',
                l: asr.LastName + ' ' + asr.FirstName,
                pa: ts.a,
                t: 0,
                FirstName: asr.FirstName,
                LastName: asr.LastName,
                Sport: asr.Sport_caption,
                Discipline: asr.Discipline_caption
            }]
        });
        
    },
        
    linkAthleteManuallyCallback: function (rsp,ts) {
    
        var me = (rsp.scope||this);
        var asr = me.main.data.getProvider("activesiteroster");

        //debugger;
        // Link the athlete to the site roster
        me.main.data.createLink({
            parent: asr,
            child: rsp.objects[0]
        });
        
        this.gopreviousdashboardHandler();
        
    },
    
    /* -----------------------------------------------------------------------
    selectallathletes:
    Selects all athletes in the athlete pool
    __________________________________________________________________________ */
   
    selectallathletesHandler: function (sp) {
   	var tgrid = this.main.util.get('athletespool');
        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        
        // Get the selected urine samples
        for (var i=0,l=data.length;i<l;i++) {
     		data[i].selecteathlete=true;
		store.getAt(i).set('selectedathlete', true);
	}

	grid.reconfigure(store);

    }, 


    /* -----------------------------------------------------------------------
    Add Athletes to Roster:
    For In Competition Events adds the selected athletes to the site roster and
    creates new test sessions for the athletes.
    __________________________________________________________________________ */
    
    addAthletesToRosterHandler: function (sp) {
    
        var ts = new Date();
        var ae = this.main.data.getProvider("activeevent");

    
        // Find the selected athletes in the grid
        var agrid = this.main.util.get('athletespool');
        var grid = agrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        var atharray = [];
        
        // Add the selected athletes to an array
        for (var i=0,l=data.length;i<l;i++) {
           if (data[i].selectedathlete) {
               atharray.push(data[i]);
           }
        }
        
        if (atharray.length==0) {
            this.main.feedback.say({
                title: 'Add Selected to Site Roster',
                message: 'You must select at least one athlete from the athlete pool.'
            });
            return;
        }

        for (var i=0,l=atharray.length;i<l;i++){
            	var ar = atharray[i];
        	var cb = Ext.bind(this.createSessionCallback,this);
		this.main.actions.perform({
            		cmd: 'request',
            		a: ar.ca,
            		relationship: 'children',
            		callback: cb,
            		scope: this,
            		component: sp.component
        	});
	}
    },

    createSessionCallback: function (rsp) {

            var me = (rsp.scope||this);
    	    var ts = new Date();
	    var ar = rsp.leaf;
	    var aa = rsp.desc[0];
            var ae = me.main.data.getProvider("activeevent");

            // Check that athlete is linked to roster entry
            if (!Ext.isDefined(aa)) {
		    me.main.feedback.say({
			    title: 'Site Roster',
			    message: 'Error: roster entry not associated with a valid athlete.'
		    });
                return;
            }
           
	    
            
	    // Update the Site roster entry for the athlete
            me.main.actions.perform({
                cmd: "put",
                component: rsp.component,
                objects: [ar],
                RosterStatus: 'OPENED',
                RosterHistory: 'Checked In',
                timestamp: ts
            });
           
           // Create a ledger entry to check athlete into site roster
            me.main.data.create({
                component: rsp.component,
                scope: me,
                data: [{
                    cid: 'ledgerentry',
                    l:  'Ledger Entry ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                    pa: ar.a,
                    t: 0,
                    Timestamp: ts,
                    RosterHistory: 'Checked In'
                }]
            });
           
            var cb = Ext.bind(me.addTestSessionCallback,me,[aa,ar],1);
        
            // Create a new test session
            me.main.data.create({
                callback: cb,
                component: rsp.component,
                scope: me,
                data: [{
                    cid: 'testsession',
                    l: 'Test Session ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                    pa: ae.a,
                    t: 0,
                    ArrivalDate: ts,
                    ArrivalTime: ts,
                    EventID: ae.EventID,
                    SiteID: ae.SiteId,
                    EventType:'IC',
                    designation: 'event',
                    Urine: true,
                    UrineType: 'full',
                    Status: 'ASSIGNED',
                    CheckedOut: false
                }]
            });
        
        
        me.gopreviousdashboardHandler();
    },
    
    addTestSessionCallback: function (rsp,aa,ar) {
        
        var me = (rsp.scope||this);
        var ts = rsp.objects[0];
        
	// Add the test session alias to the roster so that
	// the test can be removed if the athlete is removed
	// from the site roster
	me.main.actions.perform({
                cmd: "put",
                component: rsp.component,
                objects: [ar],
                testalias: ts.a
            });

        var cb = Ext.bind(me.linkTestSessionCallback,me,[ts],1);

        // Request the athele object so we can link to this test session
        me.main.actions.perform({
            cmd: 'request',
            a: aa.ca,
            relationship: 'descendants',
            callback: cb,
            scope: me,
            component: rsp.component
        });
        
    },
        
    linkTestSessionCallback: function (rsp,ts) {
    
        var me = (rsp.scope||this);

	var ae = me.main.data.getProvider("activeevent");
//        debugger;
        // Link the athlete to the new test session record
        me.main.data.createLink({
            parent: ts,
            child: rsp.leaf
        });
        
        // Lookup the id values to write to the test session
        var sportval = rsp.leaf.Sport || '';
        var sportId = '';
        if (sportval) sportId = me.main.localization.reverselookupValue('sport','Sport',sportval,'SportId');
        
        var disciplineval = rsp.leaf.Discipline || '';
        var disciplineId = '';
        if (disciplineval) disciplineId = me.main.localization.reverselookupValue('discipline','Discipline',disciplineval,'DisciplineId');
        
	// Get the Testing and Results Authorities from the sportauthority objects
	    var ra,ta = 'USADA';
	    var raln, taln = 'United States Anti-Doping Agency';
	    var saarr = me.main.data.filter(me.main.data.getChildren(ae),"cid EQ sportauthority");
	    for (var i=0,l=saarr.length;i<l;i++) {
	    	if (saarr[i].SportId == sportId) {
	    		ra = saarr[i].ResultsAuthorityAbbreviation;
	    		ta = saarr[i].TestingAuthorityAbbreviation;
			raln = saarr[i].ResultsAuthorityLongName;
			taln = saarr[i].TestingAuthorityLongName;
	    	}
	    }
        
        // Add athlete info to test session if it exists.
        this.main.actions.perform({
            cmd: "put",
            component: rsp,
            objects: [ts],
            Sport: sportId,
            Sport_caption: sportval,
            Discipline: disciplineId,
            Discipline_caption: disciplineval,
            TestingAuthority: ta,
            ResultsManagementAuthority: ra,
	    TestingAuthorityLongName: taln,
	    ResultsAuthorityLongName: raln,
	    SampleCollectionAuthorityLongName: 'United States Anti-Doping Agency',
	    CollectionAuthority: 'USADA',
	    AddressType: rsp.leaf.AddressType||'',
            ResidenceAddr1: rsp.leaf.Addr1||'',
            ResidenceCity: rsp.leaf.City||'',
            ResidenceState: rsp.leaf.State||'',
            ResidenceZip: rsp.leaf.Zip||'',
            ResidenceCountry_caption: rsp.leaf.Country||''
        });
        
    },

    checkactiveictestHandler: function (sp) {
	// Get the test session associated with this roster entry
	var tsa = sp.value.data.testalias;
	
	var cb = Ext.bind(this.checkactiveictestCallback,sp.scope,[sp.callback,sp.value,sp.idx],1);

	this.main.actions.perform({
                cmd: 'request',
                a:tsa, 
                relationship: 'item',
                callback: cb,
                scope: sp.scope,
                component: sp.cmp
            });

    },

    checkactiveictestCallback: function (rsp,cb,value,idx) {
	// Check that the test session associated with the roster entry hasn't been 
	// completed, started, or checked out before removing the test and roster entry
        var me = (rsp.scope||this);
	var msg = '';
	if (rsp.leaf._Status=='COMPLETED'){
		// Test session has been completed
		//me.main.feedback.say({
                //	title: 'Cannot Delete Roster Entry',
                //	message:'The test session associated with this roster entry has been completed.'
            //});
   	    Ext.Msg.alert('Cannot Delete Roster Entry','The test session associated with this roster entry has been completed.');
            return false;
	} else if (rsp.leaf._Status=='OPENED') {
		// Test session has been started
		//me.main.feedback.say({
		//	title: 'Cannot Delete Roster Entry',
		//	message:'The test session associated with this roster entry has already been started.'
		//});
		Ext.Msg.alert('Cannot Delete Roster Entry','The test session associated with this roster entry has already been started.');
		return false;
	} else if (rsp.leaf.CheckedOut!=false) {
		// test session is checked out to someone
		//me.main.feedback.say({
		//	title: 'Cannot Delte Roster Entry',
		//	message:'The test session associated with this roster entry is currently in use.'
		//});
		Ext.Msg.alert('Cannot Delte Roster Entry','The test session associated with this roster entry is currently in use.');
		return false;
	}

	Ext.callback(cb,rsp.scope,[value,idx]);

    }, 

    removeactiveictestHandler: function (sp) {
	var tsa = sp.value.testalias;
	
	this.main.actions.perform({
                cmd: 'request',
                a:tsa, 
                relationship: 'item',
                callback: this.removeactiveictestCallback,
                scope: this,
                component: sp.cmp
            });

    },

    removeactiveictestCallback: function (rsp) {
	var me = (rsp.scope||this);
	me.main.data.move({
       	    component: rsp.component,
       	    scope: me,
       	    source: rsp.leaf,
       	    destination: me.main.environment.get("RECYCLEBIN")
       	});

        me.main.util.get('activesessionsgrid').loadData();


    },
    /* -----------------------------------------------------------------------
    Update person roster status:
    Toggles the roster status for the active support person and creates a new 
    roster entry.
    __________________________________________________________________________ */
    
    updatePersonRosterStatusHandler: function (sp) {
    
        var ap = this.main.data.getProvider("activeperson");
        
        var newstatus = (ap.RosterHistory=='Checked In') ? 'Checked Out' : 'Checked In';
        
        var cb = Ext.bind(this.updatePersonStatusCallback,this,sp.component,1);
        Ext.Msg.show({
            title: 'Update Site Roster Status',
            message: 'Update the Site Roster status for this person to ' + newstatus + '?',
            buttons: Ext.Msg.OKCANCEL,
            fn: cb
        });
    },
    
    updatePersonStatusCallback: function (rsp,cmp) {
        var me = (rsp.scope||this);
        var ap = me.main.data.getProvider("activeperson");
        
        if (rsp==='ok') {
            me.doUpdateRosterStatus(ap,cmp);
        }
        
    },
    
    /* -----------------------------------------------------------------------
    Update athlete roster status:
    Updates the site roster status for the athlete: toggles between Checked In/
    Checked Out; adds a new roster entry.
    __________________________________________________________________________ */
    
    updateAthleteRosterStatusHandler: function (sp) {
        var ar = this.main.data.getProvider("activesiteroster");
        
	if (ar.RosterHistory=='Completed') {
	    this.main.feedback.say({
                title: 'Update Site Roster Status',
                message:'This test session has been completed.'
            });
            return;
	}

        var newstatus = (ar.RosterHistory=='Checked In') ? 'Checked Out' : 'Checked In';
        
        var tgrid = this.main.util.get('siterostergrid');
        var grid = tgrid.down("grid");

        
        var cb = Ext.bind(this.updateAthleteStatusCallback,this,[tgrid],1);
        Ext.Msg.show({
            title: 'Update Site Roster Status',
            message: 'Update the Site Roster status for this athlete to ' + newstatus + '?',
            buttons: Ext.Msg.OKCANCEL,
            fn: cb
        });
//        this.doUpdateRosterStatus(ar);
    },
    
    updateAthleteStatusCallback: function (rsp,cmp) {
        var me = (rsp.scope||this);
        var asr = me.main.data.getProvider("activesiteroster");
        
        if (rsp==='ok') {
           me.doUpdateRosterStatus(asr,cmp);
        }
    },
    
    doUpdateRosterStatus: function (rsp,cmp) {
        var me = (rsp.scope||this);
        
        var ts = new Date();
        var newstatus = (rsp.RosterHistory=='Checked In') ? 'Checked Out' : 'Checked In';
        
        me.main.actions.perform({
            cmd: "put",
            component: me,
            objects: [rsp],
            RosterHistory: newstatus
        });
       
        //Create a ledger entry for this person
        me.main.data.create({
            component: me,
            scope: me,
            data: [{
                cid: 'ledgerentry',
                l:  'Ledger Entry ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                pa: rsp.a,
                t: 0,
                Timestamp: ts,
                RosterHistory: newstatus
            }]
        });
        
        var arr = me.main.render.getHistory();
        
        for (var i=arr.length-1,l=0;i>l;i--){
            //var item = arr.pop();
	    var item = arr[i];
            var src = item.source||item.redirectsource;
            me.main.actions.perform(item);
            break;
        }
           
        //        cmp.loadData();
        
//        me.main.actions.perform({
//            cmd: 'request',
//            source: 'icdashboard',
//            target: 'userinterface'
//        });
    },
    
    updateAthleteRosterEntourageHandler: function (sp) {
    
    },
    
    /* -----------------------------------------------------------------------
    Offer Assignment DCO:
    Enables RTL user to offer a test session assignment to the selected DCO:
    Checks AssignmentAPI for the test session status; Writes status update to
    AssignmentAPI; Reparents test session to DCO; Updates test session status.
    __________________________________________________________________________ */
    
    offerassignmentdcoHandler: function (sp) {
         
        // Get the DCO from the combobox
        var cbox = this.main.util.get('transferDCO');
        var aDCO = cbox.getRawValue();
        var dcoID = cbox.findRecordByDisplay(aDCO).data.value;
        
        // Find the selected test sessions in the grid
        var tgrid = this.main.util.get('rtlunassignedgrid');
        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        var tarray = [{'DCO':aDCO,'DCOId':dcoID}];
        var tlist='';
        
        for (var i=0,l=data.length;i<l;i++) {
           if (data[i].TransferTestSession) {
               tarray.push(data[i]);
           }
        }
        
        if (tarray.length==1) {
            this.main.feedback.say({
                title: 'Offer Assignment',
                message: 'You must select at least one test session assignment.'
            });
            return;
        }
        
        for (var i=1,l=tarray.length;i<l;i++) {
            tlist = (tlist=='') ? tarray[i].AthletePrintedName : tlist + '<br>' + tarray[i].AthletePrintedName;
        }
        
        var cb = Ext.bind(this.doofferassignmentcheck,this,[tarray],1);
        // Verify selection with user
        Ext.Msg.show({
            title: 'Offer Assignment',
            message: 'The following assignments will be offered to ' + aDCO + ':<br>' + tlist,
            buttons: Ext.Msg.OKCANCEL,
            fn: cb
        });
        
    },
    
    doofferassignmentcheck: function (rsp,tdata) {
        var me = (rsp.scope||this);
        
        if (rsp==='ok') {
//            debugger;

            var DCOID = tdata[0].DCOId;

            for (var i=1,l=tdata.length;i<l;i++){
           
                var tsid = tdata[i].TSID;
                // bind callback function, attaching the test session object as
                // an additional argument
                var cb = Ext.bind(me.doofferassignmentCallback,me,tdata[i],1);
           
                // TESTING
                var rval = {"applicationid":"testsessionassignment","type":"success","message":"","data":[{"testsessionid":"8675309","dcoid":"1","statusid":"9","status":"Offered","statusdate":"today"}]};
                me.doofferassignmentCallback(rval,[tdata[0],tdata[i]]);
           
                // write test session assignment to API
//                me.main.actions.perform({
//                    cmd: "task",
//                    n: "testsessionassignment",
//                    dcoid: DCOID,
//                    statusid: "9",
//                    testsessionid: tsid,
//                    callback: cb,
//                    scope: me
//                });
            }
           
            me.main.actions.perform({
                cmd: 'request',
                source: 'icdashboard',
                target: 'userinterface'
            });
           
        }
    },
    
    doofferassignmentCallback: function (rsp,tdata) {
        var me = (rsp.scope||this);
        
        var ts = new Date();
        
        // Update the status and reset the parent
        if (rsp.type=="success") {
            var pdco = 'dco.' + tdata[0].DCOId;
            me.main.actions.perform({
                cmd: "put",
                component: me,
                objects: [tdata[1]],
                _Status: 'OFFERED',
                OfferedDt: ts,
                pa: pdco
            });
        }

    },
    
    /* -----------------------------------------------------------------------
    Transfer Assignment DCO:
    Enables RTL user to transfer a test session assignment to the selected region:
    Checks AssignmentAPI for the test session status; Writes status update to
    AssignmentAPI; Reparents test session to region; Updates test session status.
    __________________________________________________________________________ */
    
    transferassignmentdcoHandler: function (sp) {
         
        // Get the DCO from the combobox
        var cbox = this.main.util.get('transferRegion');
        var aRegion = cbox.getRawValue();
        var regionID = cbox.findRecordByDisplay(aRegion).data.value;
        
        // Find the selected test sessions in the grid
        var tgrid = this.main.util.get('rtlunassignedgrid');
        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        var tarray = [{'Region':aRegion,'RegionId':regionID}];
        var tlist='';
        
        for (var i=0,l=data.length;i<l;i++) {
           if (data[i].TransferTestSession) {
               tarray.push(data[i]);
           }
        }
        
        if (tarray.length==1) {
            this.main.feedback.say({
                title: 'Transfer Assignment',
                message: 'You must select at least one test session assignment.'
            });
            return;
        }
        
        for (var i=1,l=tarray.length;i<l;i++) {
            tlist = (tlist=='') ? tarray[i].AthletePrintedName : tlist + '<br>' + tarray[i].AthletePrintedName;
        }
        
        var cb = Ext.bind(this.dotransferassignmentcheck,this,[tarray],1);
        // Verify selection with user
        Ext.Msg.show({
            title: 'Transfer Assignment',
            message: 'The following assignments will be transferred to ' + aRegion + ' Region:<br>' + tlist,
            buttons: Ext.Msg.OKCANCEL,
            fn: cb
        });
        
    },
    
    dotransferassignmentcheck: function (rsp,tdata) {
        var me = (rsp.scope||this);
        
        if (rsp==='ok') {
//            debugger;

            var RegionID = tdata[0].RegionId;

            for (var i=1,l=tdata.length;i<l;i++){
           
                var tsid = tdata[i].TSID;
                // bind callback function, attaching the test session object as
                // an additional argument
                var cb = Ext.bind(me.dotransferassignmentCallback,me,tdata[i],1);
           
                // TESTING
                var rval = {"applicationid":"testsessionassignment","type":"success","message":"","data":[{"testsessionid":"8675309","dcoid":"1","statusid":"9","status":"Offered","statusdate":"today"}]};
                me.dotransferassignmentCallback(rval,[tdata[0],tdata[i]]);
           
                // write test session assignment to API
//                me.main.actions.perform({
//                    cmd: "task",
//                    n: "testsessionassignment",
//                    regionid: RegionID,
//                    statusid: "11",
//                    testsessionid: tsid,
//                    callback: cb,
//                    scope: me
//                });
            }
           
            me.main.actions.perform({
                cmd: 'request',
                source: 'rtlassignmentsdash',
                target: 'userinterface'
            });
           
        }
    },
    
    dotransferassignmentCallback: function (rsp,tdata) {
        var me = (rsp.scope||this);
        
        var ts = new Date();
        
        // Update the status and reset the parent
        if (rsp.type=="success") {
            var pregion = 'region.' + tdata[0].RegionId;
            me.main.actions.perform({
                cmd: "put",
                component: me,
                objects: [tdata[1]],
                _Status: 'UNASSIGNED',
                pa: pregion
            });
        }

    },
    
    /* -----------------------------------------------------------------------
    Accept Assignment:
    Enables DCO user to accept one or more offered assignments: Checks 
    AssignmentAPI for test session status; Writes status update to AssignmentAPI;
    Changes status of test session.
    __________________________________________________________________________ */
    
    acceptassignmentsHandler: function (sp) {
        
        // Find the selected test sessions in the grid
        var tgrid = this.main.util.get('offeredassignmentsgrid');
        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        var tarray = [];
        var tlist='';
        
        // Find the selected items in the grid
        for (var i=0,l=data.length;i<l;i++) {
           if (data[i].SelectedTestSession) {
               tarray.push(data[i]);
           }
        }
        
        // Check that something is selected
        if (tarray.length==0) {
            this.main.feedback.say({
                title: 'Accept Assignment',
                message: 'You must select at least one test session assignment'
            });
            return;
        }
        
        // format for verification
        for (var i=0,l=tarray.length;i<l;i++) {
            tlist = (tlist=='') ? tarray[i].AthletePrintedName : tlist + '<br>' + tarray[i].AthletePrintedName;
        }
        
        // Include the selected test sessions in the callback
        var cb = Ext.bind(this.acceptassignmentscheck,this,[tarray],1);
        
        // Verify selection with user
        Ext.Msg.show({
            title: 'Accept Assignments',
            message: 'The following assignments will be accepted:<br>' + tlist,
            buttons: Ext.Msg.OKCANCEL,
            fn: cb
        });
    },
    
    acceptassignmentscheck: function (rsp,tdata) {
        var me = (rsp.scope||this);
    
        if (rsp==='ok') {
            for (var i=0,l=tdata.length;i<l;i++){
                var tsid = tdata[i].TSID;
                me.doacceptassignmentHandler(tdata[i]);
            }
        }
        me.assignmentsdashboardHandler();
    },
    
    acceptassignmentHandler: function (sp) {
        var aa = this.main.data.getProvider("activeassignment");
        this.doacceptassignmentHandler(aa);
        this.assignmentsdashboardHandler();
    },
    
    doacceptassignmentHandler: function (rsp) {
    
        var me = (rsp.scope||this);

        // Dummy up return value until assignment is working on server
//        var rval = {"applicationid":"testsessionassignment","type":"success","message":"","data":[{"testsessionid":"8675309","dcoid":"1","statusid":"1","status":"Accepted","statusdate":"today"}]};
//        this.acceptassignmentCallback(rval,[rsp]);

//        debugger;
        
        // DISABLED UNTIL ASSIGNMENT TASK IS WORKING ON SERVER
        var cb = Ext.bind(me.acceptassignmentCallback,me,[rsp],1);
        
        // Write assignment status to API
        this.main.actions.perform({
            cmd: "task",
            n: "testsessionassignment",
            action: '/api/write',
            testsessionid: rsp.TSID,
            dcoid: 6||this.entity.getUser().DCOID,
            statusid: 1,
            callback: cb,
            scope: this
        });
    },
    
    acceptassignmentCallback: function (rsp, atest) {
        var me = (rsp.scope||this);
        
//        debugger;
           
        switch (rsp.success) {
            case "warning":
            case "success":
                me.main.actions.perform({
                    cmd: "put",
                    component: me,
                    objects: [atest[0]],
                    _Status: 'ASSIGNED'
                });
                break;
           case "error":
               var msgtitle = 'Error';
               var msgmessage = 'An error has occurred accepting assignment.';
               if ((Ext.isArray(rsp.events)) && (rsp.events.length>0)) {
                   msgtitle = rsp.events[0].Message;
                   msgmessage = rsp.events[0].message;
               }
               this.main.feedback.say({
                   title: msgtitle,
                   message: msgmessage
               });
               break;
        }
    },
    
    assignmentsdashboardHandler: function (sp) {

        this.main.actions.perform({
            cmd: 'request',
            source: 'assignmentsdash',
            target: 'userinterface'
        });
    },
    
    declineassignmentHandler: function (sp) {
        var aa = this.main.data.getProvider("activeassignment");
        
    },

    /* -----------------------------------------------------------------------
    Load Whereabouts:
    Opens a new browser window and displays whereabouts info for the selected
    athlete.
    __________________________________________________________________________ */
    
    loadwhereaboutsHandler: function () {
    
        this.main.actions.perform({
            cmd: "task",
            n: "whereabouts",
            dcoid: 6||this.entity.getUser().DCOID,
            callback: this.loadwhereaboutsCallback,
            scope: this
        });
    
    },
    
    loadwhereaboutsCallback: function (rsp) {
        var me = (rsp.scope||this);
        
        //TODO:  need to add athlete id to this url per documentation.
        var url = "https://athlete-admin-staging.usada.org/Whereabouts/dco/forathlete?logintoken=" + rsp.events[0].token + "&athleteid=55299";
        
        //Open in a new browser window in Safari.
        window.open(url, "_system");

    },
    
    
    
     /* -----------------------------------------------------------------------
    Business Rules.  Checks a test session to see if it is complete.
    __________________________________________________________________________ */
    
    
    checktestsessionHandler: function (sp) {
    
        //This function is called after rule's component (DCF tabpanel) is added to the screen.
        //It is also called every time the completed context changes from any of the managers in the DCF section.
        
        this.main.actions.perform({
            cmd: 'setbusy',
            component: sp.component,
            message: 'Checking progress...'
        });
        
        //Important: Defer to allow all components to draw on screen, then must set up all managers in
        //manageProviders before this function will work correctly.
        
        /* Common pitfalls so far:
            incorrect provider
            provider must be character exact
            must separate providers and managers into trees so they do not overlap.
            provider name is case sensitive and must be exact
            forgetting to put required:true on a component
            
        */

        Ext.defer(function () {
        
            var obj = {
                component: sp.component,
                subtopic: (sp.rule) ? sp.rule.value : ''
            }
            
            //container = name of the component you want to mark with complete/incomplete style.
            //manager = name of a manager component that is managing a section of fields and marking them complete/incomplete
            //title = base title to alter with complete/incomplete style.
            //required = if true it will ignore started setting and track the completeness of that section no matter what.
            
            var ts = this.main.data.getProvider("{activetestsession}");
            var ath = this.main.data.getProvider("{activetestsession > athlete[0]}");

            
            //DCF
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:ts,title:this.main.localization.translate('dcor'),container:'dcfLandingForm'},obj));
            
            
            
            //Notification Form
            //-------------------------------
            this.checkForm(Ext.apply({dataprovider:ts,title:this.main.localization.translate('notification'),container: 'dcornot',completed:[ath.athletenameFieldset_completed,ts.notificationFieldset_completed,ts.chaperoneFS_completed,ts.notificationPart4_completed]},obj));
            
            
            //Part 1 (Notification)
this.checkSection(Ext.apply({title: this.main.localization.translate("notification"),container: "notificationPart1",managers: ["athletenameFieldset","notificationFieldset","chaperoneFS"],required: true},obj));
            
            //Part 2 (Regulations)
            this.checkSection(Ext.apply({title: this.main.localization.translate("regulations"),container: "notificationPart2"},obj));
            
            //Part 3 (Notification Letter)
            this.checkSection(Ext.apply({title: this.main.localization.translate("notificationletter"),container: "notificationPart3"},obj));
           
            //Part 4 (Signature)
            this.checkSection(Ext.apply({title: this.main.localization.translate("signature"), container: "notificationPart4", managers: ["notificationPart4"],required:true},obj));
    

            //Athlete Info Form
            //-------------------------------
            this.checkForm(Ext.apply({dataprovider:ts,title:this.main.localization.translate('athleteinfotitle'),container: 'dcorai',completed:[ts.athleteinfoPart1_completed,ts.identificationFieldset_completed,ath.dobFieldset_completed,ath.nationalityFS_completed,ts.residenceAddressFS_completed,ts.mailingFS_completed]},obj));
            
            
            //Part 1 (Arrival)
            this.checkSection(Ext.apply({title: this.main.localization.translate("arrival"), container: "athleteinfoPart1", managers: ["athleteinfoPart1"],required:true},obj));
            
            //Part 2 (Identification)
            this.checkSection(Ext.apply({title: this.main.localization.translate("identification"),container: "athleteinfoPart2", managers: ["identificationFieldset","dobFieldset","nationalityFS"],required:true},obj));
        
            //Part 3 (Residence Address)
            this.checkSection(Ext.apply({title: this.main.localization.translate("residenceaddress"),container: "athleteInfoPart3", managers: ["residenceAddressFS"],required:true},obj));
            
            //Part 4 (Mailing Address)
            this.checkSection(Ext.apply({title: this.main.localization.translate("mailingaddress"),container: "athleteInfoPart4", managers: ["mailingFS"],required:true},obj));
            
            //Part 5 (Support Personnel)
            this.checkSection(Ext.apply({title: this.main.localization.translate("athleterepresentative"),container: "athleteInfoPart5"},obj));


            //Lab/Sample Info Form
            //-------------------------------
            this.checkForm(Ext.apply({dataprovider:ts,title:this.main.localization.translate('labsampleinfotitle'),container: 'dcorlab',completed:[ts.labSampleInfoPart1_completed,ath.genderFS_completed,ts.labSampleInfoPart3_completed]},obj));
            
            //Part 1 (Competition)
            this.checkSection(Ext.apply({title: this.main.localization.translate("competition"),container: "labSampleInfoPart1",managers: ["labSampleInfoPart1","genderFS"],required: true},obj));
            
            //Part 2 (Tests)
            this.checkSection(Ext.apply({title: this.main.localization.translate("tests"),container: "labSampleInfoPart2"},obj));
            
            //Part 3 (Site)
            this.checkSection(Ext.apply({title: this.main.localization.translate("site"),container: "labSampleInfoPart3",managers: ["labSampleInfoPart3"],required: true},obj));


            //Blood Sample Form
            //-------------------------------
            
            
            //Need to loop over bloodsamples.
            var bcomplete = true;
            var bfound = false;
            var bsamples = this.main.data.filter(this.main.data.getChildren(ts),"cid EQ bloodsample");
            for (var i=0,l=bsamples.length;i<l;i++) {
                bfound = true;
                var b = bsamples[i];
                if (b.BloodSampleInfo_completed) {
                } else {
                    bcomplete = false;
                }
            }
            
            //Recheck Blood Samples list.
            //Might need to move this into a rule that only works when Blood data is being added.
            var dvw = sp.component.down("xdataview[name='BloodSamplesList']");
            if (dvw) dvw.refresh();

            if (ts.Blood!=true){
                this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate("bloodsample"),container: "dcorbl",disabled: true},obj));
            } else {
                if (bfound) {
                    this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate("bloodsample"),container: "dcorbl",completed: [bcomplete,ts.BloodQuestionsSet1_completed,ts.BloodQuestionsSet2_completed,ts.BloodQuestionsSet3_completed,ts.BloodQuestionsSet4_completed,ts.BloodSampleBCOSignature_completed,ts.BloodSampleDCOSignature_completed]},obj));
                } else {
                    this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate("bloodsample"),container: "dcorbl", completed: [false]},obj));
                }
            }
            
            //Competition
            if (ts.Blood) {
                  this.checkSection(Ext.apply({title: this.main.localization.translate("competition"),container: "BloodQuestionsSet1",managers:["BloodQuestionsSet1"],required: true},obj));
            } else {
                  this.checkSection(Ext.apply({title: this.main.localization.translate("competition"),container: "BloodQuestionsSet1",required: true},obj));
            }
            
            //Altitude
            if (ts.Blood) {
                  this.checkSection(Ext.apply({title: this.main.localization.translate("altitudetitle"),container: "BloodQuestionsSet2",managers:["BloodQuestionsSet2"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("altitudetitle"),container: "BloodQuestionsSet2",required: true},obj));
            }
                  
            //Extreme Conditions
            if (ts.Blood) {
                  this.checkSection(Ext.apply({title: this.main.localization.translate("extrmeconditions"), container: "BloodQuestionsSet3", managers:["BloodQuestionsSet3"], required: true},obj));
            } else {
                  this.checkSection(Ext.apply({title: this.main.localization.translate("extrmeconditions"), container: "BloodQuestionsSet3",requried: true},obj));
            }
 
            //Blood loss/transfusion
            if (ts.Blood) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("bloodlosstransfusion"), container: "BloodQuestionsSet4", managers:["BloodQuestionsSet4"], required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("bloodlosstransfusion"), container: "BloodQuestionsSet4",requried: true},obj));
            }
                  
            //Part 3 (Sample Info)
            if (bfound) {
                this.setComplete(Ext.apply({title: this.main.localization.translate("sampleinfo"),container: "BloodSampleInfo", completed: bcomplete},obj));
            } else {
//                this.checkSection(Ext.apply({title: this.main.localization.translate("sampleinfo"),container: "BloodSampleInfo",required: true},obj));
                  this.setComplete(Ext.apply({title: this.main.localization.translate("sampleinfo"), container: "BloodSampleInfo", completed: false},obj));
            }
            
            //Part 4 (BCO Signature)
            if (ts.Blood) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("bcosignaturetitle"),container: "BloodSampleBCOSignature",managers:["BloodSampleBCOSignature"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("bcosignaturetitle"),container: "BloodSampleBCOSignature",required: true},obj));
            }
            
            //Part 5 (DCO Signature)
            if (ts.Blood) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "BloodSampleDCOSignature",managers:["BloodSampleDCOSignature"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "BloodSampleDCOSignature",required: true},obj));
            }
            


            //Urine Sample Form
            //-------------------------------
            
            //Need to loop over urinesamples.
            var ucomplete = true;
            var ufound = false;
            var usamples = this.main.data.filter(this.main.data.getChildren(ts),"cid EQ urinesample");
            for (var i=0,l=usamples.length;i<l;i++) {
                ufound = true;
                var u = usamples[i];
                if (u.ChaperoneForm_completed && u.urinesampleform_completed) {
                } else {
                    ucomplete = false;
                }
            }
        
            if (ts.Urine!=true) {
                this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate('urinesample'),container: 'dcorur',disabled: true},obj));
            } else {
                if (ufound) {
                    this.checkForm(Ext.apply({dataprovider:ts,title:this.main.localization.translate('urinesample'),container: 'dcorur',completed:[ucomplete]},obj));
                } else {
                    this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate('urinesample'),container: 'dcorur',completed:[false]},obj));
                }
            }
            
            //Recheck Urine Samples list.
            //Might need to move this into a rule that only works when data is being added.
            //var dvw = sp.component.down("xgrid[name='urinesamples']");
            //if (dvw) dvw.loadData();
            
                  
            //Part 1 (Chaperone)
            if (ufound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("chaperone"),container: "ChaperoneForm",managers:["ChaperoneForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("chaperone"),container: "ChaperoneForm",required: true},obj));
            }
            
            //Part 2 (Urine Sample)
            if (ufound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("urinesample"),container: "urinesampleform",managers:["urinesampleform"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("urinesample"),container: "urinesampleform",required: true},obj));
            }


            //Research Consent Form
            //-------------------------------
	    if (ts.AcceptTerms=="Yes") {
            	this.checkForm(Ext.apply({dataprovider:ts,title:this.main.localization.translate('researchconsenttitle'),container: 'dcorcs',completed:[ts.ResearchConsentPart1_completed,ts.ResearchConsentPart2_completed]},obj));
	    } else {
            	this.checkForm(Ext.apply({dataprovider:ts,title:this.main.localization.translate('researchconsenttitle'),container: 'dcorcs',completed:[ts.ResearchConsentPart1_completed]},obj));
	    }	
            
            
            //Part 1 (Accept Terms)
            this.checkSection(Ext.apply({title: this.main.localization.translate("accepttermstitle"),container: "ResearchConsentPart1",managers: ["ResearchConsentPart1"],required: true},obj));
            
            //Part 2 (Athlete Signature)
	    if (ts.AcceptTerms=="Yes") {
            	this.checkSection(Ext.apply({title: this.main.localization.translate("athletesignature"),container: "ResearchConsentPart2",managers: ["ResearchConsentPart2"],required: true},obj));
	    } else {
            	this.checkSection(Ext.apply({title: this.main.localization.translate("athletesignature"),container: "ResearchConsentPart2",required: true},obj));
	    }


            //Declaration of Use Form
            //-------------------------------
            this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate('declarationofuse'),container: 'dcordou',completed:[ts.douDeclarations_completed,ts.douAgreement_completed]},obj));
            
            //Part 1 (Declarations)
            this.checkSection(Ext.apply({title: this.main.localization.translate("declarations"),container: "douDeclarations",managers: ["douDeclarations"],required: true},obj));
            
            //Part 2 (Laboratory/Sample Information)
            this.checkSection(Ext.apply({title: this.main.localization.translate("labsampleinfo"),container: "douLabSampleInfo",required: true},obj));
            
            //Part 3 (Agreement)
            this.checkSection(Ext.apply({title: this.main.localization.translate("agreement"),container: "douAgreement",managers: ["douAgreement"],required: true},obj));



            //Partial Sample Form
            //-------------------------------
            
            //Need to loop over partial samples.
            var pscomplete = true;
            var pfound = false;
            var partials = this.main.data.filter(this.main.data.getChildren(ts),"cid EQ partialsample");
            for (var i=0,l=partials.length;i<l;i++) {
                pfound = true;
                var p = partials[i];
                if (p.dcorpsVolumeKit_completed && p.dcorpsDateTimeForm_completed && p.dcorpsWitnessSig_completed && p.dcorpsDCOSig_completed && p.dcorpsDCOSig_completed && p.dcorpsDateTimeOpened_completed && p.dcorpsAthleteSig_completed && p.dcorpsDCOSig2_completed) {
                } else {
                    pscomplete = false;
                }
            }
            
            //Recheck Partial Samples list.
            //Might need to move this into a rule that only works when data is being added.
            //var dvw = sp.component.down("xgrid[name='PartialSampleList']");
            //if (dvw) dvw.loadData();
            
            if (pfound) {
                this.checkForm(Ext.apply({dataprovider:ts,title:this.main.localization.translate('partialsample'),container: 'dcorpt',completed:[pscomplete]},obj));
            } else {
                this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate('partialsample'),container: 'dcorpt'},obj));
            }
            
            
            //Part 1 (Volume/Kit)
            if (pfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("volumekit"),container: "dcorpsVolumeKit",managers: ["dcorpsVolumeKit"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("volumekit"),container: "dcorpsVolumeKit",required: true},obj));
            }
            
            //Part 2 (Date/Time Sealed)
            if (pfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("datetimesealed"),container: "dcorpsDateTimeForm",managers:["dcorpsDateTimeForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("datetimesealed"),container: "dcorpsDateTimeForm",required: true},obj));
            }
            
            //Part 3 (Witness Signature)
            if (pfound) {
                this.checkSection(Ext.apply({title:this.main.localization.translate( "witnesssignature"),container: "dcorpsWitnessSig",managers: ["dcorpsWitnessSig"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("witnesssignature"),container: "dcorpsWitnessSig",required: true},obj));
            }
            
            //Part 4 (DCO Signature)
            if (pfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "dcorpsDCOSig",managers: ["dcorpsDCOSig"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "dcorpsDCOSig",required: true},obj));
            }
            
            //Part 5 (Date/Time Opened)
            if (pfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("datetimeopened"),container: "dcorpsDateTimeOpened",managers: ["dcorpsDateTimeOpened"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("datetimeopened"),container: "dcorpsDateTimeOpened",required: true},obj));
            }
            
            //Part 6 (Athlete Signature)
            if (pfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("athletesignature"),container: "dcorpsAthleteSig",managers: ["dcorpsAthleteSig"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("athletesignature"),container: "dcorpsAthleteSig",required: true},obj));
            }
            
            //Part 7 (DCO Signature)
            if (pfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "dcorpsDCOSig2",managers: ["dcorpsDCOSig2"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "dcorpsDCOSig2",required: true},obj));
            }


            //Custody Transfer Form
            //-------------------------------
            
            //Need to loop over custody transfers.
            var ctcomplete = true;
            var cfound = false;
            var transfers = this.main.data.filter(this.main.data.getChildren(ts),"cid EQ custodytransfer");
            for (var i=0,l=transfers.length;i<l;i++) {
                cfound = true;
                var ct = transfers[i];
                if (ct.reasonForm_completed && ct.AthleteSig2_completed && ct.DcoSigForm_completed  && ct.TransferAthleteForm_completed && ct.AthleteSigForm_completed && ct.DcoSig2_completed) {
                } else {
                    ctcomplete = false;
                }
            }
            
            //Recheck Custody Transfer form.
            //Might need to move this into a rule that only works when data is being added.
            //var dvw = sp.component.down("xgrid[name='custodytransferlist']");
            //if (dvw) dvw.loadData();
            
            if (cfound) {
                this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate('custodytransfertitle'),container: 'dcortrans',completed:[ctcomplete]},obj));
            } else {
                this.checkForm(Ext.apply({dataprovider:ts,title:this.main.localization.translate('custodytransfertitle'),container: 'dcortrans'},obj));
            }
                  
            //Part 1 (Reason for Transfer)
            if (cfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("reasonfortransfer"),container: "reasonForm",managers: ["reasonForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("reasonfortransfer"),container: "reasonForm",required: true},obj));
            }
            
            //Part 2 (Athlete Signature)
            if (cfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("athletesignature"),container: "AthleteSig2",managers:["AthleteSig2"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("athletesignature"),container: "AthleteSig2",required: true},obj));
            }
            
            //Part 3 (DCO Signature)
            if (cfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "DcoSigForm",managers: ["DcoSigForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "DcoSigForm",required: true},obj));
            }
            
            //Part 4 (Transfer to Athlete)
            if (cfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("transfertoathlete"),container: "TransferAthleteForm",managers: ["TransferAthleteForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("transfertoathlete"),container: "TransferAthleteForm",required: true},obj));
            }
            
            //Part 5 (Athlete Signature)
            if (cfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("athletesignature"),container: "AthleteSigForm",managers: ["AthleteSigForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("athletesignature"),container: "AthleteSigForm",required: true},obj));
            }
            
            //Part 6 (DCO Signature)
            if (cfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "DcoSig2",managers: ["DcoSig2"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "DcoSig2",required: true},obj));
            }
            


            //Supplementary Form
            //-------------------------------
            
            //Need to loop over custody transfers.
            var supcomplete = true;
            var sfound = false;
            var supplementaries = this.main.data.filter(this.main.data.getChildren(ts),"cid EQ supplementaryreport");
            for (var i=0,l=supplementaries.length;i<l;i++) {
                sfound = true;
                var sup = supplementaries[i];
		// Each supplementary report can have multiple signatures
		// Loop through these to make sure all are complete
		//var supsigs = this.main.data.filter(this.main.data.getChildren(sup),"cid EQ supportperson");
		//var ssigcomplete = true;
		//var ssigfound = false;
		//for (var j=0,k=supsigs.length;j<k;j++) {
		//	ssigfound = true;
		//	var supsig = supsigs[j];
		//	if (supsig.dcorsupPart5_completed) {
		//	} else {
		//		ssigcomplete = false;
		//	}
		//}
		//if (ssigfound) {
			// save status of signatures to the supplementary report
		//	this.main.actions.perform({
		//		cmd: "put",
		//		component: this,
		//		objects:[sup],
		//		dcorsupPart5_completed: ssigcomplete,
		//		dcorsupPart5_started: ssigfound
		//	});
		//}

                if (sup.dcorsupPart2_completed) {
                } else {
                    supcomplete = false;
                }
            }

	    //var ssp = this.main.data.getProvider("{supplementarysupportperson}");
            
            //Recheck Supplementary form.
            //Might need to move this into a rule that only works when data is being added.
            //var dvw = sp.component.down("xgrid[name='addsupplementarylist']");
            //if (dvw) dvw.loadData();
            

            if (sfound) {
                this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate('supplementary'),container: 'dcorsup',completed:[supcomplete]},obj));
            } else {
                this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate('supplementary'),container: 'dcorsup'},obj));
            }
            
                  
            //Part 1 (Applies To)
            this.checkSection(Ext.apply({title: this.main.localization.translate("appliesto"),container: "dcorsupPart1",required: true},obj));
            
            //Part 2 (Person Submitting)
            if (sfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("personsubmittingtitle"),container: "dcorsupPart2",managers:["dcorsupPart2"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("personsubmittingtitle"),container: "dcorsupPart2",required: true},obj));
            }
            
            //Part 3 (Purpose of Report)
            this.checkSection(Ext.apply({title: this.main.localization.translate("purposeofreport"),container: "dcorsupPart3",required: true},obj));
            
            //Part 4 (Comments)
            this.checkSection(Ext.apply({title: this.main.localization.translate("comments"),container: "dcorsupPart4",required: true},obj));
            
            //Part 5 (Signature)
            //if (sfound) {
            //    this.checkSection(Ext.apply({title: this.main.localization.translate("signature"),container: "dcorsupPart5",managers: ["dcorsupPart5"],required: true},obj));
            //} else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("signature"),container: "dcorsupPart5",required: true},obj));
            //}
            
            
            var ath = this.main.data.getProvider("{activetestsession > athlete[0]}");
           

            //Signatures Form
            //-------------------------------
            
            this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate('signatures'),container: 'dcorsig',completed:[ts.signaturesPart2_completed,ts.signaturesPart7_completed]},obj));
           
                  
            //Part 1 (Representative)
            this.checkSection(Ext.apply({title: this.main.localization.translate("athleterepresentative"),container: "representativesignatureform"},obj));
            
            //Part 2 (Other Signatures)
            this.checkSection(Ext.apply({title: this.main.localization.translate("othersignatures"),container: "signaturesPart5"},obj));
            
            //Part 3 (Subsequent DCO)
            this.checkSection(Ext.apply({title: this.main.localization.translate("subsequentofficer"),container: "signaturesPart6"},obj));

            //Part 4 (Review)
            this.checkSection(Ext.apply({title: this.main.localization.translate("reviewforms"), container: "signaturesPart1", required:true},obj));
            
            //Part 5 (DCO Signature)
            this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "signaturesPart7", managers: ["signaturesPart7"],required:true},obj));

            //Part 6 (Athlete Signature)
            this.checkSection(Ext.apply({title: this.main.localization.translate("athletesignature"),container: "signaturesPart2", managers: ["signaturesPart2"],required:true},obj));
                  
            //Part 8 (Receipt)
            this.checkSection(Ext.apply({title: this.main.localization.translate("receipt"),container: "signaturesPart8"},obj));
            
            //Part 9 (Submit Forms)
            this.checkSection(Ext.apply({title: this.main.localization.translate("submit"),container: "signaturesPart9"},obj));
            
            this.checkCollection(Ext.apply({button:"submittestsessionbutton",completed:[ts.dcornot_completed,ts.dcorai_completed,ts.dcorlab_completed,ts.dcorbl_completed,ts.dcorur_completed,ts.dcorcs_completed,ts.dcordou_completed,ts.dcorpt_completed,ts.dcortrans_completed,ts.dcorsup_completed,ts.dcorsig_completed],override:ts.papertest},obj));
            
            //Done
            this.main.actions.perform({
                cmd: 'removebusy',
                component: sp.component
            });
            
        },1000,this);

    },
    
    
     checkuarHandler: function (sp) {
    
        //This function is called after rule's component (UAR tabpanel) is added to the screen.
        //It is also called every time the completed context changes from any of the managers in the UAR section.
        
        this.main.actions.perform({
            cmd: 'setbusy',
            component: sp.component,
            message: 'Checking progress...'
        });
        
        //Important: Defer to allow all components to draw on screen, then must set up all managers in
        //manageProviders before this function will work correctly.
        
        /* Common pitfalls so far:
            incorrect provider
            provider must be character exact
            must separate providers and managers into trees so they do not overlap.
            provider name is case sensitive and must be exact
            forgetting to put required:true on a component
            
        */

        Ext.defer(function () {
        
            var obj = {
                component: sp.component,
                subtopic: (sp.rule) ? sp.rule.value : ''
            }
            
            //container = name of the component you want to mark with complete/incomplete style.
            //manager = name of a manager component that is managing a section of fields and marking them complete/incomplete
            //title = base title to alter with complete/incomplete style.
            //required = if true it will ignore started setting and track the completeness of that section no matter what.
            
            var uar = this.main.data.getProvider("{activeuar}");
            var loc = this.main.data.getProvider("{activeuar > location}");

            
            //Athlete Information
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:uar,title: this.main.localization.translate('athleteinfotitle'),container:'uarathleteinfoform',completed:[uar.uarathleteinfoform_completed]},obj));
            

            //Attempt Information
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:uar,title: this.main.localization.translate('attemptinformation'),container:'attemptinfoform',completed:[uar.attemptinfoform_completed]},obj));
            
            //First Location
            //-------------------------------
            
            //Need to loop over Phone Calls.
            var pcomplete = true;
            var pfound = false;
            if (uar.FirstCallsPlaced) {
                var pnumbers = this.main.data.filter(this.main.data.getChildren(uar),"cid EQ phonecall");
                for (var i=0,l=pnumbers.length;i<l;i++) {
                    var p = pnumbers[i];
                    if (p.UARLocation=='FirstLocation'){
                    	pfound=true;
                        if (p.FirstLocNumbersCalledForm_completed) {
                        } else {
                            pcomplete = false;
                        }
                    }
                }
            }
            
            //Recheck Phone Calls list.
            //Might need to move this into a rule that only works when Blood data is being added.
            //var dvw = sp.component.down("xgrid[name='uarfirstnumberscalledgrid']");
            //if (dvw) dvw.loadData();
            
            
            this.checkForm(Ext.apply({dataprovider:uar,title: this.main.localization.translate("firstlocation"),container: "FirstLocationTabPanel",completed: [pcomplete,uar.FirstLocationLocationForm_completed,uar.FirstLocationTimeForm_completed,uar.FirstLocationCommentsForm_completed]},obj));
            
            //Location
            this.checkSection(Ext.apply({title: this.main.localization.translate("firstlocation"),container: "FirstLocationLocationForm",managers:["FirstLocationLocationForm"],required: true},obj));
            
            //Time
            this.checkSection(Ext.apply({title: this.main.localization.translate("time"),container: "FirstLocationTimeForm",managers:["FirstLocationTimeForm"],required: true},obj));
            
            //Person Contacted
            this.checkSection(Ext.apply({title: this.main.localization.translate("personcontacted"),container: "FirstLocationPersonContactedForm",required: true},obj));
            
            //Restricted Access
            this.checkSection(Ext.apply({title: this.main.localization.translate("restrictedaccess"),container: "FirstLocationRestrictedAccessForm",required: true},obj));
            
            //Numbers Called
            if (pfound) {
                this.setComplete(Ext.apply({title: this.main.localization.translate("phonenumberscalled"),container: "FirstLocNumbersCalledForm",completed: pcomplete},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("phonenumberscalled"),container: "FirstLocNumbersCalledForm",required: true},obj));
            }
            
            
            //Comments
            this.checkSection(Ext.apply({title: this.main.localization.translate("comments"),container: "FirstLocationCommentsForm",managers:["FirstLocationCommentsForm"],required: true},obj));
            
            
            
            //Second Location
            //-------------------------------
            
            //Need to loop over Phone Calls.
            var scomplete = true;
            var sfound = false;
            if (uar.SecondCallsPlaced==true) {
                var snumbers = this.main.data.filter(this.main.data.getChildren(uar),"cid EQ phonecall");
                for (var i=0,l=snumbers.length;i<l;i++) {
                    var sn = snumbers[i];
		    if (sn.UARLocation=='SecondLocation'){
                    	sfound = true;
                    	if (sn.SecondLocNumbersCalledForm_completed) {
                    	} else {
                    	    scomplete = false;
                    	}
		    }
                }
            }
            
            //Recheck Phone Calls list.
            //Might need to move this into a rule that only works when Blood data is being added.
            //var dvw = sp.component.down("xgrid[name='uarsecondnumberscalledgrid']");
            //if (dvw) dvw.loadData();
            
            //secondlocationvisited
            if (uar.secondlocationvisited=="Yes") {
                this.checkForm(Ext.apply({dataprovider:uar,title: this.main.localization.translate("secondlocation"),container: "SecondLocationTabPanel",completed: [scomplete,uar.SecondLocationLocationForm_completed,uar.SecondLocationTimeForm_completed,uar.SecondLocationCommentsForm_completed]},obj));
            } else if (uar.secondlocationvisited=="No") {
                this.checkForm(Ext.apply({dataprovider:uar,title:this.main.localization.translate('secondlocation'),container:'SecondLocationTabPanel'},obj));
            } else {
                this.checkForm(Ext.apply({dataprovider:uar,title:this.main.localization.translate('secondlocation'),container:'SecondLocationTabPanel',completed: [uar.SecondLocationLocationForm]},obj));
            }
            
            //Location
            if (uar.secondlocationvisited=="Yes") {
                this.checkSection(Ext.apply({title: this.main.localization.translate("location"),container: "SecondLocationLocationForm",managers:["SecondLocationLocationForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("location"),container: "SecondLocationLocationForm",required: true},obj));
            }
            
            //Time
            if (uar.secondlocationvisited=="Yes") {
                this.checkSection(Ext.apply({title: this.main.localization.translate("time"),container: "SecondLocationTimeForm",managers:["SecondLocationTimeForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("time"),container: "SecondLocationTimeForm",required: true},obj));
            }

            
            //Person Contacted
            this.checkSection(Ext.apply({title: this.main.localization.translate("personcontacted"),container: "SecondLocationContactedForm",required: true},obj));
            
            //Restricted Access
            this.checkSection(Ext.apply({title: this.main.localization.translate("restrictedaccess"),container: "SecondLocationRestrictedAccessForm",required: true},obj));
            
            //Numbers Called
            if (sfound) {
                this.setComplete(Ext.apply({title: this.main.localization.translate("phonenumberscalled"),container: "SecondLocNumbersCalledForm",completed: scomplete},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("phonenumberscalled"),container: "SecondLocNumbersCalledForm",required: true},obj));
            }

            
            //Comments
            if (uar.secondlocationvisited=="Yes") {
                this.checkSection(Ext.apply({title: this.main.localization.translate("comments"),container: "SecondLocationCommentsForm",managers:["SecondLocationCommentsForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("comments"),container: "SecondLocationCommentsForm",required: true},obj));
            }
            
            
            //Comments
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:uar,title:this.main.localization.translate('comments'),container:'uarcommentsform'},obj));
            
            //DCO Confirmation
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:uar,title:this.main.localization.translate('dcoconfirmation'),container:'dcoconfirmationform',completed:[uar.dcoconfirmationform_completed]},obj));
            
            //Check completed status on all forms
            this.checkCollection(Ext.apply({button:'submituarbtn',completed:[uar.uarathleteinfoform_completed,uar.attemptinfoform_completed,uar.FirstLocationTabPanel_completed,uar.SecondLocationTabPanel_completed,uar.dcoconfirmationform_completed]},obj));
            

            //Done
            this.main.actions.perform({
                cmd: 'removebusy',
                component: sp.component
            });
            
        },1000,this);

    },
    
     checkaarHandler: function (sp) {
    
        //This function is called after rule's component (AAR tabpanel) is added to the screen.
        //It is also called every time the completed context changes from any of the managers in the AAR section.
        
        this.main.actions.perform({
            cmd: 'setbusy',
            component: sp.component,
            message: 'Checking progress...'
        });
        
        //Important: Defer to allow all components to draw on screen, then must set up all managers in
        //manageProviders before this function will work correctly.
        
        /* Common pitfalls so far:
            incorrect provider
            provider must be character exact
            must separate providers and managers into trees so they do not overlap.
            provider name is case sensitive and must be exact
            forgetting to put required:true on a component
            
        */

        Ext.defer(function () {
        
            var obj = {
                component: sp.component,
                subtopic: (sp.rule) ? sp.rule.value : ''
            }
            
            //container = name of the component you want to mark with complete/incomplete style.
            //manager = name of a manager component that is managing a section of fields and marking them complete/incomplete
            //title = base title to alter with complete/incomplete style.
            //required = if true it will ignore started setting and track the completeness of that section no matter what.
            
            var aar = this.main.data.getProvider("{activeaar}");
            
            //Location of Completed Test
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:this.main.localization.translate('locationcompletedtest'),container:'locationofcompletedtestform',completed:[aar.locationofcompletedtestform_completed]},obj));
            

            //Other Locations
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:this.main.localization.translate('otherlocations'),container:'otherlocationsform'},obj));
            
            
            //Notification
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:this.main.localization.translate('notification'),container:'aarnotificationform',completed:[aar.aarnotificationform_completed]},obj));
            
            //Processing
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:this.main.localization.translate('processing'),container:'aarprocessingform',completed:[aar.aarprocessingform_completed]},obj));
            
            //Post Processing
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:this.main.localization.translate('postprocessing'),container:'aarpostprocessingform',completed:[aar.aarpostprocessingform_completed]},obj));
            
            //Comments
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:this.main.localization.translate('comments'),container:'aarcommentsform'},obj));
            
            //Signature
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:this.main.localization.translate('signature'),container:'aarsignatureform',completed:[aar.aarsignatureform_completed]},obj));
            
            //Check completed status on all forms
            this.checkCollection(Ext.apply({button:'submitaarbutton',completed:[aar.locationofcompletedtestform_completed,aar.aarnotificationform_completed,aar.aarprocessingform_completed,aar.aarpostprocessingform_completed,aar.aarsignatureform_completed]},obj));

            //Done
            this.main.actions.perform({
                cmd: 'removebusy',
                component: sp.component
            });
            
        },1000,this);

    },

     checkmanifestHandler: function (sp) {
    
        //This function is called after rule's component (Sample Manifest tabpanel) is added to the screen.
        //It is also called every time the completed context changes from any of the managers in the Sample Manifest section.
        
        this.main.actions.perform({
            cmd: 'setbusy',
            component: sp.component,
            message: 'Checking progress...'
        });
        
        //Important: Defer to allow all components to draw on screen, then must set up all managers in
        //manageProviders before this function will work correctly.
        
        /* Common pitfalls so far:
            incorrect provider
            provider must be character exact
            must separate providers and managers into trees so they do not overlap.
            provider name is case sensitive and must be exact
            forgetting to put required:true on a component
            
        */

        Ext.defer(function () {
        
            var obj = {
                component: sp.component,
                subtopic: (sp.rule) ? sp.rule.value : ''
            }
            
            //container = name of the component you want to mark with complete/incomplete style.
            //manager = name of a manager component that is managing a section of fields and marking them complete/incomplete
            //title = base title to alter with complete/incomplete style.
            //required = if true it will ignore started setting and track the completeness of that section no matter what.
            
            var am = this.main.data.getProvider("{activemanifest}");
            
            //Urine Samples
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('urinesamples2'),container:'selectusamplesform'},obj));
            

            //Blood Samples
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('bloodsamples'),container:'selectbsamplesform'},obj));
            
            
            //Date/Time
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('datetime'),container:'smdatetimeform',completed:[am.smdatetimeform_completed]},obj));
            
            //Urine Analysis
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('urineanalysis'),container:'urineanalysisform'},obj));
            
            //Blood Analysis
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('bloodanalysis'),container:'bloodanalysisform'},obj));
            
            //Shipping Info
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('shippinginfo'),container:'shippinginfoform',completed:[am.shippinginfoform_completed]},obj));

             //Waybill
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('waybill'),container:'waybillpanel'},obj));

            //Comments
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('comments'),container:'smcommentsform'},obj));
            
            //Signature
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('dcosignature'),container:'smdcosignatureform',completed:[am.smdcosignatureform_completed]},obj));
            
            //Check completed status on all forms
            this.checkCollection(Ext.apply({button:'savemanifestbutton',completed:[am.smdcosignatureform_completed,am.smdatetimeform_completed,am.shippinginfoform_completed]},obj));

            //Done
            this.main.actions.perform({
                cmd: 'removebusy',
                component: sp.component
            });
            
        },1000,this);

    },
    
    
    checkSection: function (sp) {
    
        //Check a section for completeness and set style.
    
        try {
        
            var managers = sp.managers;
            var started = false;
            var completed = true;
            
            if (Ext.isArray(managers)) {
            
                //Combine all the different managers to create a complete/started condition for all managers.
                for (var i=0,l=managers.length;i<l;i++) {
                    var n = managers[i];
                    var manager = this.main.data.getObjectByName(n);
                    var provider = this.main.data.getProvider(manager.manage);
                    if (provider) {
                        started = started||(provider[n+"_started"]);
                        completed = completed&&(provider[n+"_completed"]);
                    } else {
                        console.log("couldn't find provider " + manager.manage);
                    }
                }

                var container = this.main.data.getObjectByName(sp.container);
                if (started||sp.required) {
                    var selector = container.cid + "[name='" + sp.container + "']";
                    var o = sp.component.down(selector);
                    if (completed) {
                        o.setTitle('<span class="fa fa-check-circle" style="color:green;"></span> ' + sp.title);
                    } else {
                        o.setTitle('<span class="fa fa-ban" style="color:red;"></span> ' + sp.title);
                    }
                }
               
            } else {
           
                //Enable this section if you want to have all tabs with checkmarks even if there are no required fields.
                //Leave this section disabled if you want to only put checkmarks on tabs with required information.
           
           
                try {
                    //Manual override to give green checks to sections that don't have any required fields.
                    var container = this.main.data.getObjectByName(sp.container);
                    var selector = container.cid + "[name='" + sp.container + "']";
                    var o = sp.component.down(selector);
                    o.setTitle('<span class="fa fa-circle" style="color:grey;"></span> ' + sp.title);
                } catch (e) {
                    console.log("Couldn't locate " + selector);
                }
           
            }
        
        } catch (e) {
            //debugger;
        }
        
    },
    
    setComplete: function (sp) {
        //Manual switch for special cases.
        var container = this.main.data.getObjectByName(sp.container);
        var selector = container.cid + "[name='" + sp.container + "']";
        var o = sp.component.down(selector);
        if (sp.completed) {
            o.setTitle('<span class="fa fa-check-circle" style="color:green;"></span> ' + sp.title);
        } else {
            o.setTitle('<span class="fa fa-ban" style="color:red;"></span> ' + sp.title);
        }
    
    },
    
    checkForm: function (sp) {
        //Manual check for main forms.
        var notreq = false;
        if (!sp.completed) {
            sp.completed = [true];
            notreq = true;
        }
        if (!sp.disabled) sp.disabled = false;
            
        var v = sp.completed.every(function(i){
            return i;
        });
            
        var frm = sp.component.down("xtabpanel[name='" + sp.container + "']")||sp.component.down("xform[name='" + sp.container + "']")||sp.component.down("xpanel[name='" + sp.container + "']");
            
        var ap = {
            cmd: "put",
            component: this,
            objects: [sp.dataprovider]
        };
        ap[sp.container+"_completed"] = v;
        this.main.actions.perform(ap);
        
        if (frm) {
            if (sp.disabled||notreq) {
                frm.setTitle('<span class="fa fa-circle" style="color:grey;"></span> ' + sp.title);
            } else {
                if (v) {
                    frm.setTitle('<span class="fa fa-check-circle" style="color:green;"></span> ' + sp.title);
                } else {
                    frm.setTitle('<span class="fa fa-ban" style="color:red;"></span> ' + sp.title);
                }
            }
        }
        
    },
    
    checkCollection: function (sp) {
        if (!sp.completed) sp.completed = [true];
        
        var x = (sp.override===true) ? true : false ;
        
        var v = sp.completed.every(function(i){
            return i;
        });
        
        var btn = sp.component.down("xbutton[name='" + sp.button + "']");
           
        if (btn) {
            if (v || x) {
                btn.enable(true);
            } else {
                btn.disable(true);
            }
        }
    }
           
        
        
        
});

Ext.ClassManager.addNameAliasMappings({
    'Paperless.view.main.Actions': ['custom']
});
