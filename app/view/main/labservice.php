<?php
    
//---------------------------------------------------------------------------------------------------------
// LabService.php
//---------------------------------------------------------------------------------------------------------
/*
 Written: Feb 2017
 This task is a service that pulls items from the Paperless database that need to be submitted to the labs.
 
 SampleStatus: 
    AVAILABLE = Available to DCO to add to a manifest
    SELECTED = The DCO has added it to a manifest but hasn't submitted the manifest
    SHIPPED = The DCO has submitted the manifest and shipped the samples, ready to save to lab
    SENT = The lab XML has been saved, the sample has been shipped and is completely processed.
 
 */



//---------------------------------------------------------------------------------------------------------
// Includes
//---------------------------------------------------------------------------------------------------------
// Include all files necessary from Bravos to interact with database/webservice.

include '../config.php';

include '../includes/Collection.php';
include '../includes/Database.php';
include '../includes/Util.php';
include '../includes/Write.php';
include '../includes/Pedigree.php';
include '../includes/Entity.php';
include '../includes/Environment.php';
include '../includes/Response.php';
include '../includes/Transaction.php';

$util = new Util();
$db = new Database();
$env = new Environment();
$entity = new Entity();
$write = new Write();
$rsp = new Response("json","webservice");

$logpath = "../labs";
$archivepath = $logpath."/archive";
$savePath = "";  // will change based on variables
$saveArchivePath = "";  // will change based on variables

$unprocesseditems = array();
$urineSamples = array();
$bloodSamples = array();

//---------------------------------------------------------------------------------------------------------
// Configuration
//---------------------------------------------------------------------------------------------------------
// Read task configuration being posted to this task process from task.php action.



if (isset($_POST["data"])) {
    $data = $_POST["data"];
} else {
    //If this is being called directly from URL or task has no data associated with it, use these as defaults.
    //source=alias of folder that contains unprocessed items to send to Simon. -- All Data folder
    $data = '{"id":"simonapi","source":"folder.7.2016.9.23.18.18.58.901","api":"http://paperlessnewqa.com/simon/Api/Submit"}';
}

$transaction = new Transaction();
$transaction->open($data);

$sp = json_decode($data,true);

//These settings allow the service to be called directly from a url with a single item -- by alias.
if (isset($_GET["alias"])) {
    $sp["alias"] = $_GET["alias"];
}

//---------------------------------------------------------------------------------------------------------
// Data
//---------------------------------------------------------------------------------------------------------
// Gather all data from the database needed for the task.  If there are multiple records
// the service will be called in a loop.


//Default: Search for items that have not been submitted to Simon.  Get 100 urine samples and 100 blood samples and put them into a queue.
if (!isset($sp["alias"])) {
    
    $urineSamples = getUnprocessedSamples($sp["source"], "urinesample");
    $bloodSamples = getUnprocessedSamples($sp["source"], "bloodsample");
    
    $allsamples = array_merge(filter($urineSamples,"cid","urinsample"), filter($bloodSamples,"cid","bloodsample"));

if (count($allsamples)>0) {

    //This will allow this service to continually run and not get stuck on an error.  Eventually only the ones that have errors will be left in the queue.
    shuffle($allsamples);

    $unprocesseditems = [$allsamples[0]];

}
    
} else {
    //Resubmit the specific item to Simon.  Find it no matter if it has been already submitted or not.
    $arr = getItem($sp["alias"]);
    foreach($arr as $item) {
        if (($item["cid"]=='urinesample')||($item["cid"]=='bloodsample')) {
            $unprocesseditem = $item;
        }
    }
    
    $unprocesseditems[0] = $unprocesseditem;
}


$objects = array();

foreach($unprocesseditems as $unprocesseditem) {
    //Were there any items that need to be sent to the lab xml?

    if (isset($unprocesseditem)) {

        if ((isset($unprocesseditem["dt"]))&&(isset($unprocesseditem["ct"]))) {

            $xmlString = $unprocesseditem["ct"];
            $data = $unprocesseditem["dt"];
            
            if (isset($data['SampleStatus'])) {
                $status = $data["SampleStatus"];
            } else {
                $status = "";
            }


            if (isset($data['ShippingLab_caption'])) {

                $labName = $data["ShippingLab_caption"];
                if (!isset($labName)) {
                    $labName = "other";
                }
                $savePath = $logpath . "/" . $labName;
                $saveArchivePath = $archivepath . "/". $labName;

            } else {

                $labName = "";
                $savePath = $logpath;
                $saveArchivePath = $archivepath;
            }


            if (isset($data['BloodSampleCode'])) {
                $sampleNumber = $data['BloodSampleCode'];
            }
            elseif (isset($data['UrineSampleCode'])) {
                $sampleNumber = $data['UrineSampleCode'];
            }
            else {
                // not sure what to make of this - set sample to blank
                $sampleNumber = "";
            }
                

            // we only need to write the file if there is a sample number, otherwise log that badmammajamma
            if (empty($sampleNumber) || empty($xmlString)) {
            }
            else {
                
                try {
                    // create the lab folder if it doesn't exist
                    if (!file_exists($savePath)) {
                        mkdir($savePath, 0777, false);
                    }
                    
                    //write xml to file with sample number (code) as name
                    file_put_contents($savePath."/". $sampleNumber .".xml", $xmlString);
                                  
                    array_push($objects,[
                        "a"=>$unprocesseditem["a"],
                        "t"=>0,
                        "dt"=>[
                            "timestamp"=>$db->getTimestamp(),
                            "SampleStatus"=>"SENT"
                        ]
                     ]);

                    // create the archive folder if it doesn't exist
                    if (!file_exists($archivepath)) {
                        mkdir($archivepath, 0777, false);
                    }
                    
                    // create the lab archive folder if it doesn't exist
                    if (!file_exists($archivepath."/".$labName)) {
                        mkdir($saveArchivePath, 0777, false);
                    }
                    
                    //write xml to file with sample number (code) as name
                    file_put_contents($saveArchivePath."/". $sampleNumber . ".xml", $xmlString);

                } catch (Exception $e) {
                
                    //---------------------------------------------------------------------------------------------------------
                    // Log fail results
                    //---------------------------------------------------------------------------------------------------------
                    // Gather results from api and add to the event stack.  These will be gathered
                    // into a single group to pass back to the parent task so that all results can be 
                    // managed as a single batch.
        
                    //Error occured writing the file. Return a message to the calling service.
                    $rsp->addEvent("task",[
                            "id"=>"",
                            "type"=>"error",
                            "message"=>"Unable to save lab xml. " . $e["message"]
                    ]);

                }
            }
        }
    }
}


//Write results data to database.
$write->writeData("write", $objects);

$messages = array();
foreach ($objects as $object) {
    array_push($messages, $object);
}
if (isset($rsp->getItems()["task"]["event"])) {
    $events = $rsp->getItems()["task"]["event"];
    foreach($events as $event) {
        array_push($messages,$event);
    }
}

$rsp->write();


//---------------------------------------------------------------------------------------------------------
// Helper Functions
//---------------------------------------------------------------------------------------------------------
// Custom functions that are needed to read/write data for this task.

function getUnprocessedSamples($a, $classid) {
    global $util,$db;
    
    // Pull an array of samples belonging to specific data folder represented by $a that have not been processed.  Limit to 1 on each pass.
    // All samples must be marked as completed before it is considered for submission.
    $object = [
    'a'=>$a,
    'descendants'=>100,
    'ancestors'=>"NONE",
    '$cid'=>$classid,
    '$SampleStatus'=>"SHIPPED",
    'page'=>1,
    'pagesize'=>100
    ];
    
    return $util->decode($db->read($object));
}


function getItem($a) {
    global $util,$db;
    
    //Returns a specific item from the database whether already submitted or not.
    
    $object = [
    'a'=>$a,
    'descendants'=>0,
    'ancestors'=>"NONE"
    ];
    
    return $util->decode($db->read($object));
}


function filter($arr,$k,$v) {
    $arr2 = array();
    foreach($arr as $item) {
        if ($item[$k] == $v) {
            array_push($arr2,$item);
        }
    }
    return $arr2;
}
    
    
?>
