<?php

//---------------------------------------------------------------------------------------------------------
// Simon.php
//---------------------------------------------------------------------------------------------------------
/*
Written: Feb 2017
This task is a service that pulls items from the Paperless database that need to be submitted to Simon.
Paperless saves all of the payload needed to submit to simon in a separate object called a testpacket.
It is a json array of a test and all of its descendants needed for simon submission.



This service only search for a single item at a time and only performs a single pass.  
*/


//---------------------------------------------------------------------------------------------------------
// Includes
//---------------------------------------------------------------------------------------------------------
// Include all files necessary from Bravos to interact with database/webservice.

include '../config.php';

include '../includes/Collection.php';
include '../includes/Database.php';
include '../includes/Util.php';
include '../includes/Write.php';
include '../includes/Pedigree.php';
include '../includes/Entity.php';
include '../includes/Environment.php';
include '../includes/Response.php';
include '../includes/Transaction.php';

$util = new Util();
$db = new Database();
$env = new Environment();
$entity = new Entity();
$write = new Write();
$rsp = new Response("json","webservice");


//---------------------------------------------------------------------------------------------------------
// Configuration
//---------------------------------------------------------------------------------------------------------
// Read task configuration being posted to this task process from task.php action.


if (isset($_POST["data"])) {
    $data = $_POST["data"];
} else {
	//If this is being called directly from URL or task has no data associated with it, use these as defaults.
	//source=alias of folder that contains unprocessed items to send to Simon. -- All Data folder
	$data = '{"id":"simonapi","source":"alltestpackets","api":"http://paperlessnewqa.com/simon/Api/Submit"}';
}

$transaction = new Transaction();
$transaction->open($data);

$sp = json_decode($data,true);



//These settings allow the service to be called directly from a url with a single item -- by name.
if (isset($_GET["source"])) {
    $sp["source"] = $_GET["source"];
}


//---------------------------------------------------------------------------------------------------------
// Data 
//---------------------------------------------------------------------------------------------------------
// Gather all data from the database needed for the task.  If there are multiple records
// the service will be called in a loop.






   
//Default: Search for items that have not been submitted to Simon.  Limit to the first 1 item that hasn't been submitted.
if ($sp["source"]=='alltestpackets') {

	//Prefer to submit test sessions first before other types.
	$arr = getUnprocessedPackets('alltestpackets');

	$packets = filter($arr,"cid","testpacket");

    //Shuffle the array so that a single test that won't submit to the api doesn't stop others from being processed, but
    //the system will continue to try to submit it if it is the only one left.
    shuffle($packets);

	if (count($packets) > 0) {
		$unprocessedpacket = $packets[0];
	}

} else {



	//Resubmit the specific item to Simon.  Find it no matter if it has been already submitted or not.
	$arr = getItem($sp["source"]);


    $packets = filter($arr,"cid","testpacket");

    if (count($packets) > 0) {
		$unprocessedpacket = $packets[0];
	}

}


//Were there any packets that need to be sent to the simonapi?
if (isset($unprocessedpacket)) {

	//Build the payload to be sent.

    //---------------------------------------------------------------------------------------------------------
    // Task API configuration 
    //---------------------------------------------------------------------------------------------------------
    // Parameters for the APIs are always sent as a single json object.
    // Convert the object from BravoObjects into a json.
        

    //---------------------------------------------------------------------------------------------------------
	// Data 
	//---------------------------------------------------------------------------------------------------------
	// Call the external service.

	//Pass this data to the simon submission service.
	//Call the api and pass the object as the configuration
    
    
    
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($unprocessedpacket));
	curl_setopt($curl, CURLOPT_URL, $sp["api"]);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	//$result = json_decode(curl_exec($curl),true);  //use this if the result coming back is string json
	$result = curl_exec($curl); //use this if the result coming back is application/json
    
            
            
	curl_close($curl);
	unset($curl);
    
    
    /*  Dummy result for testing.
    $result = [
        "id"=>"simonapi",
        "type"=>"success",
        "message"=>"ok"
    ];
    */

	//---------------------------------------------------------------------------------------------------------
	// Process Result  
	//---------------------------------------------------------------------------------------------------------
	// result will always be a json with id,type,data properties

	//---------------------------------------------------------------------------------------------------------
	// Success Action 
	//---------------------------------------------------------------------------------------------------------
	// Perform these actions if the API call is successful


	if ($result["type"]=="success") {

        //---------------------------------------------------------------------------------------------------------
        // Database Changes
        //---------------------------------------------------------------------------------------------------------
        // Make any changes necessary in the database to mark off this record as successful.

		//Add timestamp to record and change status that you just processed so that it will not be reprocessed unless you remove/rename the timestamp,


        //output array for the results.
        $objects = array();

        array_push($objects,[
            "a"=>$unprocessedpacket["a"],
            "dt"=>[
				"timestamp"=>$db->getTimestamp(),
                "result"=>$result["type"],
                "message"=>$result["message"]
			]
		]);


        //Write results data to database.
        //$write->writeData("task",$objects);  TODO- remove this to test marking off of submissions-- currently will just replay over and over.

        //---------------------------------------------------------------------------------------------------------
        // Log success results 
        //---------------------------------------------------------------------------------------------------------
        // Gather results from api and add to the event stack.  These will be gathered
        // into a single group to pass back to the parent task so that all results can be 
        // managed as a single batch.
    
        //Get events.
        $messages = array();
        foreach ($objects as $object) {
            array_push($messages,$object);
        }
        if (isset($rsp->getItems()["task"]["event"])) {
            $events = $rsp->getItems()["task"]["event"];
            foreach($events as $event) {
                array_push($messages,$event);
            }
        }
        
        //Return a message to the api task with a summary of all these events.
        $rsp->addEvent("task",[
            "id"=>"",
            "type"=> "success",
            "message" => json_encode($messages,true)
        ]);

    } else {

        //---------------------------------------------------------------------------------------------------------
        // Log fail results 
        //---------------------------------------------------------------------------------------------------------
        // Gather results from api and add to the event stack.  These will be gathered
        // into a single group to pass back to the parent task so that all results can be 
        // managed as a single batch.
    
        //Simon api service returned an error. Return a message to the calling service.
        $rsp->addEvent("task",[
            "id"=>"",
            "type"=>"error",
            "message"=>"The submission failed. " . $result["message"]
        ]);
    }
    
  

} else {

    //---------------------------------------------------------------------------------------------------------
    // No Data Found
    //---------------------------------------------------------------------------------------------------------
    // Service was not called because there was no data available.  Send this event
    // back to the calling service.
    

    $rsp->addEvent("task",[
        "id"=>"",
        "type"=>"notice",
        "message"=>"No tests found to submit."
    ]);
}


$rsp->write();




//---------------------------------------------------------------------------------------------------------
// Helper Functions 
//---------------------------------------------------------------------------------------------------------
// Custom functions that are needed to read/write data for this task.

function getUnprocessedPackets($source) {
	global $util,$db;

	//Pull an array of test session packets belonging to specific data folder represented by $a that have not been processed.
	$object = [
		'a'=>$db->getAlias($source),
		'descendants'=>0,
        'ancestors'=>"NONE",
		'$cid'=>"testpacket",
        '$result'=>"ISNULL",
		'page'=>1,
		'pagesize'=>100
	];

	return $util->decode($db->read($object));

}

function getItem($source) {
	global $util,$db;

	//Returns a specific item from the database whether already submitted or not.
	$object = [
		'a'=>$db->getAlias($source),
		'descendants'=>0,
        'ancestors'=>"NONE"
	];

	return $util->decode($db->read($object));
}


function filter($arr,$k,$v) {
	$arr2 = array();
	foreach($arr as $item) {
		if ($item[$k] == $v) {
			array_push($arr2,$item);
		}
	}
	return $arr2;
}

?>
