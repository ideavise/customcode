<?php

//---------------------------------------------------------------------------------------------------------
// TestSessionAssignment1.php
// The USADA organization needs a way to centrally track Test Session Assignment data. 
// This will insure the most up­to­date status of a test session is available. This will 
// also make it simpler to obtain test session assignment information by Doping Control 
// Officer (DCO), organization, region, or data range. The service is written using 
// .Net 4.5 WEB.API. To interact with the service, the POST http verb is used. 
// Data passed to and from the service is in JSON format:
// 1) POST  api/query/ - get test session assignment
// 2) POST  api/write/ - write test session assignment
//---------------------------------------------------------------------------------------------------------

/* Written: Aug 2, 2016 Bruce G. Williams 
// Testing for Simon API using TestSessionAssignment API Specification
// Reference: "TestSessionAssignmentAPISpecificationDoc-6.pdf" Ryan 6/3/2016
*/

//---------------------------------------------------------------------------------------------------------
// Includes
//---------------------------------------------------------------------------------------------------------
// Include all files necessary from Bravos to interact with database/web service.

include '../config.php';
include '../includes/Collection.php';
include '../includes/Database.php';
include '../includes/Util.php';
include '../includes/Write.php';
include '../includes/Pedigree.php';
include '../includes/Entity.php';
include '../includes/Environment.php';
include '../includes/Response.php';
include '../includes/Transaction.php';


$util = new Util();
$db = new Database();
$env = new Environment();
$entity = new Entity();
$write = new Write();
$rsp = new Response("json","webservice");

//---------------------------------------------------------------------------------------------------------
// Configuration
//---------------------------------------------------------------------------------------------------------
// Read task configuration being posted to this task process from task.php action.



//Retrieve all data from post variable named data.  Should a be valid JSON string.
if (isset($_POST["data"])) {
    $data = $_POST["data"];
} else {
	$data = "{}";
}



$transaction = new Transaction();
$transaction->open($data);

//Optional variables from the query string.
//$arr = new array();
//parse_str($_SERVER['QUERY_STRING'], $arr);


$sp = [
    "id"=>$services["testsessionassignment"]["applicationid"],
    "username"=>$services["testsessionassignment"]["username"],
    "password"=>$services["testsessionassignment"]["password"],
    "data"=>json_decode($data,true)
];

//Optional -- Allow settings to be passed in directly through query string if you want to call it directly to handle specific data or override some settings.
//if (isset($_GET["var"])) {
    //$sp["var"] = $_GET["var"];
//}

//---------------------------------------------------------------------------------------------------------
// Data 
//---------------------------------------------------------------------------------------------------------
// Gather all data from the database needed for the task.

//None needed.  The parameters are all passed through the task settings.


//---------------------------------------------------------------------------------------------------------
// Parameters
//---------------------------------------------------------------------------------------------------------
// Gather all data from the database needed for the task.



//Action
if (isset($sp["data"]["action"])) {
	$action = $sp["data"]["action"];
} else {
	$action = "/api/query";
}

//External service connection details
$service_address = $services["testsessionassignment"]["host"] . $action;

$payload = $sp;
$sp["api"] = $service_address;
  
        
//Debugging convenience. If there is an error from the external service, this tells exactly what data the service was sent.
//This can then be copied to a local version and tested individually.
//echo(json_encode($payload));
//echo($sp["api"]);
//exit();


//---------------------------------------------------------------------------------------------------------
// Service Call
//---------------------------------------------------------------------------------------------------------
// Call the external service.
// Pass this data to the flagging service.
// Call the api and pass the object as the configuration
$curl = curl_init();
curl_setopt_array($curl,$curl_options);  //$curl_options are defaults defined in config.php



curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($sp,JSON_UNESCAPED_SLASHES));
curl_setopt($curl, CURLOPT_URL, $sp["api"]);
$result = json_decode(curl_exec($curl),true);
    
curl_close($curl);
unset($curl);


if (isset($result["type"])) {

    if ($result["type"]=="success") {
    
        //---------------------------------------------------------------------------------------------------------
        // Success Action
        //---------------------------------------------------------------------------------------------------------
        // Perform these actions if the API call is successful.


        //---------------------------------------------------------------------------------------------------------
        // Return success results
        //---------------------------------------------------------------------------------------------------------
        
        //Return a message to the api task.
        $rsp->addEvent("task",array_merge($result,[
            "id"=>"",
            "type"=> "success"
        ]));

    } else {

        //---------------------------------------------------------------------------------------------------------
        // Log fail results 
        //---------------------------------------------------------------------------------------------------------
        // Gather results from api and add to the event stack.  These will be gathered
        // into a single group to pass back to the parent task so that all results can be 
        // managed as a single batch.

	//Remove username password.
	if (isset($sp["username"])) {
		unset($sp["username"]);
	}
	if (isset($sp["password"])) {
		unset($sp["password"]);
	}

        //Service returned an error. Return a message to the calling service.
        $rsp->addEvent("task",[
            "id"=>"",
            "type"=>"error",
            "message"=> "The service failed. " . $result["message"] . ". " . json_encode($sp)
        ]);
    }
        
} else {
    
    //No payload returned from service.
    if (isset($result)) {
        $rsp->addEvent("task",array_merge($result,[
            "id"=>"",
            "type"=>"error",
            "message"=>"An unknown error from the api occurred and no useable data was returned. " . json_encode($sp)
        ]));
    } else {
        $rsp->addEvent("task",[
            "id"=>"",
            "type"=>"error",
            "message"=>"An unknown error from the api occurred and no useable data was returned. " . json_encode($sp)
        ]);
    }

}



$rsp->write();

//---------------------------------------------------------------------------------------------------------
// Helper Functions 
//---------------------------------------------------------------------------------------------------------
// Custom functions that are needed to perform this task.






?>
