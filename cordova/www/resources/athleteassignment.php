<?php

//---------------------------------------------------------------------------------------------------------
// AthleteAssignment.php
// 
// TestSessions and Athletes are imported as separate objects into Paperless.  This function creates a 
// parent/child relationship between a testsession and the athlete assigned to the test sessions.
// If the links are already created, they will just be refreshed but not duplicated.

//Note!!!: Currently just calling this directly from TaskScheduler.  No need to wrap this into a task.
//---------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------
// Includes
//---------------------------------------------------------------------------------------------------------
// Include all files necessary from Bravos to interact with database/web service.

include '../config.php';
include '../includes/Collection.php';
include '../includes/Database.php';
include '../includes/Util.php';
include '../includes/Write.php';
include '../includes/Pedigree.php';
include '../includes/Entity.php';
include '../includes/Environment.php';
include '../includes/Response.php';
include '../includes/Transaction.php';


$util = new Util();
$db = new Database();
$env = new Environment();
$entity = new Entity();
$write = new Write();
$rsp = new Response("json","webservice");

//---------------------------------------------------------------------------------------------------------
// Configuration
//---------------------------------------------------------------------------------------------------------
// Read task configuration being posted to this task process from task.php action.



//Retrieve all data from post variable named data.  Should a be valid JSON string.
if (isset($_POST["data"])) {
    $data = $_POST["data"];
} else {
	$data = "{}";
}


$transaction = new Transaction();
$transaction->open($data);

//Optional variables from the query string.
//$arr = new array();
//parse_str($_SERVER['QUERY_STRING'], $arr);



//---------------------------------------------------------------------------------------------------------
// Data 
//---------------------------------------------------------------------------------------------------------
// Gather all data from the database needed for the task.

//None needed.  The parameters are all passed through the task settings.


//---------------------------------------------------------------------------------------------------------
// Parameters
//---------------------------------------------------------------------------------------------------------
// Gather all data from the database needed for the task.



$rows = $db->getSqlData("SELECT * FROM TestSessions");
$links = array();

foreach($rows as $row) {
	$tsid = $row["TestSessionId"];
	$personid = $row["AthletePersonId"];

	if (isset($tsid)&&isset($personid)) {
		$pa = "testsession." . $tsid;
		$ca = "athlete." . $personid;
		$alias = $pa . "~" . $ca;
		array_push($links,[
			"a"=>$alias,
			"pa"=>$pa,
			"ca"=>$ca,
			"t"=>1
		]);
	}
}

$write->doWrite($links);
   

$rsp->write();

//---------------------------------------------------------------------------------------------------------
// Helper Functions 
//---------------------------------------------------------------------------------------------------------
// Custom functions that are needed to perform this task.






?>
