<?php


//---------------------------------------------------------------------------------------------------------
// emailathlete.php
//---------------------------------------------------------------------------------------------------------
/*
Written: Mar 2017
This task is a service that pulls items from the Paperless database that emailed to the athlete.
Paperless saves all of the payload needed to email to the documents with doctype dcor.  
The email address is given in notification node.  If the emailforms is not set to true in the testsession,
this service will not send that document.



This service only search for a single item at a time and only performs a single pass.  
*/


//---------------------------------------------------------------------------------------------------------
// Includes
//---------------------------------------------------------------------------------------------------------
// Include all files necessary from Bravos to interact with database/webservice.

include '../config.php';

include '../includes/Collection.php';
include '../includes/Database.php';
include '../includes/Util.php';
include '../includes/Write.php';
include '../includes/Pedigree.php';
include '../includes/Entity.php';
include '../includes/Environment.php';
include '../includes/Response.php';
include '../includes/Transaction.php';
include '../includes/Task.php';

$util = new Util();
$db = new Database();
$env = new Environment();
$entity = new Entity();
$write = new Write();
$rsp = new Response("json","webservice");

//environment.
$env->build();


//---------------------------------------------------------------------------------------------------------
// Configuration
//---------------------------------------------------------------------------------------------------------
// Read task configuration being posted to this task process from task.php action.


if (isset($_POST["data"])) {
    $data = $_POST["data"];
} else {
	//If this is being called directly from URL or task has no data associated with it, use these as defaults.
	//source=name of folder that contains unprocessed items to email to the athletes. -- All Data folder
	$data = '{"id":"emailathletes","source":"alldata"}';
}

$transaction = new Transaction();
$transaction->open($data);

$sp = json_decode($data,true);



//These settings allow the service to be called directly from a url with a single item -- by alias.
if (isset($_GET["alias"])) {
    $sp["alias"] = $_GET["alias"];
}


//---------------------------------------------------------------------------------------------------------
// Data 
//---------------------------------------------------------------------------------------------------------
// Gather all data from the database needed for the task.  If there are multiple records
// the service will be called in a loop.

   
//Default: Search for items that have not been emailed to the athlete.  Limit to the first 1 item that hasn't been submitted.
if (!isset($sp["alias"])) {

	//Prefer to submit test sessions first before other types.
	$arr = getUnprocessedForms($sp["source"]);

	$forms = filter($arr,"cid","document");

    //Shuffle the array so that a single test that won't submit to the api doesn't stop others from being processed, but
    //the system will continue to try to submit it if it is the only one left.
    shuffle($forms);

	if (count($forms) > 0) {
		$unprocessedform = $forms[0];
	}

} else {
	//Try to reemail this item.  Find it no matter if it has been already submitted or not.
	$arr = getItem($sp["alias"]);

    $forms = filter($arr,"cid","document");

    if (count($forms) > 0) {
		$unprocessedform = $forms[0];
	}

}


//Were there any packets that need to be sent to the simonapi?
if (isset($unprocessedform)) {

	//Build the payload to be sent.

    //---------------------------------------------------------------------------------------------------------
    // Task API configuration 
    //---------------------------------------------------------------------------------------------------------
    // Parameters for the APIs are always sent as a single json object.
    // Convert the object from BravoObjects into a json.
        

    //---------------------------------------------------------------------------------------------------------
	// Data 
	//---------------------------------------------------------------------------------------------------------
	// Email this form to the athlete.  Translated subject/message is generated when the document is created in paperless app.

//echo "<html><head><link rel='stylesheet' href='http://paperlessbravo.usada.org/resources/pdf.css'><head><body>" . $unprocessedform["ct"] . "</body></html>";
//exit();
    
    $signature = '<br><br><br> <div style=“margin-left:auto;margin-right:auto;”><img style="display:block;margin-left:auto;margin-right:auto;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQ0AAABFCAYAAABQbrO9AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAlwSFlzAAALEwAACxMBAJqcGAAAIF1JREFUeF7tnQlQVFe6x/PGeVOpqXqRKC6RBnlVyaTee1MzvHqTZGpmMkOiE5O4EeOuUVyjiIq7uOKOoAguiBvivqEiRsUdFVREFHA3mhgny2wxTN5MopOn3/v+X9/bOdy+TTfadLf2peoUzV3OvX053+/+v+9855x/IaKnrB/XTyA1a3Uk70Ux/kR78Nxu8TEo6k/VmEF9yj04NyAP0Z8Hf4fCgLzBALmp7TvzI+vVqxddVfXt73/0o3/9nx//+Ommz/zb0/Xr1fvhU/V+WO+pB/fp3ldV3376gx/84EbziNACPrbwv/7zPx6PdgFoBGNJWZodrZUE/p3EJYdLoVaqlM/Yjv0oOFY/T/8d5cnz4/OiTM6tVp8n9fj7GO07JPn7PgLx+gyKkOycTYnZOZtv5+7YRUuWrqcx4xfSgCEp1LrLVGrdFSWJ2nRDmUaxg1Jp1PhMWr3mA7p46TKdKa24U3zqfPZHH9+KDMTvp9/TEw0MzVBjDFAAEIhLOZc8Aww8AkAg/0Pr+t4saDi/ZPcVHAxZs257+patO+9lLV9PfQfNpl+0HkuNokdQ2x4zqc+QNBozablTGTpqMbXtOZOebzWOXmg1lvrGzaf1m/YTQ4NOna4s+uzzzwMSHk8MNBTVoCsGgAHllgIHACQg/xF1bezeqt+CRnVo7NxVkJi/u+Bu1rJN9FbnyQIKGH/msh108lQZ7S04QctX5VH8qHQaOrp6WbBoM23NPUgVlRcpZ/1eAkSef3Mcvdx2Aq3M3kMMDTpXfjX7D59+FuKt/5836nksoQHD5wIApGtuhA4I3a2AKwH3IaAetjf+Yf6uw4KGHRrFJ0+H5O8+eHT3ngP34X4AFvFs9PsOFEsZOjaTXnl3KjV8YyzV//VAeubX79Mzvxkkpb72G9sathhJP2k7ifoPW0hr1u8T0KQs2EwvMDzavTebTpdcuH+27NJnn9z+Q8Co4McCGjB+DRJQEVAOKiTgYiA2EDAP1d+GXZfXt6BBT5WcORu1d9/R2zlr8+il1uPYBZlBBQwKFBh/Q3Y3Gr6ZSA3fmkAN355A9aPj6Rku+P2rGIZI9FDHtgZvJVLo2xPtx/I5LbvPojUbCuhUyXlWJksEHumLcunqtZv3Pvr4k5i6/N96WnfAQkOLR0AxIPagQ0KPRUBhRHv6Ja3jvBfsDnZonDtfEXWiqOTuzOTV9MLvR9O4ySsRwKRhY5d+DwsGRcPWkyi0DbsrbSdTgzfG0LMtR0h5o9sUevb3o7iMlL9D202VY3AszgFkAJB2febSoSMltG7jAXrhrfE0fMxSjnV8QsWnyuP83Z4DChou1ARAoasJKx7h596uYIZG5YVLIadLyr6dPXeNuCPzM7bS4aNnqEW3mRT65ni7WmjNqgEQAAzaJ1GTmCQGApTEOArlMm/hOgaD/XOj1hOo8TvTqXHMNDkW5zRkeISiDobHi+0m0/LsD2jXByfolfbswsSn042bH9H1D2/49YXpd2hoigJuh96roasKKIxYKy7hPZXgjTdUMEPj+InS26tW5wsw0hZuo/w9RfQigMBuBVwMqIVQBkaj9lMFBE06zKCm787k3wyGthOpcbtJlJG5SX6jNHkniZ7rOIuadsQxMxgg03j7FDs8uB4AKJRBtGDxDjp3/pIoDiibygtX7l29dt1v7rhfoKEFMuF6qPEJvacD2y1F4WdF4QowwQqN40Vl2fsPFt9/gV0NKAy8/QUYirporMGiMauLpgyD5zrNpmad51BYl2R6DoDoMI127d5Pr7Pr0Yw/N+s0S/Y168z7+ViBC5QHFygPuC3isvA15i/MpXy+JsCRtWIXlZRWfOyNl8DD1OFTaGjuB1wNNUahux8BEeR5mIcYTOcEIzQ+vHEz+srVa5yQNZ2Gj82i8sqrFBk9mEKihzncERg5jB3Koum7MxgCDISuc8nWLYXCu6dSGP8d3nkWHSk8TjFDMuRzePd5UmzdUqlZl7naebMFOFAednBwDIRzOJ55dRCt3bifVubsleDo2bILD0rOlCf7o+3VOTS0ng/0bhhVBdwRBDQtVRGgqsKsQQYjNErOVN6eNnM1vdJuIl249CG90WmCdKHW/90QCmk5UtwRcUU0dSHqodNMgUVEz/kU+d4C/pxCHYYtoRPFp2jcnA0UwX8375Uu+yJ6prHiYEXCkEGBOoHbIuB4ezyFtBguvS//3mIoVV68Tj3eT6Ou/VPYZblw78LFyz63nzqDhgYLuBrGWAX+xnYrh+IxgoUOkGCDxq1PbseWV1wWtyR7TQHNS99C9X/FeRe/jaP6rw2TXpDQtxPFtRBXpKtdWYR3mytgiOydQc1jF1JE73RKmLmB8y5KaemavRTOfzfvs4j3LaKI99IooluywMOuPFLEZWkSM1UCps++MYpCXh8ukHpvcApdvvqRuCnI6yg+WZbra7XhdWhYsAiswKW3G1SwQaPoZMXVcZNWUHtOtLp4+Ya4JUjOQs5FCHeZNnhzDPd2JFLT9pPZxUgWo4fxR7KKiOjO4GAwRPRbQuFc0lftpbJz5VRw5LT8HdF/Kf/m0oMVCcMFgLGrEnZX2MVp0n4KNW6DLlgGB8MJkHrmt4Np49bDNGn6WklBZ6WBjFKfvoC9Bg0LFk82LIJRaXAadxQrDU7rTqQFnGA1efoqye4Ut4Tf/A1YAcB9QE8IAp1h3AvSvMc8u7pgWISzwgh/bz6FD1xG4e+voNOcz8HdtlLajV0n22x9F5EtNl3gEdFnMUUANqxSwti9QbAU4ACUGrQaI5CCm9Kh9ww6XnSeQluMpgOHTt4/XnQm0dsvh5rq8wo0tK5RoxuiBzhd+lzTkjc0Hzc1O8GbZcqsdb/x5QMMtmsFk9IoO38lewXnSTzPqeA3P/qYfvbGME1lDJXELCgAdKU2jZlCzTpOp/Aus8UlgSsiEBiQRbbeC8g2MIu6T91Kl69cIwRUUWZk7Sfb4FXUDPvfXy5gieiXSeG97PGPCMQ4OnJAlWGEayC3A25K/dftauNIYSl1HzhfUtePnThz25ft8JGgoeVYFJr0hiDo6TYBZfj4ZSsjW40kbxbU6csHGGzXCiZolFdcuz1sdCb1i19AO/OP0bO/4/Rvh8oYLSqjCasMKALpDeG4RHhP7hEBNNj1sEFJDFpBz/XJoGWbjxP3wkhyFkpR6WVqNiCTwt5fRra4bFYdy8kGN6YPxz9YnQA+gFCzd6c7qY2QFiNo+px1kivyeudpMuDNly7KQ0NDC2Yau07xN3pEPPKxYuMXnPcmMFDXhOk5XYLNkH35fYMJGhhl+nqnqRL8HDd1lX08CfImOJMzFNmcmsqAIoAyaN4jhWzsXtj6LibbAIbGoJXULH4NtRm3TlLA4erwwDNHGZOeT2HD1pFtSA6FMVxsUCYAB6DD8InomixuCtQGgqJI/GrEiV+hbSZRTN9UOni4RFyUUyXn7jM03L6kvdVOag0NTV0Yx4PoiVm1uvG27834yhU03u45g8Ynra6VCsles5+fS3DEFvz1PYMFGl/88U/RgAayP3N3HKZ2vZMlO1PGkyA9nLtD7clbs8nGwJDeEsQvWCnY2M2AyxEWt4qiRq6nQ8UXiYe30+eff0FffPFHKfh89cYnFDMtj8GxlsGRLarEBoWCeEgv7lHh+Eg496TYtG5YyS7lxDHJFuU0cyiMF1jtrF67B6NjfZazUStouIldeKQu1MZuBEbn/nMJpQ/Lwb9++TV3ce2Xv/+77ThTeAAs+jkZmTsBjEJ/GVOwXDeIoBGzddsRahw9kvMhKuzZnxhMxgYLw4UBo4sV3aPoLWnOwc8IBD5ZKUAxhLHKSJj/AZ0qu0qffvY5MYToz3/5K7frL6X85a9f0h//9GeZM2PO6mP08xHrBDIS32CVEtF3Cde5UHpTbOhN4Z4Z5IHIOBVO+ML4lG0Ms7bcqzNm4nLas/d4ga/aoMfQ4MaC8SFm7kjSw9wsgp9GaMxK2QjDd/yUlF4TKLhSI4AJjlF+0o33orlRyAtxUkFm+5BsxgXJaPi+KC7T2pWYDuI6ZteOVq6BelBQd43jBnh/DBfUiRKr9FzEqn8/zHN/1HOCCBpJOjRKz56j0FY8II2hgdRuGC4MGLkUMOjIngvEwNG12nXCekpddYCOn74gwdPbf/hUlAUgUVX1N/r66/+Vgs+AB8ABFXKBu3MR9xgwI5d+Fr9KumORw9FcumDn2VPRWdkg61TGpjA0snmawK4DUmRGMA6Glj/q/9bT891CA/EJrfGaAcPRoD29oH7ckNGZO40wAATUn5qAoZ8LF0b5qZaKrjVw/b6rQUMz+Gr7NCVl9j1N3R4+XnXTbpkAyxVo9Z4lJ3WmPW+1JyoP9Wr3hgAz9j30c6/t/8nkOwkIH7WeQD+flUHSqtV7qE336Tz13hkZxYq3OwakYWxJ+4FpFDM4g4ZOXkXT0rZQDk/TB7iUV1TSpctXMBJV4hhQGX/681/ozldf0d///nf69u5dKf/4xz8EHFAfUBsf3/pEAqXoYamovEDbPzhOact30/jkjdQhfjHFxC2kn3bl8Sk8qC2UYxuIrSSnbaRRE5ZSX55OMGCgoTVgs/jFIzfcHoPm3VChAWDk7jhB8TwvwaudJot7AvejpkApjt1/6JyoEXzmn2rdu9qbXSBg0vjxxnfsMzFWBHRxDL6/E8XVutV61Ovwdhi5DghddejbJGhscl95en3ab3G5cKyiPpzO85URBpPSyFi0nVozNLbvPCDQeJFzJtr0SabhEzJpxORlNGv+esrKzqOCg4UcUyjh8SDnxeAvXb7K0LgpIAAQAAYA4ptvvqHvvvuO7t+/L+CA4oDaQHwDQdLvoXGRyrguZI+uWreH0rO20+hp2TRyygrqOjiNXukyQ5TG5Jm8LTGTXZRZgQGNGoCBxl6rgKdZg27ZLekbFQhGQLgLggIygItex8/bjn1gYoCQ97hfp1gHb9ONUzdKGLWuMuTt7qoYVIpDTRiAEanUp7oYUG46OKoM58AtcdyD9tkBLA1Ufn3LBxM04J68xIldRwtPOJRGI37Ly7B3dk/gMsB1QNzh1cFZ1GfSekpZvo+OFpeL0tChAaXxVVWVqIt7//ynFADkb3/7WtwWHRoVPK4ka+MxGpq8ndqOyrGnmXNvTHiP+TImxT4SlofQ81gXKI2lK3IpdvC8wHBP3ADjkaUxkrq83dWKnhgTaOgG6GRo/B11F0De2vy3auQCGsDBDBya+tBnEVNh44ApH6MCwK0C0p65fk8AUZIrBeMrVeHiuweLe5IAaDR6bSQdOlwoMQ3MmYEgpCOmwdCQmAZDQ8aXOIKgK6hHUi7lHSiTeAWCoFAUgARcFMADKgMuC4BSWnmTRmQUUFh8jiR8IVNUYhqAhh7T0KDxfUxjEk9evJXacCo5YhqHDp8s8lW7MH2bagZj5tsneOPGzIKgjwqRAQkLq6kJ7Y3oSTzDEQfRDF03XP3capBUjVm7hitowJ2QrmgTmDkBQVE+uD7UiAUNP3afo8v1ytWb99F7krdrH73WZZpJ7wlDg4e1ywA1Hs0qmaDI0eAMUNvglWRjCAyat5eu3bwtAU+AA6CA6vjyzh1xW1LWn6SwERspbOgaasZJXmGOJC+7ymgug9h4aD0DCupG5trARD3snsh9dU6ilLRNSCf3mcvqBA2DUajgqFGy1wYmMHC4F+gmrQ0sELdwdfyoSSvnGKR+gqs3NW937IOBGs6DwTpcDigSfT+Uh+I+wLhh2OqxDqgqasTpuSlQFqAY7qdcqxdKR3/+poqnNs/cW8dqoPSri+St71JTPQyNEMQjkEKenLqaeg9Olclw9DwNuAiYecueDYr08bk8zoQNHHkayLVAzgXnXiB5a2LmAYltQHEAHlAX+LxxTymFj9xENj4mbMhq6aaVpDCAR0snl6xQJHhhdi8MYuMgLHpwXu6YRB/s3U+hLcfQtu2HkbPxyB6Ap8+1GjQMb2cVGPL287RSd8f1ipt/G12lOjRcBTzVmAVyNhD0BDTMelXg8hiM3/GmhrErhg8o3NIMUtSJBgNXx/Ah9viGAgLTHhYYu3YcrqEfU02dGaCMe4wEmJTjzep2uD3unm1d7w8WaOA5Xrj4YVU3nrdiQHwqzWFwNGg5msec8ITAPOJUH3fyHGdqSkZo1zn2ZCxFbYTFMQhYbTTtv1gSvND9CndFLz+NZ7CwwggbysldrDJsPP4EGaE2uDpIFOO0dEzegxm/MCgO408a8gQ8KL3i7Pf0Ex4ir6WRV3OB67IdGKGhG5Ox4TokvJduBm9Y+UFuhqvkLa1HRD9UfgM0xuN/9e7E74z3ZQAgjBKKAEU1UDFG3uZwJbTP6nPI0Y5RIYTjo5WiPy+RiLw9RoEArolzcY5ab7l2rKooYpU6E5Q6fPYWcff/DSZonDl7KX8Bj+/4RdvxtG5Drn1YPMae8KQ4DVqNltGnGIWK8SG62rBnhfLIVYxsZeUQ1h/ZoVk0In2f5G1g9TSUlbnslrC6eK4fH8u/JZbBSWFQKsgG1QetNWMgYUAchsg3eHOsY6TrnJQVPNFwqoyL4WzQO+7+b97c74CG4Q2oQkPexl4sUCzy466HBKpCSw13AMPMPenYL/kLs/tTYGCEIMDhMEQNJmZv+HLeB9UQpRiwuBRqUfbpykUFjFm9AAjqVcEgcNKLAXoB4w4EEzTYpYi5eOm6xDUyFuVQhx48Y9erDA6ZfGeUTJCD8Sf2YfH28ScS22D3AoFMuBphPIo1nIHws4Q1vHbJdbp2/UMpA2bnCSzCAA0+DsHP8P6ABs+/wYrFPu6EE8g0lYHBcRiKjyH5P38zgbZs20HP8zas5Ha08DRyCrxpozXWpctuNGBXEtmr0pjdiElwL5CH4S55S8/DQFo53BmoEjNoGIOgBuOD/McbHIaMAoXg5GrxNqfjFAPGPl1ZOMlAZZ+4OBpkVCWifnZc23BctXvSoOLymr5sJCYwCxiI1fVz4EWK7vYbsoB69p9FS3mdVlkpDRPwtEiQOS4caoON28bjT9Dbgfk0AA4bFAMGoAEIHODcvKtIFnpGQcwDSgRD4sMwzgQT8iB1HK4Jgp/qCFdNZWA4Pib/GTNhAU1MyqSXYybrk/BIu/NV0aEBozJ7Izq9VR/1xmDgMHyAwF2KuLq/pgFs6I151Puyzves0QWT0kCbqLxwLRlzV6DrddXqLdSjf5KiNrQ5NdioEaiUSYQ5p0Lm/cQUfjzqtTmDwD5DVyZNSd9J58sracP2QhnUhvk2ZB/gwm4NUtEBHcwrKnkZ7/DI1mpzabDKeGsE7cjbTS/FTJTRt0cLSy76uu3q0IAMN4OG198o6shWV7GM2vSo4FhfP7Rgvl6wQYMzNUMuXrp2t29cGnWKncULNud/H9tgV+HZN0ZLspWMeEXehsw+zu4FuyqSvwEQyCxeGdRx1Apeja2MJs7fpgVMWVnIPKGsLjjo2Zxh0Zyhg25cdLFiPRSJZXD8BMoGCmde+hoaP3mpqAxknvJ0f171BDxp25DScE1c9QZ4XfYgc7O2UKjpeGSWevJFrWM8UxLunlOwQQPP40xpZWLp2coHz/MarSlpa3jdk3X2xZx5TVbMbYFu0O+HynMsgoGB4fL6TOT2rM5U+mnvNJ4I+DQNnLha5t7AyFjMJSrKhHMxbOySADr64DSMpMU4F4AJ678OTkilDZvzJZaxjpczKCouk2C6rwugAb/Z40Favr5B63q+bxQ1PfNghAaex+kz5R8v4TEgWHMkO2cHzxe6zL6yGkMDyVY6OJqwOkAXKQxfX/MELgt6VxCnKDxWRO8MYWUBqDBIoCwca5/gXHSx6uninK6OEa3IDenxfjLnZRzimbqSZF3XisrLd1lpOMXXfGEvgIarSL9fKOaLL21d4+FBFKzQ4Hk9o0rPlt8bMmIhvcazee0/eIxmp/K6rJLwxeDg9HKZnIdHoTbl5QzU1dUw74asqMZds5krd8h8oviM3hEbL3mgr7ImiyTx+VAtsgasBoy+wzJkvZTYuPnUost0GdfCK775bSHomqAh3YdWsZ5BMPeeqN+dZxGPwYQ8GCSG9O3DR4opef5GbWlGTr7COq7afBv6Oq6y6BFAgLVbOadjYdYW+Y3SlAOdgAuKYz1XrKomK8lzqjjDqN/whTLatS93HLToOp3OV1x5UHTyvE+7WI0MsKBhgbFWL4ZgVRq64Rw8VBxXcuYsxQ5KpZd5JfetuQdp6/aD9EpHdiVkEWi76pA0c1YLMlZEZv0az6vEJ1L/EWnSTYvPWDVeVAVGrqorx/MI1hc50zQ1YwsdP1FC7XrNFmBUXLj2oKj4fL6/X+SPNTTyn/5lEheqo+IXf9HfDcLd9YMdGng++wqOxSGgOWLsIlnpbPqctTy573lOVsyWOTdktXfMJcqqAy4Gej+w/ACSs1r14L85sImCbTJRsENZ2Bd8HjBiCQ9AO0VrNhTQK+8gnpHGuR0f3i8+We53YOD7AxqucjQCviuTYVFYR8Cocmc8wbrfgobdXWVoRB88dOzuoiWbZM6N9r3m0PpNB6i0rJISp+XQLzvztHysPAABJGQhi7Q+llasVngBaV7SEcdAWQwcmUmHj5ZK6T+cgcRKZHbqZhmrgnyRQGlzevaiz7pcvfnFGRhVdQSNPG/e55NUlwWN72Nc3BMSuSNvX/nOXXvvDx25gJ7nsSHtedbylTxNIMaZ7N5XTDM5ixkrorXqOoUiW2Khozgp+Btl3JSVtInn7ZDj9xbTgOGLZeTqgGELWb1cvM/rtt5haPg8F6OmNqsnd7lKIQ/YTEuGRVQdAQPujteT2p4UcFjQcA6M7ys4GLt5666qjZt3PIhLmEcvtUuUpQV6DFpAs1I30ebco7Sn4JSMctXLieIK2Z6xZCcNTFhCL3eYKuf0H5pBhcfP0dXrN+9dunwjmWf1chry4O+2pEMjx0WuRsB2u7Jhx9YhNAKK7P5uJFbviWc9aNt35sdm52y+jcFki5aspXgOer7dLYkVCA9s40WNGqGwitALtmER56G8tOLS5Ttlucay85fulFdcSeZFlQIOFno70KGBAVmuXJSANCANGohp1EUJ2H+YvwFiKQ33AGFoRK1dty1x7frcizvz9tzdu+8A53Ucppy1O/8vM2srZS3PlRnOkVKOVeT3Hyy+euz4mWyGRkDamlOXq76BG0OeC3BY+RpWt6yjW9aChntoGI2MoRHC0Ig+fKQwmuMg0ZyoFc3QiGZoPJY9dGpjgNp47GIb/n7zBtv1LWjUHhpPWhupltjDDSLBhdoATB5LKj5p/zB/fx8LGhY0nLIBuVG4DIpiRKy/Gy2ubwVB/ddwLWj479kHgu1JcpfZjXDDcDW/Brb7HRwMjfQ67Dnx+/cLlMbhom1gVLTVJR3EcS5X0MAcGwELjjrMBL0VyAYbCPdmKQ1LaZhCA40TiiJQXZU6VBk5gWCYgXwPFjQsaLiEhtIV62q+DQRHo3zdwBkY0XUIjYDNgPX1c3Z1vUCDBs/qFsUl2l0JhOen3WsM/07SCj4/dh0MbqGhqQ74sa66Y33q3zIwYuoooQtJYj6HYCA05trcQwBCw9NV+vyWOKXBQibUdlHyavM/qKtj+d4SuOA+q2q6hkfQ8NBdsQwuCIJjFjRq556wAYZwKXczL255XYHA03o1peaAmnqeBpLT/Ftifh5DQ3FXoDpcBUnTA6F3xdMHZR1XOwNQVKdP1WVN/yft7ajLffVtfktxA7C/znrF4GK4cjOMxgjD45Ku3VuOZpCP7BZr9/DQasoNNGKV+42sNTQUeGAejlsmyWBwYxAHqbN/kmXstTd2bz2zQFMahjci4KC/LQsN+9DwHfsMLoMYLWS5coxDOdewHXWq56D+PNStX1szNtUtcRnD0O5JPxb1on5ABtvwt1OgXqvfeA+Ap3oPuCe9Xrggar1R/De2uXKdnID20NCw4OE/w/UWAB6mnscYGipQjEYmyslgOI63ttl2zfBcGRrqFzjw7xjD+TBgbHOCh/Ft78KQ0xUgQbF4cg+AiAoj9ZxoE7Cp+51U5SNDw+C2uBr0hizTh5ZOD9O4rXPqBmpPCDSMhiaqpJbQUMEDNZBjUB2iCgAHw3b12jgvVoEADNhdYFeClHxciMmxuirR69DvQYWGsX6oDkCs3FAfzkFxsluvQUOBBwa+wT0xc12wDeNbXEo0y9jrxti99VyfIGjASPCmhsGIK+0pNEwUQYK2TTVOh3vE+6JqAAeMWDduIzRitPtC/dWClNp9O7k9Grz07be0843QgNqRe3YFrJrai9ehoV4MeRxcEBw1AwiCqRZAHrMelycEGtXiHYrhVJPtrrabQMNMHYjBmsRVoEiMigDnQ5FUg0YNCgTHJSkgEfWhAULdzpsEhio0nNwN7RjTa5vBo06hYQIQQKLQJHgKgAAuQlarBO4zsKDhSCSryY2oglHX1I5NlALcBAsaNT00vfG5gAjAAhcnxnJlAgsgQQQNMXpNARgDh0Y3otbxOhO1UhM0sM/onlTbprhYUDKOoOfDKg29Pr8qDXfqQXNl0I0LxWFUI3BvVJBEuavP2l83sAkiaMDwzNwIACPEEKPAcemay4B4AdwBPU6CeIZuxOXaPo/cEz4WdaFeKBcjCODOqDDDMcZ6zQKhHrknWt24X3Fx1BLQrgBUhtZI4dZAeQAcKlB0mKB3BvsBnWgUCxoWNAzxBNXXdxXTgJG6672QtqUZc03H6scleFCnq0CoWf3pSvxCVRXGYwERebl6EtPQjlPhpNZX7SUd0NBwZ/iaOhFIaNAAOPSiAwa/oV7Uffpn/Vzjb8BH3hRWqf4MHmOloRqvK2hAFVR7q2vqQd2m9jgARGaGhjd+pGaIqNMVjHCc483Pn6PdAEbgYoAhlIgRGKjXYeiG67uMtfBxsSbfx/Fd9OtaRmGBoVZtIJCh4S3Aw+BVo3NXL1wRzeBrVLhavQADitNLyQQantbrOE6Hlbt7dhOkBehM7xHn1arBPMqNWOc+GaolGKDhr7ZqhIa/7sPddS1oWEqjVm1AjzO5a1jW/tq/JCxoWMZYK2O0jKz2RvakPTPNfUGcREqgfr//B+yJkCGqzV4pAAAAAElFTkSuQmCC" /><p class="MsoNormal" align="center" style="text-autospace:none;text-align:center;"><strong><em><span style="font-family:Calibri; font-size:16.0pt; color:#1D365E;text-align:center; ">U.S. Anti-Doping Agency</span></em></strong><span style="font-family:Calibri; font-size:15.0pt; "> </span></p><p class="MsoNormal" align="center" style="text-align:center;text-autospace:none;"><strong><em><span style="font-family:Calibri; font-size:13.0pt; ">Preserving</span></em></strong><em><span style="font-family:Calibri; font-size:13.0pt; "> the integrity of competition</span></em><span style="font-family:Calibri; font-size:15.0pt; "> </span></p><p class="MsoNormal" align="center" style="text-align:center;text-autospace:none;"><strong><em><span style="font-family:Calibri; font-size:13.0pt; ">Inspiring </span></em></strong><em><span style="font-family:Calibri; font-size:13.0pt; ">true   sport</span></em><span style="font-family:Calibri; font-size:15.0pt; "> </span></p><p class="MsoNormal" align="center" style="text-align:center;text-autospace:none;"><strong><em><span style="font-family:Calibri; font-size:13.0pt; ">Protecting </span></em></strong><em><span style="font-family:Calibri; font-size:13.0pt; ">the   rights of athletes</span></em><span style="font-family:Calibri; font-size:15.0pt; "> </span></p><p class="MsoNormal" style="text-autospace:none;"><strong><span style="font-family:Calibri; font-size:16.0pt; ">Stay Connected: <a href="http://www.usada.org/"><span style="text-underline:none; color:#1D365E; text-decoration:none; ">www.usada.org</span></a><span style="color:#1D365E; ">&nbsp;| <a href="http://www.facebook.com/usantidoping"><span style="color:#1D365E; text-decoration:none; ">Facebook</span></a>&nbsp;| <a href="http://www.twitter.com/usantidoping"><span style="color:#1D365E; text-decoration:none; ">Twitter</span></a></span></span></strong><strong><span style="font-family:"Times New Roman"; font-size:16.0pt; ">&nbsp;</span></strong><strong><span style="font-family:Calibri; font-size:16.0pt; color:#1D365E; ">| <a href="http://www.truesport.org/"><span style="color:#1D365E; text-decoration:none; ">www.TrueSport.org</span></a></span></strong></p></div>';

    $result = (new Task)->swiftemail([
        "toaddress"=>$unprocessedform["dt"]["notification"],
        //"bccaddress"=>"paperlessadmin@usada.org",
        "data"=>$unprocessedform["ct"],
        "subject"=>$unprocessedform["dt"]["subject"],
        "message"=>$unprocessedform["dt"]["message"] . $signature,
        "headertext"=>$unprocessedform["dt"]["AthletePrintedName"] . " Page &p; / &P;",
        "footertext"=> "© United States Anti-Doping Agency.  REV 3/2017",
        "headeralign"=>"right",
        "filename"=>"dcor_" . $unprocessedform["dt"]["timestamp"],
        "css"=>"http://paperlessbravo.usada.org/resources/pdf.css",
        "base"=>$env->getValue("base")
    ]);
	
    /*  Dummy result for testing.
    $result = [
        "id"=>"emailathlete",
        "type"=>"success",
        "message"=>"ok"
    ];
    */

	//---------------------------------------------------------------------------------------------------------
	// Process Result  
	//---------------------------------------------------------------------------------------------------------
	// result will always be a json with id,type,data properties

	//---------------------------------------------------------------------------------------------------------
	// Success Action 
	//---------------------------------------------------------------------------------------------------------
	// Perform these actions if the API call is successful


	if ($result["type"]=="success") {

        //---------------------------------------------------------------------------------------------------------
        // Database Changes
        //---------------------------------------------------------------------------------------------------------
        // Make any changes necessary in the database to mark off this record as successful.

		//Add timestamp to record and change status that you just processed so that it will not be reprocessed unless you remove/rename the timestamp,


        //output array for the results.
        $objects = array();

        array_push($objects,[
            "a"=>$unprocessedform["a"],
"t"=>0,
            "dt"=>[
				"emailtimestamp"=>$db->getTimestamp(),
                "result"=>$result["type"],
                "servicemessage"=>$result["message"]
			]
		]);

        //Write results data to database.
        $write->writeData("task",$objects);  //TODO- remove this to test marking off of submissions-- currently will just replay over and over.

        //---------------------------------------------------------------------------------------------------------
        // Log success results 
        //---------------------------------------------------------------------------------------------------------
        // Gather results from api and add to the event stack.  These will be gathered
        // into a single group to pass back to the parent task so that all results can be 
        // managed as a single batch.
    
        //Get events.
        $messages = array();
        foreach ($objects as $object) {
            array_push($messages,$object);
        }
        if (isset($rsp->getItems()["task"]["event"])) {
            $events = $rsp->getItems()["task"]["event"];
            foreach($events as $event) {
                array_push($messages,$event);
            }
        }
        
        //Return a message to the api task with a summary of all these events.
        $rsp->addEvent("task",[
            "id"=>"",
            "type"=> "success",
            "message" => json_encode($messages,true)
        ]);

    } else {

        //---------------------------------------------------------------------------------------------------------
        // Log fail results 
        //---------------------------------------------------------------------------------------------------------
        // Gather results from api and add to the event stack.  These will be gathered
        // into a single group to pass back to the parent task so that all results can be 
        // managed as a single batch.
    
        //Simon api service returned an error. Return a message to the calling service.
        $rsp->addEvent("task",[
            "id"=>"",
            "type"=>"error",
            "message"=>"The submission failed. " . $result["message"]
        ]);
    }
    
  

} else {

    //---------------------------------------------------------------------------------------------------------
    // No Data Found
    //---------------------------------------------------------------------------------------------------------
    // Service was not called because there was no data available.  Send this event
    // back to the calling service.
    

    $rsp->addEvent("task",[
        "id"=>"",
        "type"=>"notice",
        "message"=>"No tests found to email."
    ]);
}


$rsp->write();




//---------------------------------------------------------------------------------------------------------
// Helper Functions 
//---------------------------------------------------------------------------------------------------------
// Custom functions that are needed to read/write data for this task.

function getUnprocessedForms($source) {
	global $util,$db;

	//Pull an array of test session packets belonging to specific data folder represented by $a that have not been processed.
	$object = [
		'a'=>$db->getAlias($source),
		'descendants'=>100,
        'ancestors'=>"NONE",
		'$cid'=>"document",
        '$doctype'=>"dcor|dcorcombined|receipt",
        '$emailforms'=>"true",
        '$result'=>"ISNULL",
		'page'=>1,
		'pagesize'=>100
	];

	return $util->decode($db->read($object));

}

function getItem($a) {
	global $util,$db;

	//Returns a specific item from the database whether already submitted or not.
	$object = [
		'a'=>$a,
		'descendants'=>0,
        'ancestors'=>"NONE"
	];
	
	return $util->decode($db->read($object));
}


function filter($arr,$k,$v) {
	$arr2 = array();
	foreach($arr as $item) {
		if ($item[$k] == $v) {
			array_push($arr2,$item);
		}
	}
	return $arr2;
}


?>
